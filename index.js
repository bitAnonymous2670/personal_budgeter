/*

	NOTE
		- TAGS AND CATEGORIES ARE THE SAME THING APPREARANTLY

*/

const express = require('express');
const app = express();
const port = 3000;

const body_parser = require('body-parser');

app.use(body_parser.urlencoded({ extended: false }));
app.use(body_parser.json());

const transaction_routes 	= require(__dirname + "/routes/transactions.routes.js");
const category_routes 		= require(__dirname + "/routes/categories.routes.js");
const tags_routes 			= require(__dirname + "/routes/tags.routes.js");
const account_routes 		= require(__dirname + "/routes/accounts.routes.js");
const generate_routes 		= require(__dirname + "/routes/generate.routes.js");
const reporting_routes 		= require(__dirname + "/routes/reporting.routes.js");
const planning_routes 		= require(__dirname + "/routes/planning.routes.js");

app.use('/transactions', 	transaction_routes);
app.use('/categories', 		category_routes);
app.use('/accounts', 		account_routes);
app.use('/generate', 		generate_routes);
app.use("/reporting", 		reporting_routes);
app.use("/planning", 		planning_routes);
app.use('/tags', 			tags_routes);

app.get('/', (req, res) => res.sendFile(__dirname + "/public/index.html"));
app.use(express.static('public'));
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`));