import { config } from "./config.js";


let category_view = {
	"id": "category_view",
	"width": 300,
	"cols": [
		{
			"rows": [
				{"height": 20},
				{
					"id": "category_list_view",
					"view": "list",
					"data": [],
					"ready": function() {
						fetch(config.url + "/categories", {
							"method": 'GET',
							"headers": { 'Content-Type': 'application/json' }
						})
						.then(response => response.json())
						.then((data) => {
							
							data.result.forEach((a) => {
								$$("category_list_view").add({
									"id": a["category_id"],
									"value": a["category_value"]
								});
							});

							return fetch(config.url + "/accounts", {
								"method": 'GET',
								"headers": { 'Content-Type': 'application/json' }
							});
						})
						.then(response => response.json())
						.then((data) => {

							data.result.forEach((a) => {
								$$("account_list_view").add({
									"id": a["account_id"],
									"value": a["account_name"]
								});
							});

							let from_date = $$("transaction_date_selection_view").get_from_date();
							let to_date = $$("transaction_date_selection_view").get_to_date();
							let query = "from=" + from_date + "&to=" + to_date;
							let url = config.url + "/transactions?" + query;
							return fetch(url, {
								"method": 'GET',
								"headers": { 'Content-Type': 'application/json' }
							});
						})
						.then(response => response.json())
						.then((data) => {
							// console.log(data);
							$$("cumulative_transaction_table").add_rows_two(data.result);
							let total_cash = $$("cumulative_transaction_table").get_total_cash();
							$$("account_summary_total_cash").set_total(total_cash);
							$$("cumulative_transaction_table").update_all_row_categories();

							let table_data = $$("cumulative_transaction_table").get_table_data();
							$$("analysis_pivot_summary").update_table(table_data);
							$$("pivot_piechart_view").draw_piechart();
						});
					},
					"onContext":{}
				},
				{"height": 20},
				{
					"view": "text",
					"id": "add_category_text",
					"label": "Category Name:",
					"labelPosition":"top"
				},
				{"height": 20},
				{
					"view": "button",
					"value": "Add To List",
					"on": {
						"onItemClick": function(id, ev) {
							let adding_value = $$("add_category_text").getValue();

							if (
								adding_value == "" || 
								adding_value == null ||
								adding_value == undefined
							) return;

							let submission_array = new Array(1);
							submission_array[0] = {
								"category_value": adding_value
							};
							$$("add_category_text").setValue();

							fetch(config.url + "/categories/", {
								"method": 'PUT',
								"headers": { 'Content-Type': 'application/json' },
								"body": JSON.stringify({ "category_details": submission_array })
							})
							.then(response => response.json())
							.then((data) => {
								return fetch(config.url + "/categories", {
									"method": 'GET',
									"headers": { 'Content-Type': 'application/json' }
								});
							})
							.then(response => response.json())
							.then((data) => {
								$$("category_list_view").clearAll();
								data.result.forEach((a) => {
									$$("category_list_view").add({
										"id": a["category_id"],
										"value": a["category_value"]
									});
								});
								$$("cumulative_transaction_table").update_all_row_categories();
							});
						}
					}
				},
				{"height": 20}
			]
		},
		{"width": 20},
	]
};

export { category_view };