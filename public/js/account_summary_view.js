webix.protoUI({
	"name":"account_summary_view",
	$init:function(config){
		let div_class = "class=\"summary-container\"";

		let summary_title = "Total Cash";
		let summary_total = "$0.00";

		if (config.title) {
			summary_title = config.title;
		}

		let div_content = "<div class=\"summary-title\">" + summary_title + "</div>";
		div_content += "<div class=\"summary-ammount\">" + summary_total + "</div>";

		this.$view.innerHTML = "<div " + div_class + ">" + div_content + "</div>";
	},
  	$setSize:function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	set_total: function(total) {

		$(this.$view).find(".summary-ammount")
					 .removeClass("summary-positive")
					 .removeClass("summary-nuetral")
					 .removeClass("summary-negative");

		if (total < 0) {
			$(this.$view).find(".summary-ammount")
						 .addClass("summary-negative")

		} else {
			$(this.$view).find(".summary-ammount")
						 .addClass("summary-positive")
		}

		if ((typeof total) != "string")
			total = total.toString();

		if (total.includes("-"))
			total = total.replace("-", "-$");
		else
			total = "$" + total;

		$(this.$view).find(".summary-ammount").html(total);
	},
	reset_total: function() {
		this.set_total(0);
	}
}, webix.ui.view, webix.EventSystem); 