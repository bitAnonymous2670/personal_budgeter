let pivot_table_columns = [
	{
		"id": "categories", 
		"value": "Categories", 
		"key": "category"
	},
	{
		"id": "color", 
		"value": "Color", 
		"key": null,
		"css": [
			"short-col",
		],
		"header_css": [
			"analysis-short-col"
		]
	},
	{
		"id": "total_cost", 
		"value": "Total Cost", 
		"key": null,
		"css": [
			"right-text",
			"analysis-short-col",
			"summary-negative"
		],
		"header_css": [
			"analysis-short-col"
		]
	},
	{
		"id": "total_income", 
		"value": "Total Income", 
		"key": null,
		"css": [
			"right-text",
			"analysis-short-col",
			"summary-positive"
		],
		"header_css": [
			"analysis-short-col"
		]
	},
	{
		"id": "expense_percent", 
		"value": "Expense %", 
		"key": null,
		"css": [
			"right-text",
			"analysis-short-col"
		],
		"header_css": [
			"analysis-short-col"
		]
	},
	{
		"id": "transaction_count", 
		"value": "Count", 
		"key": null,
		"css": [
			"right-text",
			"analysis-short-col"
		],
		"header_css": [
			"analysis-short-col"
		]
	}
];

// TODO: Add the ability to pass in classes
function pivot_array_to_row(row_id, row_number, row) {
	let row_str = "<tr id=\"" + row_id + "\">";
	if ((typeof row_number) == "number")
		row_str += ("<td class=\"tr-number\">" + (row_number + 1) + "</td>");
	else 
		row_str += ("<td class=\"tr-number\">" + (row_number) + "</td>");

	for (let i = 0; i < pivot_table_columns.length; i++) {
		let td_class = "";
		let var_str = "";
		let div_style = "";

		if ("css" in pivot_table_columns[i]) {
			td_class += pivot_table_columns[i]["css"].join(" ");
			td_class += " ";
		}

		if (pivot_table_columns[i]["id"] == "color") {
			div_style += "background-color:" + row[pivot_table_columns[i]["id"]] + "; ";
			div_style += "width:25px;height:25px;margin:auto;"
		} else {
			var_str = row[pivot_table_columns[i]["id"]];
		}

		row_str += "<td class=\"" + td_class + "\">";
		row_str += "<div style=\"" + div_style + "\">" + var_str + "</div>";
		row_str += "</td>";
	}

	row_str += "</tr>";

	return row_str;
}

function format_money(value) {
	if ((typeof value) == "number") 
		value = "" + value.toFixed(2);
	
	if (value.includes("-")) {
		value = value.replace("-", "-$");
	} else {
		value = "$" + value;
	}

	return value;
}

webix.protoUI({
	"name":"pivot_summary_view",
	$init:function(config){
		let div_class = "class=\"analysis-pivot-table\"";

		let table_columns = pivot_table_columns;

		if (config.title) {
			// summary_title = config.title;
		}

		let div_content = "<table>";

		// table header
		div_content += "<thead class=\"pivot-table-header\"><tr>";
		div_content += "<th class=\"th-checkbox\">#</th>";

		for (let i = 0; i < table_columns.length; i++) {
			let table_class = "";

			if ("header_css" in table_columns[i]) {
				table_class += table_columns[i]["header_css"].join(" ");
				table_class += " ";
			}

			div_content += "<th class=\"" + table_class + "\">";
			div_content += table_columns[i]["value"];
			div_content += "</th>";
		}

		div_content += "</thead></tr>";

		div_content += "<tbody></tbody>";
		div_content += "</table>";

		this.$view.innerHTML = "<div " + div_class + ">" + div_content + "</div>";

	},
  	$setSize: function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	get_empty_summary_object: function() {
		let list_items = $$("category_list_view").serialize().map( (x) => {
			return {
				"id": x.id, "sub": {
				"value": x.value,
				"amount_outgoing": 0,
				"amount_incoming": 0,
				"count": 0
			}}
		});

		let ret_obj = {};

		for (let i = 0; i < list_items.length; i++) {
			ret_obj[list_items[i]["id"]] = list_items[i]["sub"];
		}

		ret_obj[null] = {
			"value": "Uncategorized", 
			"amount_outgoing": 0,
			"amount_incoming": 0,
			"count": 0
		}

		return ret_obj;
	},
	get_table_rows: function(rows) {
		let pivot_rows = [];
		let total_expenses = 0;
		let total_income = 0;
		let total_expense_holder = 0;
		let total_income_holder = 0;
		let empty_pivot_obj = this.get_empty_summary_object();

		let trans_am = "transaction_amount";
		let trans_cat = "transaction_category";

		for (let i = 0; i < rows.length; i++) {
			if (rows[i][trans_cat] == "jRVTY6QsKxPXU61NsvscQe") {
				// console.log(rows[i])
			} else if (rows[i][trans_am] < 0) {
				empty_pivot_obj[rows[i][trans_cat]]["amount_outgoing"] += rows[i][trans_am];
				total_expenses += rows[i][trans_am];
			} else {
				empty_pivot_obj[rows[i][trans_cat]]["amount_incoming"] += rows[i][trans_am];
				total_income += rows[i][trans_am];
			}

			empty_pivot_obj[rows[i][trans_cat]]["count"] += 1;
		}


		for (const id in empty_pivot_obj) {

			if (total_expenses != 0) {
				total_expense_holder = empty_pivot_obj[id]["amount_outgoing"] / total_expenses;
			} else {
				total_expense_holder = 0.0;
			}

			if (total_income != 0) {
				total_income_holder = empty_pivot_obj[id]["amount_incoming"] / total_income;
			} else {
				total_income_holder = 0.0;
			}

			pivot_rows.push({
				"id": uuidv4(),
				"categories": empty_pivot_obj[id]["value"],
				"total_cost": formatMoney(empty_pivot_obj[id]["amount_outgoing"]),
				"total_income": formatMoney(empty_pivot_obj[id]["amount_incoming"]),
				"expense_percent": Math.abs(total_expense_holder * 100).toFixed(2) + "%",
				"transaction_count": empty_pivot_obj[id]["count"]
			});
		}

		pivot_rows = pivot_rows.filter(x => x.transaction_count != 0);

		for (let i  = 0; i < pivot_rows.length; i++) {
			pivot_rows[i]["color"] = getColorForPercentage(i / pivot_rows.length)
		}

		return pivot_rows;
	},
	update_table: function(rows) {
		$(this.$view).find("tbody").empty();

		let rows_string = "";
		let pivot_rows = this.get_table_rows(rows);
		let total_cost = 0;
		let total_income = 0;
		let total_count = 0;

		for (let i = 0; i < pivot_rows.length; i++) {
			let row_id = uuidv4();

			let row_cost = pivot_rows[i]["total_cost"].replace("$", "");
			row_cost = row_cost.replace(",", "");
			total_cost += parseFloat(row_cost);

			let row_income = pivot_rows[i]["total_income"].replace("$", "");
			row_income = row_income.replace(",", "");
			total_income += parseFloat(row_income);

			total_count += pivot_rows[i]["transaction_count"]

			rows_string += pivot_array_to_row(row_id, i, pivot_rows[i]);
		}


		rows_string += pivot_array_to_row(uuidv4(), "", {
			"id": uuidv4(),
			"categories": "Grand Total",
			"total_cost": formatMoney(total_cost.toFixed(2)),
			"total_income": formatMoney(total_income.toFixed(2)),
			"expense_percent": "",
			"transaction_count": total_count
		})

		$(this.$view).find("tbody").append(rows_string);
	}
}, webix.ui.view, webix.EventSystem); 