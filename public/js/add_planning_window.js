import { config } from "./config.js";

function reset_fields() {
	// $$("amount_text").setValue(null);
	$$("add_categories_list").clearAll();
	$$("category_id_select").setValue();
	$$("add_planning_from").setValue(null);
	$$("add_planning_to").setValue(null);
};

let window_body = {
	"cols": [
		{"width": 20},
		{
			"rows": [
				{"height": 20},
				{
					"view": "label",
					"label": "Categories"
				},
				{
					"view": "list",
					"id": "add_categories_list",
					"gravity": 3,
					"data": []
				},
				{
					"view": "label",
					"label": "category"
				},
				{
					"view": "select",
					"id": "category_id_select",
					"options": [],

				},
				{
					"cols": [
						{

						},
						{
							"view": "button",
							"value": "Add",
							"click": function() {
								let i;
								let selected_option = $$("category_id_select").getValue();

								if (selected_option == "DEFAULT") return;
								
								for (
									i = 0; i < $$("category_id_select").config.options.length; i++
								) {
									if (
										$$("category_id_select").config.options[i].id == 
										selected_option
									){
										break;
									}
								}

								$$("category_id_select").setValue("Default");
								if ($$("add_categories_list").exists(selected_option)) {
									return;
								}

								$$("add_categories_list").add(
									$$("category_id_select").config.options[i]
								);
								
							}
						}
					]
				},
				{
					"view": "label",
					"label": "from"
				},
				{
					"view": "datepicker",
					"id": "add_planning_from",
					"format": "%Y-%m-%d"
				},
				{
					"view": "label",
					"label": "to"
				},
				{
					"view": "datepicker",
					"id": "add_planning_to",
					"format": "%Y-%m-%d"
				},
				{"height": 20},
				{
					"cols": [
						{
							"view": "button",
							"value": "Submit",
							"click": function(id, event) {
								let i;
								let category_ids = null;
								let planned_amount = 0;
								let begin_planning_date = null;
								let end_planning_date = null;

								category_ids = $$("add_categories_list").serialize().map(x => x.id);

								if (category_ids == null || category_ids == "DEFAULT") {
									throw "Variable \"category_ids\" is not filled.";
								}


								begin_planning_date = $$("add_planning_from").getValue();

								if (begin_planning_date == null) {
									throw "Variable \"begin_planning_date\" is not filled.";
								}


								end_planning_date = $$("add_planning_to").getValue();

								if (end_planning_date == null) {
									throw "Variable \"end_planning_date\" is not filled.";
								}

								let submission_array = new Array(category_ids.length);
								
								for (i = 0; i < submission_array.length; i++){
									submission_array[i] = {
										"category_id": 				category_ids[i], 
										"planned_amount": 			planned_amount * 100,
										"begin_planning_date": 		begin_planning_date,
										"end_planning_date": 		end_planning_date
									};
								}

								let url = config.url + "/planning";
								fetch(url, {
									"method": 'PUT',
									"headers": { 'Content-Type': 'application/json' },
									"body": JSON.stringify({
										"category_planning": submission_array
									})
								}).then((response) => {
									if (response.status != 200) {

									}

									// reset everything
									$$("add_categories_list").clearAll();
									$$("add_planning_from").setValue(null);
									$$("add_planning_to").setValue(null);
									$$("add_planning_window").hide();
									return response.json();
								}).then(function(data) {
									// console.log();
									$$("planning").config.on_view_show();
								});
							}
						},
						{"width": 20}, 
						{
							"view": "button",
							"value": "Cancel",
							"click": function(id, event) {
								$$("add_planning_window").hide();
								reset_fields();
							}
						}
					]
				},
				{"height": 20}
			]
		},
		{"width": 20}
	]
};

let add_planning_window = {
	"view":"window",
	"id": "add_planning_window",
	"modal":true,
	"position":"center",
	"head": {
		"view": "template",
		"template": "Add Category Plan",
		"height": 50,
	},
	"width": 500,
	"height": 800,
	"body": window_body,
	"on": {
		"onBeforeShow": function() {
			let url = config.url + "/categories"
			fetch(url, {
				"method": 'GET',
				"headers": { 'Content-Type': 'application/json' }
			})
			.then(response => response.json())
			.then((data) => {
				// console.log(data)
				let insert_values = data.result.map(function(x) {
					return {
						"id": x["category_id"],
						"value": x["category_value"]
					};
				});

				insert_values.unshift({"id": "DEFAULT", "value": ""});

				$$("category_id_select").define("options", insert_values);

				$$("category_id_select").refresh();

			})
		}
	}
};

export {
	add_planning_window
}