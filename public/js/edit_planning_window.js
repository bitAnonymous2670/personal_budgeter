import { config } from "./config.js";

let edit_planning_window_body = {
	"cols": [
		{"width": 20},
		{
			"rows": [
				{
					"view": "label",
					"label": "Category Name"
				}, 
				{
					"view": "text",
					"id": "edit_planning_window_category_name",
					"disabled": true
				},
				{
					"view": "label",
					"label": "Planned Amount"
				}, 
				{
					"view": "text",
					"id": "edit_planning_planned_amount"
				},
				{
					"view": "label",
					"label": "from"
				},
				{
					"view": "datepicker",
					"id": "edit_planning_from",
					"format": "%Y-%m-%d"
				},
				{
					"view": "label",
					"label": "to"
				},
				{
					"view": "datepicker",
					"id": "edit_planning_to",
					"format": "%Y-%m-%d"
				},
				{"height": 20},
				{
					"cols": [
						{
							"view": "button",
							"value": "Update",
							"click": function(id, event) {
								let category_id =
									$$("edit_planning_window").config.get_selected_planning_id();

								let planned_amount = null;
								let begin_planning_date = null;
								let end_planning_date = null; 

								planned_amount = $$("edit_planning_planned_amount").getValue();
								begin_planning_date = $$("edit_planning_from").getValue();
								end_planning_date = $$("edit_planning_to").getValue();

								if (planned_amount == null) {
									throw "Variable \"planned_amount\" is not filled.";
								}

								if (begin_planning_date == null) {
									throw "Variable \"begin_planning_date\" is not filled.";
								}

								if (end_planning_date == null) {
									throw "Variable \"end_planning_date\" is not filled.";
								}

								let submission_obj = {};
								

								submission_obj = {
									"planned_amount": 			parseInt(planned_amount * 100, 10),
									"begin_planning_date": 		begin_planning_date,
									"end_planning_date": 		end_planning_date
								};

								let url = config.url + "/planning/" + category_id;

								fetch(url, {
									"method": 'POST',
									"headers": { 'Content-Type': 'application/json' },
									"body": JSON.stringify({
										"category_planning": submission_obj
									})
								}).then((response) => {
									if (response.status != 200) {

									}

									// reset everything edit_planning_window_category_name
									$$("edit_planning_window_category_name").setValue(null);
									$$("edit_planning_planned_amount").setValue(null);
									$$("edit_planning_from").setValue(null);
									$$("edit_planning_to").setValue(null);
									$$("edit_planning_window").hide();
									return response.json();
								}).then(function(data) {
									// console.log();
									$$("planning").config.on_view_show();
								});
							}
						},
						{"width": 20},
						{
							"view": "button",
							"value": "Cancel",
							"click": function(id, event) {
								$$("edit_planning_window").hide();
								// RESET EVERYTHING
							}
						}
					]
				},
				{"height": 20}
			]
		},
		{"width": 20},
	]
}

let edit_planning_window = {
	"view":"window",
	"id": "edit_planning_window",
	"modal": true,
	"position":"center",
	"head": {
		"view": "template",
		"template": "Edit Category Plan",
		"height": 50,
	},
	"width": 500,
	"height": 500,
	"body": edit_planning_window_body,
	"on": {
		"onBeforeShow": function() {
			let url = config.url + "/planning/" + this.config.get_selected_planning_id();
			fetch(url, {
				"method": 'GET',
				"headers": { 'Content-Type': 'application/json' }
			})
			.then(response => response.json())
			.then((data) => {
				console.log(data)
				if (!("result" in data)) {
					return;
				}

				let planning_val = data["result"];

				if (planning_val.length != 1) {
					return;
				}

				planning_val = planning_val[0];

				$$("edit_planning_window_category_name").setValue(planning_val["category_value"]);
				$$("edit_planning_planned_amount").setValue(planning_val["planned_amount"] / 100);
				$$("edit_planning_from").setValue(new Date(planning_val["begin_planning_date"]));
				$$("edit_planning_to").setValue(new Date(planning_val["end_planning_date"]));

			});
		}
	},
	"set_selected_planning_id": function(planning_id) {
		this.selected_planning_id = planning_id;
	},
	"get_selected_planning_id": function() {
		return this.selected_planning_id;
	}
};

export {
	edit_planning_window
};