let tagify_element_cdn = [
	"third_party_js/tagify/dist/tagify.min.js",
	"third_party_js/tagify/dist/tagify.polyfills.min.js",
	"third_party_js/tagify/dist/tagify.css",
	"./css/tagify-element.css",
]

webix.protoUI({
	"name":"tagify_element",
	$init:function(config){
		let self = this;

		webix.require(tagify_element_cdn, function() {
			let inner_html = "";

			inner_html += "<textarea name='basic' value=''></textarea>";

			self.$view.innerHTML = inner_html;
			self.config.tagged = new Tagify($(self.$view).find("textarea")[0]);
			self.config.tagged.on("before_add", function(e) {
				console.log(e)
			})
		});
		
	},
  	$setSize:function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	$getSize:function(x, y){
		return webix.ui.view.prototype.$getSize.call(this, x, y);
	}
}, webix.ui.view, webix.EventSystem); 