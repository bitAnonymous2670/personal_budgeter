webix.protoUI({
	"name":"pivot_pie_chart",
	$init:function(config){
		let div_class = "";

		if (config.id) {
			
		}

		let div_content = "";


		this.$view.innerHTML = "";
	},
  	$setSize:function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	$getSize:function(x, y){
		// own logic
		// ...
		// parent logic
		return webix.ui.view.prototype.$getSize.call(this, x, y);
	},
	draw_piechart: function() {

		$(this.$view).empty();

		let width = $(this.$view).width();
		let height = $(this.$view).width();
		let margin = 20;

		if (width < height)
			height = width;
		else 
			width = height;

		let center = (height - margin) / 2;
		// The radius of the pieplot is half the width or half the height (smallest one). I subtract a bit of margin.
		var radius = Math.min(width, height) / 2 - margin

		// append the svg object to the div called 'my_dataviz'

		var svg = d3.select(this.$view)
					.append("div")
						.attr("id", "div_template")
					.append("svg")
						.attr("width", width)
						.attr("height", height)
						.attr("class", "pivot_pie_chart_container")
					.append("g")
						.attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

		d3.select("#div_template")
			.append("div")
				.style("opacity", 0)
				.attr("class", "tooltip")
				.attr("id", "pivot_pie_tooltip")
				.style("background-color", "white")
				.style("border", "solid")
				.style("border-width", "2px")
				.style("border-radius", "5px")
				.style("padding", "5px");

		// Three function that change the tooltip when user hover / move / leave a cell
		var mouseover = function(d) {
			d3
				.select("#pivot_pie_tooltip")
				.style("opacity", 1);
			d3
				.select(this)
				.style("stroke", "black")
				.style("opacity", 1);
		}
		var mousemove = function(d) {
			d3
				.select("#pivot_pie_tooltip")
				.html(d.data.key + ": " + formatMoney(d.data.value))
				.style("left", ((d3.mouse(this)[0] + center + 20) + "px"))
				.style("top", ((d3.mouse(this)[1] + center - 25) + "px"))
		}
		var mouseleave = function(d) {
			d3
				.select("#pivot_pie_tooltip")
				.style("opacity", 0);
			d3
				.select(this)
				.style("opacity", .7);
		}

		// data
		let table_data = $$("cumulative_transaction_table").get_table_data();
		let pivot_tables = $$("analysis_pivot_summary").get_table_rows(table_data);
		let data = {};
		let range = [];

		for (let i = 0; i < pivot_tables.length; i++) {
			let cost = pivot_tables[i]["total_cost"]
						.replace("-$", "")
						.replace(",", "")
						.replace("$", "")
			cost = parseFloat(cost);
			if (cost != 0) {
				data[pivot_tables[i]["categories"]] = cost;
				range.push(pivot_tables[i]["color"])
			}
		}

		// set the color scale
		var color = d3.scaleOrdinal()
						.domain(data)
						.range(range)

		// Compute the position of each group on the pie:
		var pie = d3
					.pie()
					.value(function(d) {return d.value; })
		var data_ready = pie(d3.entries(data))

		// Build the pie chart: Basically, each part of the pie is a path 
		// 		that we build using the arc function.
		svg
			.selectAll()
			.data(data_ready)
			.enter()
			.append('path')
				.attr('d',  d3.arc().innerRadius(0).outerRadius(radius) )
				.attr('fill', function(d){ return(color(d.data.key)) })
				.attr("stroke", "black")
				.style("stroke-width", "2px")
				.style("opacity", 0.7)
				.on("mouseover", mouseover)
				.on("mousemove", mousemove)
				.on("mouseleave", mouseleave)
	}

}, webix.ui.view, webix.EventSystem); 