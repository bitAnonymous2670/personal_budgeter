import { config } from "./config.js";

let window_body = {
	"cols": [
		{"width": 20}, 
		{
			"rows": [
				{"height": 20},
				{
					"view": "textarea",
					"id": "transaction_notes_text",
					// "gravity": 3
				},
				{"height": 20},
				{
					"view": "tagify_element",
					// "grvity": 1
				},
				{"height": 20},
				{
					"cols": [
						{
							"view": "button",
							"value": "Submit",
							"on": {
								"onItemClick": function(id, ev) {
		// GET THE CONTENT
									let transaction_notes = $$("transaction_notes_text").getValue();
									let transaction_obj = $$("cumulative_transaction_table").get_notes_item();
									if (!transaction_obj)
										throw "No transaction is selected.";

									if (!transaction_obj.hasOwnProperty("transaction_id"))
										throw "Var \"transaction_obj\" doesnt have key \"transaction_id\".";

									if ( transaction_notes == null || transaction_notes == undefined ) 
										return;

									let submission_array = new Array(1);
									submission_array[0] = { "transaction_notes": transaction_notes }; 

		// RESET THE CONTENT
									$$("transaction_notes_text").setValue();

		// SUBMIT THE ACCOUNT
									let url = config.url + "/transactions/";
									url += transaction_obj.transaction_id;
									fetch(url, {
										"method": 'POST',
										"headers": { 'Content-Type': 'application/json' },
										"body": JSON.stringify({ "transaction_details": submission_array })
									})
									.then(response => response.json())
									.then((data) => {
		// UPDATE THE LIST

										$$("transaction_notes_window").hide();
									})

								}
							}
						},
						{"width": 20},
						{
							"view": "button",
							"value": "Cancel",
							"on": {
								"onItemClick": function(ev, id) {
									$$("transaction_notes_text").setValue();
									$$("transaction_notes_window").hide();
								}
							}
						}
					],
				},
				{"height": 20},
			]
		},
		{"width": 20}
	]
}

let transaction_notes_window = {
	"view":"window",
	"id": "transaction_notes_window",
	"modal":true,
	"position":"center",
	"head": {
		"view": "template",
		"template": "Transaction Notes",
		"height": 50,
	},
	"width": 400,
	"height": 400,
	// "resize": true,
	"body": window_body
};

function init_transaction_notes_window() {
	webix.ui(transaction_notes_window);
	// console.log("here")
	// let tagged_portion = new Tagify($$("transaction_tags_text").getNode());
	// console.log(tagged_portion)
}


export {
	transaction_notes_window,
	init_transaction_notes_window
}