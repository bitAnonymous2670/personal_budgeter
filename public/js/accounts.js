
import { category_view } from "./category_list_view.js";
import { account_view } from "./account_list_view.js";

let accounts_body = {
	"id": "accounts",
	"cols": [
		category_view,
		account_view,
		{}
	]
};

export { accounts_body };