
let analysis_body = {
	"id": "analysis",
	"borderless": true,
	"rows": [
		{
			"cols": [
				// {"width": 20},
				{
					"gravity": 2, 
					"view": "pivot_summary_view", 
					"borderless": true,
					"id": "analysis_pivot_summary"
				},
				{"width": 20},
				{
					"gravity": 1, 
					"view": "pivot_pie_chart",
					"id": "pivot_piechart_view",
					"borderless": true,
				},
				{"width": 20}
			]
		},

		/*
			Row 1

				pivot table
				spending by day
		*/
		/*
			Row 2
				
				Pie chart breakdown
		*/
		/*
			Row 3
				buget breakdown
				account breakdown?

		*/
	]
};

export { analysis_body };