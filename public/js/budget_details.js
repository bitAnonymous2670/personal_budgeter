import { config } from "./config.js";

// console.log(config)

var budget_init = false;

function CSVtoArray(text) {
	var re_valid = /^\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*(?:,\s*(?:'[^'\\]*(?:\\[\S\s][^'\\]*)*'|"[^"\\]*(?:\\[\S\s][^"\\]*)*"|[^,'"\s\\]*(?:\s+[^,'"\s\\]+)*)\s*)*$/;
	var re_value = /(?!\s*$)\s*(?:'([^'\\]*(?:\\[\S\s][^'\\]*)*)'|"([^"\\]*(?:\\[\S\s][^"\\]*)*)"|([^,'"\s\\]*(?:\s+[^,'"\s\\]+)*))\s*(?:,|$)/g;
	// Return NULL if input string is not well formed CSV string.
	if (!re_valid.test(text)) return null;
	var a = [];                     // Initialize array to receive values.
	text.replace(re_value, // "Walk" the string using replace with callback.
		function(m0, m1, m2, m3) {
			// Remove backslash from \' in single quoted values.
			if      (m1 !== undefined) a.push(m1.replace(/\\'/g, "'"));
			// Remove backslash from \" in double quoted values.
			else if (m2 !== undefined) a.push(m2.replace(/\\"/g, '"'));
			else if (m3 !== undefined) a.push(m3);
			return ''; // Return empty string.
		});
	// Handle special case of empty last value.
	if (/,\s*$/.test(text)) a.push('');
	return a;
};

function convert_array_to_table_frmt(row) {
	let ret_array = new Array(6);

	let categories = $$("category_list_view").serialize().filter(x => x.value == row[4]);
	if (categories.length == 0) {
		ret_array[3] = "";
	} else {
		ret_array[3] = categories[0].id;
	}


	ret_array[0] = "";
	ret_array[1] = row[0];
	ret_array[2] = row[3];
	// ret_array[3] = "";
	ret_array[4] = parseFloat(row[1]);
	ret_array[5] = "";

	return ret_array;
}

function convert_array_to_db_format(row) {
	let return_obj = {
		"transaction_date": null,
		"transaction_amount": null,
		"transaction_payee": null,
		"transaction_notes": null,
		"transaction_line": null
	}

	row = CSVtoArray(row);

	if (row.length < 4)
		throw "Row \"" + row + "\" doesnt have more than 4 columns.";

	// console.log(row)

	return_obj["transaction_date"] = row[0];
	return_obj["transaction_amount"] = row[1];
	return_obj["transaction_payee"] = row[3];
	return_obj["transaction_line"] = parseInt(row[5]);
	
	return return_obj
}

function transaction_drop_handler(ev) {
	$$("upload_transaction_zone").drag_leave_event();

	// Prevent default behavior (Prevent file from being opened)
	ev.preventDefault();
	ev.stopPropagation();

	if (ev.dataTransfer.items) {
		// Use DataTransferItemList interface to access the file(s)

		if (1 < ev.dataTransfer.items.length) {
			throw "too many files dropped."
		}

		if (ev.dataTransfer.items.length != 1) {
			throw "false file drop trigger...";
		}

		if (ev.dataTransfer.items[0].kind === 'file') {
			ev.dataTransfer.items[0].getAsFile()
				.text()
				.then((contents) => {
					let process_array = [];
					contents = contents.split("\n");
					contents.forEach((a, i, _) => {
						contents[i] += ("," + (i - 1));
					})

// Get rid of the header.
					contents.shift();
					contents.pop();

					let submission_array = new Array(contents.length);
					submission_array = contents.map(convert_array_to_db_format);

					fetch(config.url + "/transactions/", {
						"method": 'PUT',
						"headers": { 'Content-Type': 'application/json' },
						"body": JSON.stringify({ "transaction_details": submission_array })
					}).then((response) => {
						
					});

				});
		} else {
			throw "only files can be dropped";
		}


		
	} else {
		throw "Problem uploading the file";
	}
}

function download(filename, text) {
	var element = document.createElement('a');
	element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
	element.setAttribute('download', filename);

	element.style.display = 'none';
	document.body.appendChild(element);

	element.click();

	document.body.removeChild(element);
}

let budget_body = {
	"id": "budget_details",
	"rows": [
		{
			"id": "header_summary",
			"gravity": 1,
			"cols" : [
				{
					"id": "upload_transaction_zone",
					"view": "file-drop",
					"borderless": true
				}
			]
		},
		{"height": 50},
		{
			"id": "manual_controls",
			"gravity": 1.5,
			"cols": [
				{
					"view": "account_summary_view",
					"id": "account_summary_total_cash",
					"borderless": true,
					"gravity": 1,
					"title": "Total Cash"
				},
				{
					"view": "account_summary_view",
					"borderless": true,
					"gravity": 1,
					"title": "Total Debt"
				},
				{
					"view": "date_range_selection_view",
					"id": "transaction_date_selection_view",
					"borderless": true,
					"gravity": 3,
					"title": "Please Select The Date Range Of The Transactions",
					"end_init": "current",
					"ready": function() {

						/* DONE IN CATEGORY VIEW FOR REASONS*/
					}
				},
				{
					"gravity": 1,
					"rows": [
						{
							"view": "button", 
							"value": "Refresh Table",
							"on": {
								"onItemClick": function(id, e){
									let tbl = $$("cumulative_transaction_table").clear_table();

									let from_date = $$("transaction_date_selection_view").get_from_date();
									let to_date = $$("transaction_date_selection_view").get_to_date();
									let query = "from=" + from_date + "&to=" + to_date;
									let url = config.url + "/transactions?" + query;

									fetch(url, {
										"method": 'GET',
										"headers": { 'Content-Type': 'application/json' }
									})
									.then(response => response.json())
									.then((data) => {
										$$("cumulative_transaction_table").add_rows_two(data.result);
										let total_cash = $$("cumulative_transaction_table").get_total_cash();
										$$("account_summary_total_cash").set_total(total_cash);
										$$("cumulative_transaction_table").update_all_row_categories();

										let table_data = $$("cumulative_transaction_table").get_table_data();
										$$("analysis_pivot_summary").update_table(table_data);
										$$("pivot_piechart_view").draw_piechart();
									});
								}
							}
						},
						{"height": 20},
						{
							"view": "text",
							"id": "file_name_text_field"
						},
						{
							"view": "button", 
							"value": "Download Table",
							"on": {
								"onItemClick": function(id, e){

									let from_date = $$("transaction_date_selection_view").get_from_date();
									let to_date = $$("transaction_date_selection_view").get_to_date();
									let query = "from=" + from_date + "&to=" + to_date;
									let url = config.url + "/generate?" + query;
									
									fetch(url, {
										"method": 'GET',
										"headers": { 'Content-Type': 'application/json' }
									}).then(response => response.blob()).then(blob => {
										var url = window.URL.createObjectURL(blob);
										var a = document.createElement('a');
										a.href = url;
										let file_name = $$("file_name_text_field").getValue();
										if (file_name == "" || file_name == null) {
											file_name = "budget_file.xlsx";
										} 
										a.download = file_name;
										document.body.appendChild(a); // we need to append the element to the dom -> otherwise it will not work in firefox
										a.click();    
										a.remove();  //afterwards we remove the element again         
									});
								}
							}
						}
					]
				}
			]
		},
		{"height": 50},
		{
			"view": "transaction_table_view",
			"id": "cumulative_transaction_table",
			"gravity": 7,
			"borderless": true,
			"base_url": config.url
		}
	]
};

let budget_init_function = function() {
	if (!budget_init) {
		$$("upload_transaction_zone").$view.ondrop = drop_handler;


	}
}

export { budget_body, budget_init_function, transaction_drop_handler };