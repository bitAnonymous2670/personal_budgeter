let cdn = [
	"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js"
]

webix.protoUI({
	"name":"date_range_selection_view",
	$init:function(config){
		let self = this;

		webix.require(cdn, function() {
			self.after_require();
		});
	},
	after_require: function() {

		let div_class = "class=\"date-range-container\"";

		let div_content = "<div class=\"date-range-title\">";
		div_content += this.config.title;
		div_content += "</div>";
		div_content += "<div class=\"input-container\">";
		div_content += "<div class=\"date-picker-container\"><input type=\"date\" id=\"from\"></div>";
		div_content += "<div class=\"date-picker-container\"><input type=\"date\" id=\"to\"></div>";
		div_content += "</div>";

		this.$view.innerHTML = "<div " + div_class + ">" + div_content + "</div>";
		this.init_from();

		if (this.config.ready)
			this.config.ready();
	},
	init_from: function() {

		
		$(this.$view).find("#from").val(moment().startOf('month').format('YYYY-MM-DD'));

		if (this.config.end_init && this.config.end_init == "current") {
			$(this.$view).find("#to").val(moment().format('YYYY-MM-DD'));
		} else if (this.config.end_init && this.config.end_init == "end") {
			$(this.$view).find("#to").val(moment().endOf('month').format('YYYY-MM-DD'));
		}
	},
  	$setSize:function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	get_from_date: function() {
		return moment($(this.$view).find("#from").val()).format("MMDDYYYY");
	},
	get_to_date: function() {
		return moment($(this.$view).find("#to").val()).format("MMDDYYYY");
	}
}, webix.ui.view, webix.EventSystem); 