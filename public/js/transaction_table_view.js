let tbl_cdn = [
	"https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.0/moment.min.js",
	"./css/tracking-pivot.css"
];

let transaction_table_columns = [
	{
		"id": "date", 
		"value": "Date", 
		"array_index": 1,
		"sql_key": "transaction_date",
		"filter": "date",
		"filter_id": "date_filter",
		"func": "nothing()"
	},
	{
		"id": "description", 
		"value": "Description", 
		"array_index": 2,
		"sql_key": "transaction_payee",
		"filter": "text",
		"filter_id": "description_filter",
		"func": "on_filter_keypress()"
	},
	{
		"id": "category", 
		"value": "Category", 
		"array_index": 3,
		"sql_key": "transaction_category",
		"filter": "text",
		"filter_id": "transaction_category_filter",
		"func": "on_filter_keypress()"
	},
	{
		"id": "account", 
		"value": "Account", 
		"array_index": 4,
		"sql_key": "transaction_account",
		"filter": "text",
		"filter_id": "account_filter",
		"func": "on_filter_keypress()"
	},
	{
		"id": "amount", 
		"value": "Amount", 
		"array_index": 5,
		"sql_key": "transaction_amount",
		"filter": "integer",
		"filter_id": "amount_filter",
		"func": "nothing()"
	}
];

let short_cols = [
	"Date",
];

let medium_cols = [
	"Amount",
];

let large_cols = [
	"Category",
	"Account"
];


function on_filter_keypress() {
	// UPDATE THE THING

	let table_data = $$("cumulative_transaction_table").get_table_data();
	$$("cumulative_transaction_table").clear_table();
	$$("cumulative_transaction_table").add_rows_two(table_data);
	$$("cumulative_transaction_table").update_all_row_categories();

}

function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
	try {
		decimalCount = Math.abs(decimalCount);
		decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

		const negativeSign = amount < 0 ? "-$" : "$";

		let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
		let j = (i.length > 3) ? i.length % 3 : 0;

		return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
	} catch (e) {
		console.log(e)
	}
};


function category_select_on_change(row_id) {
	$$("cumulative_transaction_table").update_table_select_item(row_id, () => {
		$$("analysis_pivot_summary").update_table($$("cumulative_transaction_table").get_table_data());
		$$("pivot_piechart_view").draw_piechart();
	});
}

function account_select_on_change(row_id) {
	$$("cumulative_transaction_table").update_table_account_item(row_id, () => {});
}


function json_to_row(row_number, row, select_html, account_html, options) {
	let trans_filter = null;
	let classes, date_str, str_ind;


// FILTER

// DESCRIPTION

	if (row["transaction_payee"] != null && "description_filter" in options) {
		if (!row["transaction_payee"].toLowerCase().includes(
				options.description_filter.toLowerCase()
			)) {
			return "";
		}

	}

// CATEGORY

	try {

		trans_filter = $$("category_list_view").serialize().filter(
			x => x.id == row["transaction_category"]
		)[0].value;

	} catch (e) {

	}

	if (trans_filter != null && "category_filter" in options) {
		let str_short_len = Math.min(options.category_filter.length, trans_filter.length);
		if (str_short_len != 0) {
			let compared_str = trans_filter.substring(0, str_short_len).toLowerCase().localeCompare(
				options.category_filter.substring(0, str_short_len).toLowerCase()
			);

			if (compared_str != 0)
				return "";
		}
	}

// ACCOUNT

	let row_str = "<tr id=\"" + row["transaction_id"] + "\" class=\"transaction_row\">";

// ROW ID
	row_str += ("<td class=\"tr-number\">" + (row_number + 1) + "</td>");


// ROW CHECK
	classes = [];
	val_str = "";

	row_str += "<td class=\"" + classes.join(" ") + "\">";
	row_str += "<div>" + val_str + "</div>";
	row_str += "</td>";


// TRANSACTION DATE
	classes = [ "transaction-table-short-col", "left-text", ];
	val_str = moment(row["transaction_date"]).add(1, "days").format("MM-DD");

	row_str += "<td class=\"" + classes.join(" ") + "\">";
	row_str += "<div>" + val_str + "</div>";
	row_str += "</td>";


// TRANSACTION DESCRIPTION
	classes = [ "transaction-table-large-col", "left-text", ];
	val_str = row["transaction_payee"];

	row_str += "<td class=\"" + classes.join(" ") + "\">";
	row_str += "<div>" + val_str + "</div>";
	row_str += "</td>";


// TRANSACTION CATEGORY
	classes = [ "transaction-table-short-col", "left-text" ];
	val_str = select_html.replace("%%ONCHANGE%%", row["transaction_id"]);

	row_str += "<td class=\"" + classes.join(" ") + "\">";
	row_str += "<div>" + val_str + "</div>";
	row_str += "</td>";



// TRANSACTION ACCOUNT
	classes = [ "transaction-table-short-col", "left-text", ];
	val_str = account_html.replace("%%ONCHANGE%%", row["transaction_id"]);

	row_str += "<td class=\"" + classes.join(" ") + "\">";
	row_str += "<div>" + val_str + "</div>";
	row_str += "</td>";


// TRANSACTION AMOUNT
	classes = [ "transaction-table-short-col", "right-text"];

	if (row["transaction_amount"] < 0) {
		classes.push("summary-negative")
	} else {
		classes.push("summary-positive")
	}

	val_str = formatMoney(row["transaction_amount"]);

	row_str += "<td class=\"" + classes.join(" ") + "\">";
	row_str += "<div>" + val_str + "</div>";
	row_str += "</td>";


	row_str += "</tr>";
	return row_str
}

webix.protoUI({
	"name":"transaction_table_view",
	"table_rows": null,
	"notes_flag": null,
	$init:function(config){
		let self = this;

		webix.require(tbl_cdn, function() {
			self.after_require();
		});
	},
	after_require: function() {
		let div_class = "class=\"transaction-table-container\"";

		let table_columns = transaction_table_columns.map(x => x.value);

		if (this.config.table_columns) {
			table_columns = this.config.table_columns;
		}

		let div_content = "<table>";


		// table header
		div_content += "<thead class=\"transaction-header\"><tr>";

		div_content += "<th class=\"th-checkbox\"></th>";
		div_content += "<th class=\"th-checkbox\"></th>";


		for (let i = 0; i < table_columns.length; i++) {
			let table_class = "transaction-table-header ";
			table_class += "transaction-table-header-filter ";

			if (short_cols.includes(table_columns[i])) {
				table_class += "transaction-table-short-col";
			}

			if (medium_cols.includes(table_columns[i])) {
				table_class += "transaction-table-medium-col";
			}

			if (large_cols.includes(table_columns[i])) {
				table_class += "transaction-table-large-col";
			}

			div_content += "<th class=\"" + table_class + "\">";

			if (transaction_table_columns[i].filter == "text") {
				let id_val = "id=\"" + transaction_table_columns[i].filter_id + "\" ";
				let on_ket = "onkeyup=\"" + transaction_table_columns[i].func + "\"";

				let comb = id_val + on_ket;
				div_content += "<input type=\"text\" " + comb + ">";
			} 


			div_content += "</th>";
		}

		div_content += "</tr><tr>";

		div_content += "<th class=\"th-checkbox\">#</th>";
		div_content += "<th class=\"th-checkbox\"><input type=\"checkbox\"></th>";

		for (i = 0; i < table_columns.length; i++) {
			let table_class = "transaction-table-header ";
			table_class += "transaction-table-header-label ";

			if (short_cols.includes(table_columns[i])) {
				table_class += "transaction-table-short-col";
			}

			if (medium_cols.includes(table_columns[i])) {
				table_class += "transaction-table-medium-col";
			}

			if (large_cols.includes(table_columns[i])) {
				table_class += "transaction-table-large-col";
			}

			div_content += "<th class=\"" + table_class + "\">";
			div_content += table_columns[i];
			div_content += "</th>";
		}


		div_content += "</tr></thead>";

		div_content += "<tbody></tbody>";
		div_content += "</table>";

		this.$view.innerHTML = "<div " + div_class + ">" + div_content + "</div>";
	},
  	$setSize:function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	_generate_select_list: function() {
		let on_change_str = "onchange=\"category_select_on_change('%%ONCHANGE%%')\""
		let ret_html = "<select class=\"category_list_select\" " + on_change_str + ">";
		ret_html += "<option disabled selected value></option>"
		let options = $$("category_list_view").serialize();

		for (let i = 0; i < options.length; i++) {
			ret_html += "<option value=\"" + options[i].id + "\">";
			ret_html += options[i].value + "</option>";
		}

		ret_html += "</select>";
		return ret_html;
	},
	_generate_account_list: function() {
		let on_change_str = "onchange=\"account_select_on_change('%%ONCHANGE%%')\""
		let ret_html = "<select class=\"account_list_select\" " + on_change_str + ">";
		ret_html += "<option disabled selected value></option>"
		let options = $$("account_list_view").serialize();

		for (let i = 0; i < options.length; i++) {
			ret_html += "<option value=\"" + options[i].id + "\">";
			ret_html += options[i].value + "</option>";
		}

		ret_html += "</select>";
		return ret_html;
	},
	add_rows_two: function(row_array) {
		let rows_string = "";
		this.table_rows = row_array;
		let my_view = $$("cumulative_transaction_table").$view;
		let filters = this.get_filters();

// Why do I have this?
		let select_html = this._generate_select_list();
		let account_html = this._generate_account_list();
		for (let i = 0; i < this.table_rows.length; i++) {
			rows_string += json_to_row(i, this.table_rows[i], select_html, account_html, filters);
		}

		$(this.$view).find("tbody").append(rows_string);
		this._set_context_menu();
	},
	get_total_cash: function() {

		let total_cash = 0;

		for (let i = 0; i < this.table_rows.length; i++) {
			total_cash += parseFloat(this.table_rows[i].transaction_amount);
		}

		return total_cash.toFixed(2);

	},
	reset_options: function(id) {
		let options = $$("category_list_view").serialize()
		let selects = $(this.$view).find("select").empty();
		selects.append(
			$("<option></option>").attr("disabled", "")
								  .attr("selected", "")
								  .attr("value", "")
		);

		$$("category_list_view").serialize().forEach((a, i, _) => {
			selects.append($("<option></option>").attr("value", a.id).text(a.value))
		});

		for (let i = 0; i < this.table_rows.length; i++) {
			if (this.table_rows[i].category != id) {
				let select = $(this.$view).find("#" + this.table_rows[i].id)
										  .find("select")
										  .val(this.table_rows[i].category);

			}
		}
	},
	update_table_select_item: function(row_id, callback) {
		let selected_id = $(this.$view).find("#" + row_id).find(".category_list_select").val();

		if (!$$("category_list_view").exists(selected_id)){
			throw "passed selected id doesnt exist in \"category_list_view\" list."
		}



		let query = "transactions/" + row_id + "/category/" + selected_id;
		console.log(this.config)
		let url = this.config.base_url + "/" + query;

		fetch(url, {
			"method": 'PUT',
			"headers": { 'Content-Type': 'application/json' }
		})
		.then(response => response.json())
		.then((data) => {

			for (let i = 0; i < this.table_rows.length; i++) {
				if (this.table_rows[i].transaction_id == row_id) {
					this.table_rows[i].transaction_category = selected_id;
					break;
				}
			}

			callback();
		});

		return;
	},
	update_table_account_item: function(row_id, callback) {
		let selected_id = $(this.$view).find("#" + row_id).find(".account_list_select").val();

		if (!$$("account_list_view").exists(selected_id)){
			throw "passed selected id doesnt exist in \"category_list_view\" list."
		}

		let query = "transactions/" + row_id + "/account/" + selected_id;
		let url = this.config.base_url + "/" + query;

		fetch(url, {
			"method": 'PUT',
			"headers": { 'Content-Type': 'application/json' }
		})
		.then(response => response.json())
		.then((data) => {

			for (let i = 0; i < this.table_rows.length; i++) {
				if (this.table_rows[i].transaction_id == row_id) {
					this.table_rows[i].transaction_account = selected_id;
					break;
				}
			}

			callback();
		});

		return;
	},
	update_all_row_categories: function() {

		for (let i = 0; i < this.table_rows.length; i++) {

			$(this.$view)
				.find("#" + this.table_rows[i].transaction_id)
				.find(".category_list_select")
				.val(this.table_rows[i].transaction_category);

			$(this.$view)
				.find("#" + this.table_rows[i].transaction_id)
				.find(".account_list_select")
				.val(this.table_rows[i].transaction_account);

		}

	},
	get_table_data: function() {
		return this.table_rows;
	},
	convert_table_to_arrays: function() {
		let ret_array = new Array(this.table_rows.length);

		let category_holder = "";

		for (let i = 0; i < ret_array.length; i++) {
			let holding_array = new Array(5);

			holding_array[0] = this.table_rows[i]["date"];
			holding_array[1] = this.table_rows[i]["amount"];
			holding_array[2] = "";
			holding_array[3] = this.table_rows[i]["description"];

			if ($$("category_list_view").exists(this.table_rows[i]["category"])) {
				category_holder = $$("category_list_view").getItem(this.table_rows[i]["category"]);
				holding_array[4] = category_holder.value;
			} else {
				holding_array[4] = this.table_rows[i]["category"];
			}

			ret_array[i] = "\"" + holding_array.join("\",\"") + "\"";
		}

		return ret_array;
	},
	_set_context_menu: function() {
		var self = this;

		$(this.$view).find(".transaction_row").on("contextmenu", function(event) {

			event.stopPropagation();
			event.preventDefault();

			let transaction_id = $(this).attr("id");
			// console.log(transaction_id);

			if ($$("disapearing_context_menu")) {
				$$("disapearing_context_menu").destructor();
			}

			self._context_menu = new webix.ui({
				"view": "contextmenu", 
				"autowidth": true, 
				"id": "disapearing_context_menu",
				 position: function(state){ 
					state.left = event.pageX + 5; // fixed values
					state.top = event.pageY + 5;
				},
				"on": {
					"onHide": function() {
						this.destructor();
					},
					"onMenuItemClick": function(context_id) {

						this.destructor();
						self._set_notes_flag(transaction_id);
						let url = self.config.base_url + "/transactions/";
						url += transaction_id;
						fetch(url, {
							"method": 'GET',
							"headers": { 'Content-Type': 'application/json' }
						})
						.then(response => response.json())
						.then((data) => {
							if (!data.hasOwnProperty("result"))
								throw "Something something bad expected return value";

							if (!Array.isArray(data.result))
								throw "Something something bad expected return value";

							if (data.result.length != 1)
								throw "Array should be of length 1, there is a problem.";

							let transaction_obj = data.result[0];

							if (!transaction_obj.hasOwnProperty("transaction_notes"))
								throw "Var \"transaction_obj\" doesnt have key \"transaction_notes\"";

							$$("transaction_notes_text").setValue(transaction_obj.transaction_notes)
							$$("transaction_notes_window").show();
						});
					}
				}
			});

			let options = ["Notes"];

			self._context_menu.parse(options);
			$$("disapearing_context_menu").show();

		})

	},
	_set_notes_flag: function(transaction_id) {
		this.notes_flag = transaction_id;
	},
	get_notes_item: function() {
		for (let i = 0; i < this.table_rows.length; i++) {
			if (this.table_rows[i].transaction_id == this.notes_flag) {
				return this.table_rows[i];
			}
		}

		return null;
	},
	get_transaction_from_id: function(transaction_id) {
		let trans = this.table_rows.filter(x => x.transaction_id == transaction_id)[0];
		return trans;
	},
	convert_json_to_array: function() {

	},
	get_rbfcu_headers: function() {
		return "\"Post Date\",\"Amount\",\"Check Number\",\"Payee\",\"Memo\"";
	},
	clear_table: function() {
		$(this.$view).find("tbody").empty();
	},
	get_filters: function() {
		let desc_filter = null;
		let cat_filter = null;
		let account_filter = null;

		let big_view = $$("cumulative_transaction_table").$view;

		desc_filter = $(big_view).find("#description_filter");
		if (desc_filter.length != 1) {
			throw "Too many description filters!";
		}

		cat_filter = $(big_view).find("#transaction_category_filter");
		if (cat_filter.length != 1) {
			throw "Too many category filters!";
		}

		account_filter = $(big_view).find("#account_filter");
		if (desc_filter.length != 1) {
			throw "Too many account filters!";
		}

		return {
			"description_filter": desc_filter.val(),
			"category_filter": cat_filter.val(),
			"account_filter": account_filter.val()
		};
	}
}, webix.AtomDataLoader, webix.EventSystem, webix.ui.view); 