import { config } from "./config.js";


let top_nav_options = [
	{
		"name": "By Category", 
		"select": "by_category", 
		"id": "by_category_option",
		// "right_nav": "empty"
	}, {
		"name": "By Year", 
		"select": "by_year", 
		"id": "by_year_option",
		// "right_nav": "empty"
	}
];

let top_nav_css_options = [
	{"key": "height", "value": 50},
	{"key": "borderless", "value": true},
	{"key": "css", "value": "top-hand-nav-css"},
	{"key": "align", "value": "center"}
];

function get_top_nav_options() {
	var nav_copy = top_nav_options;
	for (let i = 0; i < nav_copy.length; i++) {

		nav_copy[i]["view"] = "template";
		nav_copy[i]["template"] = nav_copy[i]["name"];
		nav_copy[i]["id"] = nav_copy[i]["id"];
		nav_copy[i]["height"] = 50;

		for (let j = 0; j < top_nav_css_options.length; j++) {
			nav_copy[i][top_nav_css_options[j]["key"]] = top_nav_css_options[j]["value"];
		}

		nav_copy[i]["onClick"] = {
			"top-hand-nav-css": function(ev, id) {
				$$("tracking_multiview").setValue(nav_copy[i]["select"]);
			}
		}
	}

	nav_copy.push({
		"gravity": "fill"
	})

	return nav_copy;
};

let by_category = {
	"id": "by_category",
	"borderless": true,
	"rows": [
		{
			"view": "expense_tracking_pivot",
			"id": "category_monthly_expense_tracking",
			"on_category_select": function(category) {
				console.log(category);
			}, "ready": function() {
				fetch(config.url + "/reporting/pivot", {
					"method": "GET"
				}).then(result => result.json()).then(function(json) {
					// console.log(json);
					$$("category_monthly_expense_tracking").set_pivot_table(json.result);
					$$("category_monthly_expense_tracking")._draw_body();
				})
			}
		}
	]
};

let by_year = {
	"id": "by_year",
	"borderless": true,
	"rows": [
		{
			"view": "expense_tracking_pivot",
			"id": "yearly_monthly_expense_tracking",
			"pivot_list": "year",
			"on_category_select": function(category) {
				
			}, "ready": function() {
				fetch(config.url + "/reporting/pivot", {
					"method": "GET"
				}).then(result => result.json()).then(function(json) {
					$$("yearly_monthly_expense_tracking").set_pivot_table(json.result);
					$$("yearly_monthly_expense_tracking")._draw_body();
				});
			}
		}
	]
};



let tracking_body = {
	"id": "tracking",
	"rows": [
		{	
			"cols": get_top_nav_options()
		}, {
			"height": 20
		}, {
			"gravity": 5,
			"view": "multiview",
			"id": "tracking_multiview",
			"cells": [
				by_category,
				by_year
			],
			"onViewShow": function() {
				console.log("hello");
			},
		}
	]
};

export { tracking_body };