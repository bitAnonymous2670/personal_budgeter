function drag_over_class_add(element) {
	$$(element).drag_over_event();
}

function drag_leave_class_add(element) {
	$$(element).drag_leave_event();
}

webix.protoUI({
	"name":"file-drop",
	$init:function(config){
		let div_class = "class=\"upload-transaction-zone\" ";

		if (config.id) {
			div_class += [
				"ondragenter=drag_over_class_add(\"" + config.id  + "\");",
				"ondragleave=drag_leave_class_add(\"" + config.id  + "\");",
			].join(" ");
		}

		let div_content = "Add your file to the right &#8194;";
		div_content += "<i class=\"fa fa-upload\" aria-hidden=\"true\"></i>";


		this.$view.innerHTML = "<div " + div_class + ">" + div_content + "</div>";
	},
  	$setSize:function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	drag_over_event: function() {
		this.$view.classList.add("drag-enter-file-drop");
	},
	drag_leave_event: function() {
		this.$view.classList.remove("drag-enter-file-drop")
	}
}, webix.ui.view, webix.EventSystem); 