import { config } from "./config.js";

let account_view = {
	"id": "account_view",
	"width": 300,
	"cols": [
		{
			"rows": [
				{"height": 20},
				{
					"id": "account_list_view",
					"view": "list",
					"data": [],
					"ready": function() {
						/* DONE IN CATEGORY VIEW FOR REASONS */
					},
					"onContext":{}
				},
				{"height": 20},
				{
					"view": "text",
					"id": "add_account_text",
					"label": "Account Name:",
					"labelPosition":"top"
				},
				{"height": 20},
				{
					"view": "button",
					"value": "Add To List",
					"on": {
						"onItemClick": function(id, ev) {
// GET THE CONTENT
							let adding_value = $$("add_account_text").getValue();

							if (
								adding_value == "" || 
								adding_value == null ||
								adding_value == undefined
							) return;

							let submission_array = new Array(1);
							submission_array[0] = {
								"account_name": adding_value
							};

// RESET THE CONTENT
							$$("add_account_text").setValue();

// SUBMIT THE ACCOUNT
							fetch(config.url + "/accounts/", {
								"method": 'PUT',
								"headers": { 'Content-Type': 'application/json' },
								"body": JSON.stringify({ "account_details": submission_array })
							})
							.then(response => response.json())
							.then((data) => {
// UPDATE THE LIST
								return fetch(config.url + "/accounts", {
									"method": 'GET',
									"headers": { 'Content-Type': 'application/json' }
								});
							})
							.then(response => response.json())
							.then((data) => {
								$$("account_list_view").clearAll();
								data.result.forEach((a) => {
									$$("account_list_view").add({
										"id": a["account_id"],
										"value": a["account_name"]
									});
								});
								$$("cumulative_transaction_table").update_all_row_categories();
							});
						}
					}
				},
				{"height": 20}
			]
		},
		{"width": 20},
	]
};

export { account_view };