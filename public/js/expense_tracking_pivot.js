let expense_tracking_pivot_cdn = [
	"http://192.168.1.151:3000/css/tracking-pivot.css"
];

function select_category(view_id, category) {

	if ($$(view_id).config.pivot_list && $$(view_id).config.pivot_list) {
		$$(view_id).selected_year = category;
	} else {
		$$(view_id).selected_category = category;
	}
	
	$$(view_id)._draw_body();
}

webix.protoUI({
	"name":"expense_tracking_pivot",
	"pivot_data": null,
	"categories": null,
	"years": null,
	"months": null,
	"content_dict": null,
	"selected_category": null,
	$init: function(config){

		this.$view.innerHTML = "ERR!!!";

		if (expense_tracking_pivot_cdn) {
			webix.require(expense_tracking_pivot_cdn)
				.then(webix.bind(this._before_set_body, this));
		} else {
			this._before_set_body();
		}
	},
	_before_set_body: function() {
		if (this.config.ready)
			this.config.ready.call();

	},
  	$setSize: function(){
	   webix.ui.view.prototype.$setSize.apply(this, arguments);
	},
	set_total: function(total) {
		return "";
	},
	set_pivot_table: function(pivot_data) {
		this.pivot_data = pivot_data;
		this.categories = new Set();
		this.years = new Set();
		this.months = new Set();
		this.content_dict = {};

		let formatter = new Intl.NumberFormat('en-US',{
			"style": 'currency',
			"currency": 'USD',
		});

		var dummy_var = -1;
		var spent = -1;

		this.pivot_data.forEach((a) => {

			this.categories.add(a.category_value);
			this.years.add(a.bank_year);
			this.months.add(a.bank_month);

			dummy_var = [a.category_value, a.bank_year, a.bank_month].join("|");
			spent = (parseInt(a.total_spent, 10) / 100).toFixed(2);
			this.content_dict[dummy_var] = spent

			dummy_var = ["Total", a.bank_year, a.bank_month].join("|");
			if (dummy_var in this.content_dict) {
				this.content_dict[dummy_var] += parseInt(a.total_spent, 10) / 100;
			} else {
				this.content_dict[dummy_var] = parseInt(a.total_spent, 10) / 100;
			}

			if (spent.includes("-")) {
				dummy_var = ["Expenses", a.bank_year, a.bank_month].join("|");

				if (dummy_var in this.content_dict) {
					this.content_dict[dummy_var] += parseInt(a.total_spent, 10) / 100;
				} else {
					this.content_dict[dummy_var] = parseInt(a.total_spent, 10) / 100;
				}
			}

		});

		for (obj_key in this.content_dict) {
			if (obj_key.includes("Total") || obj_key.includes("Expenses")) {
				this.content_dict[obj_key] = this.content_dict[obj_key].toFixed(2);
			}
		}

		this.categories = Array.from(this.categories).sort();

		this.categories.push("Expenses");
		this.categories.push("Total");

		this.years = Array.from(this.years).sort();
		this.months = Array.from(this.months).sort();

		this.selected_category = this.categories[0];
		this.selected_year = this.years[this.years.length - 1]



	},
	_draw_body: function() {
		let html_content = "";

		html_content += "<div class=\"main-container\">";

		html_content += "<div class=\"pivot-column category-selection\">";
		var on_click = null;

		if (this.config.pivot_list && this.config.pivot_list == "year") {
			this.years.forEach((a) => {
				on_click = "onclick=\"select_category('" + this.config.id + "','" + a + "')\"";
				class_def = "class=\"category-selector\""
				html_content += ("<div " + [on_click,class_def].join(" ") + ">" + a + "</div>");
			});
		} else {
			this.categories.forEach((a) => {
				on_click = "onclick=\"select_category('" + this.config.id + "','" + a + "')\"";
				class_def = "class=\"category-selector\""
				html_content += ("<div " + [on_click,class_def].join(" ") + ">" + a + "</div>");
			});
		}
		html_content += "</div>";

		html_content += "<div class=\"pivot-column buffer\"></div>";

		html_content += "<div class=\"pivot-column category-table\">";
		html_content += 	"<div class=\"pivot-table\">";
		html_content += 		"<div class=\"pivot-table-header\">";

		html_content += "<div class=\"pivot-table-header-cell blank-cell\"></div>";
		this.months.forEach((a) => {
			html_content += ("<div class=\"pivot-table-header-cell\">" + a + "</div>");
		});

		html_content += 		"</div>";

		if (this.config.pivot_list && this.config.pivot_list == "year") {
			this.categories.forEach((a) => {
				html_content += "<div class=\"pivot-table-header\">";

				html_content += ("<div class=\"pivot-table-header-cell\">" + a + "</div>");
				this.months.forEach((b, i, _) => {
					let comb = [a, this.selected_year, b].join("|");
					var classes = [
						"pivot-table-data-cell",
					];

					if (i == this.months.length - 1) {
						classes.push("end-cell");
					}

					classes = ("class=\"" + classes.join(" ") + "\"");

					if (comb in this.content_dict) {
						let val = this.content_dict[comb];
						html_content += ("<div " + classes + ">" + val + "</div>");
					} else {
						html_content += ("<div " + classes + ">" + "--" + "</div>");
					}
				});

				html_content += "</div>";
			});
		} else {
			this.years.forEach((a) => {
				html_content += "<div class=\"pivot-table-header\">";

				html_content += ("<div class=\"pivot-table-header-cell\">" + a + "</div>");
				this.months.forEach((b, i, _) => {
					let comb = [this.selected_category, a, b].join("|");
					var classes = [
						"pivot-table-data-cell",
					];

					if (i == this.months.length - 1) {
						classes.push("end-cell");
					}

					classes = ("class=\"" + classes.join(" ") + "\"");

					if (comb in this.content_dict) {
						let val = this.content_dict[comb];
						html_content += ("<div " + classes + ">" + val + "</div>");
					} else {
						html_content += ("<div " + classes + ">" + "--" + "</div>");
					}
				});

				html_content += "</div>";
			});
		}

		html_content += 		"</div>";
		html_content += 	"</div>";
		html_content += "</div>";

		html_content += "<div class=\"pivot-column right-side-blank\">"
		html_content += "</div>";


		this.$view.innerHTML = html_content;
	}
}, webix.ui.view, webix.EventSystem); 
