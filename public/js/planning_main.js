import { config } from "./config.js";

let formatter = new Intl.NumberFormat('en-US', {
	style: 'currency',
	currency: 'USD',
});

let planning_datatable_context_menu = {
	view:"contextmenu",
	id:"planning_datatable_context_menu",
	data:[
		{
			"value": "Edit", 
			"id": "edit"
		}, 
		{
			"value": "Delete", 
			"id": "delete"
		}
	],
	on:{
		"onMenuItemClick": function(context_id) {
			let row_id = this.getContext().id.row;
			row_id = $$("planning_datatable").getItem(row_id).planning_id;
			$$("edit_planning_window").config.set_selected_planning_id(row_id);
			

			if (context_id == "delete") {
				let url = config.url + "/planning/" + row_id;
				fetch(url, {
					"method": 'DELETE',
					"headers": { 'Content-Type': 'application/json' }
				})
				.then((response) => {
					if (response.status != 200) {

					}

					// reset everything
					$$("planning").config.on_view_show();
					return response.json();
				})
				.then((data) => {
					// console.log(data)
				})
			} else if (context_id == "edit") {
				$$("edit_planning_window").show();
			} else {

			}
		}
	}
};

webix.ui.datafilter.avgColumn = webix.extend({
	refresh:function(master, node, value){

		let vals = $$("planning_datatable").serialize()
											.map((x) => {return parseInt(x.left, 10)})
											.reduce((acc, curr) => acc + curr, 0);

		node.innerHTML = formatter.format(vals / 100);
	}
}, webix.ui.datafilter.summColumn);


let planning_datatable = {
	"view": "datatable",
	"id": "planning_datatable",
	"gravity": 8,
	"ready": function() {

	},	
	"footer": true,
	// "select": true,
	"math": true,
	"data": [],
	"columns": [
		{
			"id": "category_value",
			"header": {
				"text": "Category Name",
				"css": {
					"text-align": "center"
				}
			},
			"fillspace": true,
			"gravity": 3,
			"css": {
				'text-align':'center'
			}
		},
		{
			"id": "planned_amount",
			"header": {
				"text": "Planned",
				"css": {
					"text-align": "center"
				}
			},
			"fillspace": true,
			"gravity": 1,
			"format": function(s) {
				return formatter.format(Number(s) / 100);
			},
			"css": {
				'text-align':'right'
			}
		}, 
		{
			"id": "spent",
			"header": {
				"text": "Spent",
				"css": {
					"text-align": "center"
				}
			},
			"fillspace": true,
			"gravity": 1,
			"format": function(s) {
				return formatter.format(Number(s) / 100);
			},
			"css": {
				'text-align':'right'
			}
		},
		{
			"id": "left",
			"header": {
				"text": "What Is Left",
				"css": {
					"text-align": "center"
				}
			},
			"fillspace": true,
			"gravity": 1,
			"format": function(s) {
				return formatter.format(Number(s) / 100);
			},
			"cssFormat": function(value, config) {

				let ret_obj = {
					"text-align": "right",
					"font-weight": "700"
				}
				value = parseFloat(value.replace("$", ""));

				if (0 < value) {
					ret_obj["color"] = "#28a745!important"
				} else if (value < 0) {
					ret_obj["color"] = "#dc3545!important"
				}

				return ret_obj;
			},
			"math": "[$r,planned_amount] + [$r,spent]",
			"footer": {
				"content": "avgColumn",
				"cssFormat": function(value, config) {

					console.log(value)

					let ret_obj = {
						"text-align": "right",
						"font-weight": "700"
					}
					value = parseFloat(value.replace("$", ""));

					if (0 < value) {
						ret_obj["color"] = "#28a745!important"
					} else if (value < 0) {
						ret_obj["color"] = "#dc3545!important"
					}

					return ret_obj;
				},
			}
		},
		{
			"id": "begin_planning_date",
			"header": {
				"text": "Begin Date",
				"css": {
					"text-align": "center"
				}
			},
			"fillspace": true,
			"gravity": 2,
			"format":function(d) {
				return moment(d).format("MM-DD-YYYY");
			},
			"css": {
				'text-align':'center'
			}
		},
		{
			"id": "end_planning_date",
			"header": {
				"text": "End Date",
				"css": {
					"text-align": "center"
				}
			},
			"fillspace": true,
			"gravity": 2,
			"format":function(d) {
				return moment(d).format("MM-DD-YYYY");
			},
			"css": {
				'text-align':'center'
			}
		}
	]
}


let planning = {
	"id": "planning",
	"rows": [
		{
			"rows": [
				{
					"cols": [
						{
							"view": "date_range_selection_view",
							"id": "category_range_selection_view",
							"borderless": true,
							"title": "Please Select The Date Range Of The Planning",
							"end_init": "end",
							"ready": function() {

								/* DONE IN CATEGORY VIEW FOR REASONS*/
							}
						},
						{"width": 20},
						{
							"rows": [
								{
									"fillspace": true,
								},
								{
									"view": "button",
									"value": "Refresh",
									"click": function(id, event) {
										$$("planning").config.on_view_show_functions.forEach(
											(a) => {
												a();
											}
										)
									}
								},
								{
									"fillspace": true,
								},
							]
						},
						{
							// "gravity": 2
						}
					]
				}
			]
		},
		{
			"height": 20
		},
		planning_datatable,
		{
			"height": 20
		},
		{
			"cols": [
				{
					"gravity": 3
				},
				{
					"view": "button",
					"value": "Add",
					"click": function(id, event) {
						$$("add_planning_window").show();
					}
				}
			]
		}
	],
	on_view_show_functions: [
		function() {
			let from_date = $$("category_range_selection_view").get_from_date();
			let to_date = $$("category_range_selection_view").get_to_date();
			let query = "from=" + from_date + "&to=" + to_date;

			let url = config.url + "/planning?" + query;
			fetch(url, {
				"method": 'GET',
				"headers": { 'Content-Type': 'application/json' }
			})
			.then(response => response.json())
			.then(function(data) {

				$$("planning_datatable").clearAll();

				if (!data.hasOwnProperty("result")) 
					throw "Error key \"result\" not in JSON";

				data.result.forEach((a) => {
					$$("planning_datatable").add(a)
				})
			});
		}
	],
	on_view_show: function() {
		this.on_view_show_functions.forEach(a => a());
	}
}

export {
	planning,
	planning_datatable_context_menu
}