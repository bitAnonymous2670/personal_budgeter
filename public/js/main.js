import { budget_body, budget_init_function, transaction_drop_handler } from "./budget_details.js";
import { accounts_body } from "./accounts.js";
import { analysis_body } from "./analysis.js";
import { tracking_body } from "./tracking.js";
import { planning, planning_datatable_context_menu } from "./planning_main.js";
import { config } from "./config.js";

import {
	transaction_notes_window, 
	init_transaction_notes_window
} from "./transaction_notes_window.js";
import { add_planning_window } from "./add_planning_window.js";
import { edit_planning_window } from "./edit_planning_window.js";


let header_container = {
	"gravity": 1,
	"borderless": true,
	"css": "header_container"
};


let left_nav_options = [
	{
		"name": "Budget Details", 
		"select": "budget_details", 
		"id": "budget_details_option",
		// "right_nav": "empty"
	},
	{
		"name": "Planning", 
		"select": "planning", 
		"id": "planning_option",
		// "right_nav": "empty"
	},
	{
		"name": "Accounts",
		"select": "accounts",
		"id": "accounts_option",
		// "right_nav": "empty"
	},
	{
		"name": "Analysis",
		"select": "analysis",
		"id": "analysis_option",
		// "right_nav": "empty"
	},
	{
		"name": "Tracking",
		"select": "tracking",
		"id": "tracking_option",
		// "right_nav": "empty"
	}
];


let left_nav_css_options = [
	{"key": "height", "value": 50},
	{"key": "borderless", "value": true},
	{"key": "css", "value": "left-hand-nav-css"},
	{"key": "align", "value": "center"}
];


function get_nav_options() {
	var nav_copy = left_nav_options;
	for (let i = 0; i < nav_copy.length; i++) {

		nav_copy[i]["view"] = "template";
		nav_copy[i]["template"] = nav_copy[i]["name"];
		nav_copy[i]["id"] = nav_copy[i]["id"];
		nav_copy[i]["height"] = 50;

		for (let j = 0; j < left_nav_css_options.length; j++) {
			nav_copy[i][left_nav_css_options[j]["key"]] = left_nav_css_options[j]["value"];
		}

		nav_copy[i]["onClick"] = {
			"left-hand-nav-css": function(ev, id) {
				$$("body_container").setValue(nav_copy[i]["select"]);
				// $$("right_nav_container").setValue(nav_copy[i]["right_nav"])
			}
		}

	}

	nav_copy.push({
		"gravity": "fill"
	});

	return nav_copy;
};


let left_nav_container = {
	"css": "left_nav_container",
	"gravity": 1,
	"rows": get_nav_options()
};


let body_container = {
	"gravity": 5,
	"rows": [
		{"height": 20},
		{
			"view": "multiview",
			"id": "body_container",
			"cells": [
				budget_body,
				planning,
				accounts_body,
				analysis_body,
				tracking_body
			],
			"ready": function() {
				// console.log("here")
			}, 
			"on": {
				"onViewShow": function() {
					console.log("here")
				},
				"onViewChange": function(prevID, nextID) {

					for (let i = 0; i < left_nav_options.length; i++) {
						if (left_nav_options[i]["select"] == nextID) {
							break;
						}
					}


					if (nextID == "analysis") {
						$$("pivot_piechart_view").draw_piechart();
					}

					$$("body_container")._cells.forEach((a) => {
						if (
							a.config.id == nextID && 
							a.config.hasOwnProperty("on_view_show")
						) {
							a.config.on_view_show();
						} 
					})
				}
			}
		},
		{
			"height": 20
		}
	]
	
};


let right_nav_container = {
	"gravity": 1,
	"css": "right_nav_container",
	"id": "right_nav_container",
	"view": "multiview",
	"cells": [
		{
			"id": "empty"
		}
	]
};


webix.ui({
	rows:[
		header_container, 
		{
			"gravity": 15,
			"cols": [
				left_nav_container,
				{
					"width": 20
				},
				body_container,
				{
					"width": 20
				}
			]
		}
	]
});

init_transaction_notes_window();
webix.ui(add_planning_window);
webix.ui(edit_planning_window);
webix.ui(planning_datatable_context_menu);

$$("planning_datatable_context_menu").attachTo($$("planning_datatable"));



window.addEventListener("dragover",function(e){
	e = e || event;
	e.preventDefault();
},false);

window.addEventListener("drop",function(e){
	e = e || event;
	e.preventDefault();
},false);

$$("upload_transaction_zone").$view.ondrop = transaction_drop_handler;

/* ########################################################################################### */

webix.ui({
	view:"contextmenu",
	id:"category_list_contextmenu",
	data:["Delete"],
	on:{
		onItemClick:function(id){
			var context = this.getContext();
			if (!context || !context.id) return;
			// console.log(context.id)
			fetch(globals.url + "/categories/" + context.id, {
				"method": 'DELETE',
				"headers": { 'Content-Type': 'application/json' }
			})
			.then(response => response.json())
			.then((data) => {
				return fetch(globals.url + "/categories", {
					"method": 'GET',
					"headers": { 'Content-Type': 'application/json' }
				})
			})
			.then(response => response.json())
			.then((data) => {
				// console.log(data);
				$$("category_list_view").clearAll();
				data.result.forEach((a) => {
					$$("category_list_view").add({
						"id": a["category_id"],
						"value": a["category_value"]
					});
				});

				$$("cumulative_transaction_table").update_all_row_categories();
			});
		}
	}
});

$$("category_list_contextmenu").attachTo($$("category_list_view"));

webix.ui({
	"view": "contextmenu",
	"id": "account_list_contextmenu",
	"data": ["Delete"],
	"on": {
		"onItemClick": function(id) {
			var context = this.getContext();
			if (!context || !context.id) return;

			fetch(globals.url + "/accounts/" + context.id, {
				"method": 'DELETE',
				"headers": { 'Content-Type': 'application/json' }
			})
			.then(response => response.json())
			.then((data) => {
				return fetch(globals.url + "/accounts", {
					"method": 'GET',
					"headers": { 'Content-Type': 'application/json' }
				})
			})
			.then(response => response.json())
			.then((data) => {
				// console.log(data);
				$$("account_list_view").clearAll();
				data.result.forEach((a) => {
					$$("account_list_view").add({
						"id": a["account_id"],
						"value": a["account_name"]
					});
				});

				$$("cumulative_transaction_table").update_all_row_categories();
			});
		}
	}
});

$$("account_list_contextmenu").attachTo($$("account_list_view"));
