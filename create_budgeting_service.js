

class bank_transaction_functions {
	constructor(dao) {
		this.dao = dao
	}

	create_table() {
		const sql = `
		CREATE TABLE IF NOT EXISTS 
			bank_transactions 
		(
			transaction_id TEXT PRIMARY KEY,
			transaction_date TEXT,
			transaction_ammount INTEGER,
			transaction_payee TEXT, 
			trasnsaction_category TEXT
		)
		`;
		return this.dao.run(sql);
	}

	get_all_transactions() {
		return this.dao.all(
		`
		SELECT 
			* 
		FROM 
			bank_transactions;
		`
		);
	}

	get_specific_transaction(transaction_id) {
		return this.dao.get(
			`
			SELECT 
				* 
			FROM 
				bank_transactions
			WHERE
				transaction_id = ?;
			`,
			[transaction_id]
		);
	}

	create(transaction_details) {
		return this.dao.run(
			`
			INSERT INTO 
				bank_transactions
			(
				transaction_id,
				transaction_date,
				transaction_ammount,
				transaction_payee,
				trasnsaction_category
			)
			VALUES
				(?,?,?,?,?);
			`,
			transaction_details
		);
	}

	update(transaction_details) {
		return this.dao.run(
			`
			UPDATE 
				bank_transactions
			SET 
				transaction_date = ?,
				transaction_ammount = ?,
				transaction_payee = ?,
				trasnsaction_category = ?
			WHERE 
				transaction_id = ?;
			`,
			transaction_details
		)
	}

	delete(transaction_id) {
		return this.dao.run(
			`
			DELETE FROM 
				bank_transactions 
			WHERE 
				transaction_id = ?;
			`,
			[transaction_id]
		)
	}
}

module.exports = bank_transaction_functions;