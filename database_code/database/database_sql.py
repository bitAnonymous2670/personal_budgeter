import os

CREATE_DATABASE_SQL_FILE = "../sql/create_budgeting_upgrade.sql"
DROP_DATABASE_SQL_FILE = "../sql/drop_budgeting_upgrade.sql"

def get_database_sql():
	if not os.path.exists(CREATE_DATABASE_SQL_FILE):
		return None

	sql_query = None

	with open(CREATE_DATABASE_SQL_FILE, "r") as f:
		sql_query = f.read()

	return sql_query