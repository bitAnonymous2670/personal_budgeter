import json, os
import utils
import database.database_sql_templates as database_sql_templates

LIST_OF_DATABASES = None

BASE = utils.get_base()

with open(os.sep.join([BASE, "/json_files/list_of_databases.json"])) as f:
	LIST_OF_DATABASES = json.loads(f.read())


class invalid_database_object_format(Exception):
	pass


def json_to_sql(database_info):
	if not "name" in database_info:
		msg = "Key \"name\" does not exist is variable \"database_info\""
		raise invalid_database_object_format(msg)
	

	return database_sql_templates.CREATE_DATABASE.format(
		**{
			"name": database_info["name"]
		}
	)


def drop_json_to_sql(database_info):
	if not "name" in database_info:
		msg = "Key \"name\" does not exist is variable \"database_info\""
		raise invalid_database_object_format(msg)
	

	return [
		database_sql_templates.CHECK_DATABASE_EXISTS.format(
			**{
				"name": database_info["name"]
			}
		),
		database_sql_templates.DROP_DATABASE.format(
			**{
				"name": database_info["name"]
			}
		)
	]


def create_database_sql():
	global LIST_OF_DATABASES

	return [json_to_sql(database_info) for database_info in LIST_OF_DATABASES]


def drop_database_sql():
	global LIST_OF_DATABASES

	return [drop_json_to_sql(database_info) for database_info in LIST_OF_DATABASES]


if __name__ == "__main__": 
	create_database_sql()