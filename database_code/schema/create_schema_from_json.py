import json, os
import utils
import schema.schema_sql_template as schema_sql_template


LIST_OF_SCHEMAS = None

"""
	TODO
		- comments?
"""

BASE = utils.get_base()

class invalid_schema_object_format(Exception):
	pass


with open(os.sep.join([BASE, "json_files/list_of_schemas.json"])) as f:
	LIST_OF_SCHEMAS = json.loads(f.read())


def json_to_sql(schema_info):
	if not "name" in schema_info:
		msg = "Key \"name\" does not exist is variable \"schema_info\""
		raise invalid_schema_object_format(msg)


	if not "database" in schema_info:
		msg = "Key \"database\" does not exist is variable \"schema_info\""
		raise invalid_schema_object_format(msg)

	return schema_sql_template.CREATE_SCHEMA.format(
		**{
			"name": schema_info["name"]
		}
	)

def owner_json_to_sql(schema_info):
	if not "name" in schema_info:
		msg = "Key \"name\" does not exist is variable \"schema_info\""
		raise invalid_schema_object_format(msg)


	if not "owner" in schema_info:
		msg = "Key \"owner\" does not exist is variable \"schema_info\""
		raise invalid_schema_object_format(msg)

	return schema_sql_template.SCHEMA_OWNER.format(
		**{
			"role_name": schema_info["owner"],
			"name": schema_info["name"]
		}
	)


def create_schema_sql():
	global LIST_OF_SCHEMAS

	create_sql = [json_to_sql(schema_info) for schema_info in LIST_OF_SCHEMAS]
	
	try:
		owners = [owner_json_to_sql(schema_info) for schema_info in LIST_OF_SCHEMAS]
		create_sql.extend(owners)
	except: 
		pass

	return create_sql


if __name__ == "__main__": 
	create_schema_sql()