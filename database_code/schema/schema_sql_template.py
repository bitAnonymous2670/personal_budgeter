CREATE_SCHEMA = """CREATE SCHEMA {name};"""

SCHEMA_OWNER = """ALTER SCHEMA {name} OWNER TO {role_name};"""