import psycopg2, inspect

CONFIG = None
SIMP_CONN_STR = None
CONN_STR = "dbname={dbname} user={dbuser} host={host} port={port}"

# This should be copied to all of the files
####################################################################################################

######################################### BASIC GETTERS AND SETTERS ################################
def set_config(config: dict):
	global CONFIG

	if not isinstance(config, dict):
		raise "Variable \"config\" is not of type dict."


	if not "database" in config:
		raise "Key \"database\" is not in variable \"config\"."


	if not "port" in config["database"]:
		raise "Key \"port\" is not in variable \"config[\"database\"]\"."


	if not "database" in config["database"]:
		raise "Key \"database\" is not in variable \"config[\"database\"]\"."


	if not "user" in config["database"]:
		raise "Key \"user\" is not in variable \"config[\"database\"]\"."


	if not "host" in config["database"]:
		raise "Key \"host\" is not in variable \"config[\"database\"]\"."


	CONFIG = config

	set_conn_string()


def get_config() -> dict:
	global CONFIG

	return CONFIG


def set_conn_string():
	global CONFIG, CONN_STR, SIMP_CONN_STR

	if CONFIG is None:
		return False

	SIMP_CONN_STR = CONN_STR.format(
		dbname=CONFIG["database"]["database"], 
		dbuser=CONFIG["database"]["user"], 
		host=CONFIG["database"]["host"], 
		port=CONFIG["database"]["port"]
	)


def get_conn_str() -> str:
	global SIMP_CONN_STR

	return SIMP_CONN_STR


def get_postgres_conn() -> psycopg2.extensions.connection:
	conn = psycopg2.connect(get_conn_str())
	conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
	return conn

######################################### END BASIC GETTERS AND SETTERS ###########################


def test_connection(conn: psycopg2.extensions.connection) -> int:
	assert(type(conn) == psycopg2.extensions.connection)

	curs = None

	try:
		curs = conn.cursor()
	except Exception as e:
		err_msg = "Unable to get cursor from connection. [create_db][{}]"
		print(err_msg.format(inspect.currentframe().f_lineno))
		return False

	assert(not curs is None)

	try:
		curs.execute("SELECT 1;")
	except Exception as e:
		print(e)
		print("[test_connection][{}]".format(inspect.currentframe().f_lineno))
		return False

	return True


def run_sql(conn: psycopg2.extensions.connection, sql_text: str) -> int:
	assert(type(conn) == psycopg2.extensions.connection)
	assert(type(sql_text) == str)

	curs = None

	test_connection(conn)

	try:
		curs = conn.cursor()
	except Exception as e:
		print("[run_sql][{}]".format(inspect.currentframe().f_lineno))
		print("Unable to get cursor from connection. [run_sql]")
		return None

	assert(not curs is None)

	try:

		curs.execute(sql_text)

	except Exception as e:
		print(sql_text)
		print(e)
		print("[run_sql][{}]".format(inspect.currentframe().f_lineno))
		return None

	return curs


def run_sql_with_params(conn: psycopg2.extensions.connection, sql_text: str, sql_params: tuple) -> int:
	assert(type(conn) == psycopg2.extensions.connection)
	assert(type(sql_text) == str)
	assert(type(sql_params) == tuple)

	curs = None

	test_connection(conn)

	try:
		curs = conn.cursor()
	except Exception as e:
		print("[run_sql][{}]".format(inspect.currentframe().f_lineno))
		print("Unable to get cursor from connection. [run_sql]")
		return None

	assert(not curs is None)

	try:

		curs.execute(sql_text, sql_params)

	except Exception as e:
		print(sql_text)
		print(e)
		print("[run_sql][{}]".format(inspect.currentframe().f_lineno))
		return None

	return curs


def run_sql_with_params_many(
	conn: psycopg2.extensions.connection, sql_text: str, sql_params: tuple
) -> int:
	assert(type(conn) == psycopg2.extensions.connection)
	assert(type(sql_text) == str)
	assert(type(sql_params) == list)
	assert(all([type(x) == tuple for x in sql_params]))

	curs = None

	test_connection(conn)

	try:
		curs = conn.cursor()
	except Exception as e:
		print("[run_sql][{}]".format(inspect.currentframe().f_lineno))
		print("Unable to get cursor from connection. [run_sql]")
		return None

	assert(not curs is None)

	try:

		curs.executemany(sql_text, sql_params)

	except Exception as e:
		print(sql_text)
		print(e)
		print("[run_sql][{}]".format(inspect.currentframe().f_lineno))
		return None

	return curs


####################################################################################################
