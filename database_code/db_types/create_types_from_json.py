import os, json
import utils
import db_types.types_sql_templates as types_sql_templates

class invalid_type_object_format(Exception):
	pass

class invalid_column_reference(Exception):
	pass

class invalid_type_passed(Exception):
	pass

BASE = utils.get_base()

CONFIG = None
LIST_OF_TYPES = None
TESTING_TYPES_FILE = os.sep.join([BASE, "json_files", "added_list_of_enums.json"])

with open(os.sep.join([BASE, "./json_files/list_of_enums.json"])) as f:
	# print(f.read())
	LIST_OF_TYPES = json.loads(f.read())


if not os.path.exists(os.sep.join([BASE, "config.json"])):
	exit("config file does not exist")

with open(os.sep.join([BASE, "config.json"]), "r") as f:
	CONFIG = json.loads(f.read())


if "build_testing" in CONFIG and CONFIG["build_testing"] and os.path.exists(TESTING_TYPES_FILE):
	with open(TESTING_TYPES_FILE, "r") as f:
		LIST_OF_TYPES.extend(json.loads(f.read()))


def json_to_sql(type_info):

# CHECK BASIC TYPES
	if not "name" in type_info:
		raise invalid_type_object_format("Key \"name\" does not exist is variable \"type_info\"")

	if not "fields" in type_info:
		raise invalid_type_object_format("Key \"fields\" does not exist is variable \"type_info\"")

	if not "schema" in type_info:
		raise invalid_type_object_format("Key \"schema\" does not exist is variable \"type_info\"")

	if not "type" in type_info:
		raise invalid_type_object_format("Key \"type\" does not exist is variable \"type_info\"")


# CEHCK IF ENUM
	if not type_info["type"] == "enum":
		msg = "Passed type, {} is not of type \"enum\", it is of type \"{}\""
		raise invalid_type_passed(msg.format(type_info["name"], type_info["type"]))


	for field in type_info["fields"]:
		if not isinstance(field, str):
			msg = "Passed field type, {} is not of type \"str\", it is of type \"{}\""
			raise invalid_type_passed(msg.format(field, type(field)))

	fields = ",".join([field for field in type_info["fields"]])

	return types_sql_templates.CREATE_TYPE_ENUM.format(
		**{
			"schema": type_info["schema"],
			"name": type_info["name"],
			"fields": ",".join(["'{}'".format(field) for field in type_info["fields"]])
		}
	)

def comment_json_to_sql(type_info):
	return types_sql_templates.COMMENT_ON_TYPE.format(
		**{
			"schema": type_info["schema"],
			"name": type_info["name"],
			"comment": type_info["comment"],
		}
	)


def owner_json_to_sql(type_info):
	return types_sql_templates.OWNER_ON_TYPE.format(
		**{
			"schema": type_info["schema"],
			"name": type_info["name"],
			"role_name": type_info["owner"]
		}
	)


def create_types_sql():
	global LIST_OF_TYPES

# CREATION
	create_sql = [json_to_sql(type_info) for type_info in LIST_OF_TYPES]

# COMMENT
	for type_info in LIST_OF_TYPES:
		if "comment" in type_info:
			create_sql.append(comment_json_to_sql(type_info))

# OWNER
	for type_info in LIST_OF_TYPES:
		if "owner" in type_info:
			create_sql.append(owner_json_to_sql(type_info))


	return create_sql


if __name__ == "__main__":
	create_types_sql()