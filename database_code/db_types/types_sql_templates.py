CREATE_TYPE_ENUM = """

	CREATE TYPE {schema}.{name} AS ENUM
	({fields});

"""


COMMENT_ON_TYPE = """
	
	COMMENT ON TYPE {schema}.{name} IS '{comment}';

"""


OWNER_ON_TYPE = """

	ALTER TYPE {schema}.{name} OWNER TO {role_name};
"""