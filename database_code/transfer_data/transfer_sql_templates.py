"""
	TODO:
		- Remember what to do!
"""


SELECT_SCHEMA_SQL = """
	SELECT {columns}
	FROM {schema}.{name};
"""

SELECT_POSTGRES_SQL = """
	SELECT {columns}
	FROM {name};
"""

"""

cars = (
    (1, 'Audi', 52642),
    (2, 'Mercedes', 57127),
    (3, 'Skoda', 9000),
    (4, 'Volvo', 29000),
    (5, 'Bentley', 350000),
    (6, 'Citroen', 21000),
    (7, 'Hummer', 41400),
    (8, 'Volkswagen', 21600)
)

"INSERT INTO cars (id, name, price) VALUES (%s, %s, %s)"

"""

INSERT_SCHEMA = """
	INSERT INTO {schema}.{name} ({columns})
	VALUES ({values});
"""