import os, json
import utils

import transfer_data.transfer_sql_templates as transfer_sql_templates

import connection_functions

# select x from y
# insert into z values x

BASE = utils.get_base()
CONFIG = None
TRANSFER_TABLES_FILE = os.sep.join([BASE, "json_files", "list_of_transfer_tables.json"])

if not os.path.exists(os.sep.join([BASE, "config.json"])):
	exit("config file does not exist")

with open(os.sep.join([BASE, "config.json"])) as f:
	CONFIG = json.loads(f.read())

LIST_OF_TRANSFERS = None

if not os.path.exists(TRANSFER_TABLES_FILE):
	exit("{} does not exist.".format(TRANSFER_TABLES_FILE))


with open(TRANSFER_TABLES_FILE, "r") as f:
	LIST_OF_TRANSFERS = json.loads(f.read())


def get_from_conn(transfer_dict):

	if "from" not in transfer_dict["database"]:
		exit("Problem")

	CONFIG["database"]["database"] = transfer_dict["database"]["from"]
	connection_functions.set_config(CONFIG)
	return connection_functions.get_postgres_conn()


def get_to_conn(transfer_dict):
	
	if "to" not in transfer_dict["database"]:
		exit("Problem")

	CONFIG["database"]["database"] = transfer_dict["database"]["to"]
	connection_functions.set_config(CONFIG)
	return connection_functions.get_postgres_conn()


def format_columns(transfer_dict, key):
	return ", ".join([x[key] for x in transfer_dict["column_names"]])

def get_from_sql(transfer_dict):

	sql_query = None

	if "from" in transfer_dict["schema"]:
		sql_query = transfer_sql_templates.SELECT_SCHEMA_SQL.format(
			**{
				"columns": format_columns(transfer_dict, "from"),
				"schema": transfer_dict["schema"]["from"],
				"name": transfer_dict["table_names"]["from"]
			}
		)
	else:
		sql_query = transfer_sql_templates.SELECT_POSTGRES_SQL.format(
			**{
				"columns": format_columns(transfer_dict, "from"),
				"name": transfer_dict["table_names"]["from"]
			}
		)

	return sql_query


def get_to_sql(transfer_dict):
	
	sql_query = transfer_sql_templates.INSERT_SCHEMA.format(
		**{
			"schema": transfer_dict["schema"]["to"],
			"name": transfer_dict["table_names"]["to"],
			"columns": format_columns(transfer_dict, "to"),
			"values": ",".join(["%s" for _ in range(len(transfer_dict["column_names"]))])
		}
	)

	return sql_query




def put_everything_together(transfer_dict):

	if "database" not in transfer_dict:
		exit("Problem")
		
	if "schema" not in transfer_dict:
		exit("Problem")

	if "table_names" not in transfer_dict:
		exit("Problem")

	if "column_names" not in transfer_dict:
		exit("Problem")


	from_conn = get_from_conn(transfer_dict)
	to_conn = get_to_conn(transfer_dict)
	from_sql = get_from_sql(transfer_dict)
	to_sql = get_to_sql(transfer_dict)

	curs = connection_functions.run_sql(from_conn, from_sql)
	values = curs.fetchall()
	curs.close()

	curs = connection_functions.run_sql_with_params_many(to_conn, to_sql, values)
	curs.close()



def transfer_table_sql():
	global TRANSFER_TABLES_FILE

	[put_everything_together(t) for t in LIST_OF_TRANSFERS]

	return []

if __name__ == "__main__":
	transfer_table_sql()