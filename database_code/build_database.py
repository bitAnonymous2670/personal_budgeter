import os, json, datetime

"""
https://www.endpoint.com/blog/2013/11/21/copying-rows-between-postgresql
	ORDER
		- DATABASE
		- ROLES
		- USERS
		- SCHEMA
		- TYPES
		- TABLES
		- TRIGGERS
		- FUNCTIONS
		- TRIGGERS
		- PERMISSIONS
		- DATA

		- VIEWS


"""

import connection_functions

CONFIG = None
BASE = os.path.dirname(os.path.abspath(__file__))

if not os.path.exists(os.sep.join([BASE, "config.json"])):
	exit("config file does not exist")


with open(os.sep.join([BASE, "config.json"]), "r") as f:
	CONFIG = json.loads(f.read())
	connection_functions.set_config(CONFIG)
	

import database.create_db_from_json as create_db_from_json
import schema.create_schema_from_json as create_schema_from_json
# import db_types.create_types_from_json as create_types_from_json
import tables.create_tables_from_json as create_tables_from_json

# import init_data.create_data_from_json as create_data_from_json
# import views.create_views_from_json as create_views_from_json
import transfer_data.transfer_data as transfer_data

METRIC_LIST = []

"""
	NOTES
		- Change "json_files/list_of_databases.json" to the desired db
		- Change "json_files/list_of_schemas.json" to the desired db
		- Change "json_files/list_of_transfer_tables.json" to the desired db
		- Check "json_files/list_of_transfer_tables.json" that all the tables have transfers

"""


def create_everything():
	global CONFIG


## SWITCH TO POSTGRES TO DROP/CREATE THE DB
	CONFIG["database"]["database"] = CONFIG["upgrade_db"]["database"]
	connection_functions.set_config(CONFIG)
	try:
		conn = connection_functions.get_postgres_conn()

		if not connection_functions.test_connection(conn):
			exit("Can not connect to the DB")

## CHECK THE NUMBER OF CONNECTIONS AND RAISE AN ERROR
		curs = conn.cursor()
		curs.execute("SELECT sum(numbackends) FROM pg_stat_database;")
		conn_number = curs.fetchall()[0][0]
		print(conn_number)
		if conn_number != 1:
			exit("There is an open connection to DB {}".format(conn))
		curs.close()
		conn.close()
	except Exception as ex:
		print(ex)



## SWITCH TO POSTGRES TO DROP/CREATE THE DB
	CONFIG["database"]["database"] = "postgres"
	connection_functions.set_config(CONFIG)
	conn = connection_functions.get_postgres_conn()

	if not connection_functions.test_connection(conn):
		exit("Can not connect to the DB")

# DROP EVERYTHING ################

# DATABASE
#
#	CHECK IF THE DBs EXIST, THEN DELETE THEM
#
	for [check_db, databases] in create_db_from_json.drop_database_sql():
		check_curs = connection_functions.run_sql(conn, check_db)
		db_exists = check_curs.fetchall()
		if len(db_exists) != 0 and db_exists[0][0] == 1:
			connection_functions.run_sql(conn, databases)

################################# 
#
# CREATE EVERYTHING ################
#
# CREATE DATABASE	
	for databases in create_db_from_json.create_database_sql():
		connection_functions.run_sql(conn, databases)

# SWITCH TO SIMPLICITY FOR THE REST OF IT.
	conn.close()
	CONFIG["database"]["database"] = CONFIG["upgrade_db"]["database"]

	connection_functions.set_config(CONFIG)
	conn = connection_functions.get_postgres_conn()


# CREATE SCHEMA
	for schema in create_schema_from_json.create_schema_sql():
		curs = connection_functions.run_sql(conn, schema)
		curs.close()


# CREATE TABLES
	for table in create_tables_from_json.create_table_sql():
		curs = connection_functions.run_sql(conn, table)
		curs.close()

# TRANSFER DATA
	for table in transfer_data.transfer_table_sql():
		pass

	conn.close()
#################################


def test_evrything():
	test_controller.run()



if __name__ == "__main__":
	# test_controller.create_everything = create_everything
	create_everything()
	# test_evrything()

