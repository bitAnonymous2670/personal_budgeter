CREATE OR REPLACE FUNCTION assign_tag_to_transaction(in_trans_id INTEGER, in_tag_id INTEGER)
RETURNS INTEGER AS $function$
DECLARE
    joining_id INTEGER;
BEGIN

    joining_id = NULL;

    SELECT ttjt.join_id
    INTO joining_id
    FROM trans_tags_joining_table ttjt
    WHERE ttjt.tag_id = in_tag_id AND ttjt.trans_id = in_trans_id;

    IF joining_id IS NOT NULL THEN
        RETURN -1;          -- ALREADY EXISTS
    END IF;

    INSERT INTO trans_tags_joining_table (trans_id, tag_id)
    VALUES (in_trans_id, in_tag_id)
    RETURNING join_id INTO joining_id;

    RETURN joining_id;
END;
$function$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION assign_new_tag_to_transaction(in_trans_id INTEGER, in_tag_json jsonb)
RETURNS INTEGER AS $function$
DECLARE
    in_tag_id INTEGER;
    in_joining_id INTEGER;
BEGIN
    SELECT *
    INTO in_tag_id
    FROM insert_tag(in_tag_json);

    SELECT *
    INTO in_joining_id
    FROM assign_tag_to_transaction(in_trans_id, in_tag_id);

    return in_joining_id;
END;
$function$ LANGUAGE plpgsql;


/*
    SELECT * FROM assign_new_tag_to_transaction(
        insert_transaction('{"trans_value": "new_trans"}'::jsonb), '{"tag_value": "Phone"}'::jsonb
    );

*/