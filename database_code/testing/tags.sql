CREATE OR REPLACE FUNCTION insert_tag(in_value jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_tag_id TEXT;
BEGIN

    in_tag_id = NULL;

    IF in_value->>'tag_id' IS NULL THEN
        RETURN NULL;
    END IF;

    IF in_value->>'tag_value' IS NULL THEN
        RETURN NULL;
    END IF;

    SELECT t.tag_id
    INTO in_tag_id
    FROM tags t
    WHERE t.tag_value = in_value->>'tag_value';

    IF in_tag_id IS NOT NULL THEN
        RETURN in_tag_id;
    END IF;

    INSERT INTO tags (tag_value)
    VALUES (in_value->>'tag_value')
    RETURNING tag_id INTO in_tag_id;

    RETURN in_tag_id;

END;
$function$ LANGUAGE plpgsql;

-- mass tags 
CREATE OR REPLACE PROCEDURE insert_tags(IN in_tags jsonb[], INOUT out_bank_tranactions_ids TEXT[])
LANGUAGE plpgsql AS $$
BEGIN

    SELECT ARRAY(
        SELECT insert_tag(val) from unnest(in_tags) as val
    ) INTO out_bank_tranactions_ids;

    IF -1 = ANY(out_bank_transaction_ids) THEN
        RAISE 'malformed';
    END IF;

END $$;

CREATE OR REPLACE FUNCTION update_tag(in_value jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_tag_id TEXT;
    in_tag_value TEXT;
BEGIN

-- DEFAULT EVERYTHING  
    in_tag_id = NULL;
    in_tag_value = NULL;

-- REQUIRED
    IF in_value->>'tag_id' IS NULL THEN
        RETURN -1;
    END IF;

-- SET VALUES
    SELECT t.tag_id, t.tag_value
    INTO in_tag_id, in_tag_value
    FROM tags t
    WHERE t.tag_value = in_value->>'tag_id';

    IF in_tag_id IS NULL THEN
        RETURN -1;
    END IF;

-- OPTIONAL
    IF in_value->>'tag_value' IS NOT NULL THEN
        in_tag_value = in_value->>'tag_value';
    END IF;

-- UPDATE VALUES
    UPDATE tags
    SET tag_value = in_tag_value
    WHERE tag_id = in_tag_id;

    RETURN in_tag_id;

END;
$function$ LANGUAGE plpgsql;

-- mass tags 
CREATE OR REPLACE PROCEDURE update_tags(IN in_tags jsonb[], INOUT out_bank_tranactions_ids TEXT[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT update_tag(val) from unnest(in_tags) as val
    ) INTO not_success_result;

    IF -1 = ANY(out_bank_transaction_ids) THEN
        RAISE 'malformed';
    END IF;

END $$;


CREATE OR REPLACE FUNCTION delete_tag(in_tag_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
    in_tag_id_confirm TEXT;
BEGIN

    in_tag_id_confirm = NULL;
    
    SELECT t.tag_id
    INTO in_tag_id_confirm
    FROM tags t
    WHERE t.tag_value = in_tag_id;

    IF in_tag_id_confirm IS NULL THEN
        RETURN in_tag_id;
    END IF;

    DELETE FROM tags
    WHERE tag_id = in_tag_id_confirm;

    RETURN in_tag_id_confirm;

END;
$function$ LANGUAGE plpgsql;

-- mass tags 
CREATE OR REPLACE PROCEDURE delete_tags(IN in_tags jsonb[], INOUT out_bank_tranactions_ids TEXT[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT delete_tag(val) from unnest(in_tags) as val
    ) INTO not_success_result;

    IF -1 = ANY(out_bank_transaction_ids) THEN
        RAISE 'malformed';
    END IF;

END $$;


CREATE OR REPLACE PROCEDURE mass_procedure_call(IN call_tag TEXT, IN proc_args jsonb[])
RETURNS INTEGER AS $function$
DECLARE
    out_ids TEXT[];
BEGIN
    
    out_ids = NULL;

    IF call_tag = 'INSERT_TAGS' THEN 
        BEGIN
            CALL insert_tags(proc_args, out_ids);
            RETURN out_ids;
        EXCEPTION
            WHEN OTHERS THEN NULL;
        END;
    END IF;

    IF call_tag = 'INSERT_TAGS' THEN 
        BEGIN
            CALL update_tags(proc_args, out_ids);
            RETURN out_ids;
        EXCEPTION
            WHEN OTHERS THEN NULL;
        END;
    END IF;

    IF call_tag = 'INSERT_TAGS' THEN 
        BEGIN
            CALL delete_tags(proc_args, out_ids);
            RETURN out_ids;
        EXCEPTION
            WHEN OTHERS THEN NULL;
        END;
    END IF;

    RETURN out_ids;
END;
$function$ LANGUAGE plpgsql;

/*
    -- GET ALL TAGS
    SELECT t.tag_value
    FROM tags t
    LEFT JOIN trans_tags_joining_table ttjt ON t.tag_id = ttjt.tag_id
    WHERE ttjt.trans_id = 2;

*/