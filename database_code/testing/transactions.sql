CREATE OR REPLACE FUNCTION insert_bank_transaction(in_bank_transaction jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_transaction_id TEXT;
    in_transaction_notes TEXT;
BEGIN

    in_transaction_id = NULL;
    in_transaction_notes = NULL;

-- REQUIRED
    IF in_bank_transaction->>'transaction_id' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_bank_transaction->>'transaction_date' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_bank_transaction->>'transaction_amount' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_bank_transaction->>'transaction_payee' IS NULL THEN
        RETURN -1;
    END IF;

-- OPTIONAL
    IF in_bank_transaction->>'transaction_notes' IS NOT NULL THEN
        in_transaction_notes = in_bank_transaction->>'transaction_notes';
    END IF;


-- CHECK IF TRANSACTION EXISTS
    SELECT bt.transaction_id
    INTO in_transaction_id
    FROM bank_transactions bt
    WHERE bt.transaction_date = in_bank_transaction->>'transaction_date'
    AND bt.transaction_amount = in_bank_transaction->>'transaction_amount'
    AND bt.transaction_payee = in_bank_transaction->>'transaction_payee';

    IF in_transaction_id IS NOT NULL THEN
        RETURN in_transaction_id;
    END IF;

-- INSERT TRANSACTION
    INSERT INTO bank_transactions (
        transaction_id,
        transaction_date,
        transaction_amount,
        transaction_payee,
        transaction_notes
    )
    VALUES (
        in_bank_transaction->>'transaction_id',
        in_bank_transaction->>'transaction_date',
        in_bank_transaction->>'transaction_amount',
        in_bank_transaction->>'transaction_payee',
        in_transaction_notes
    )
    RETURNING transaction_id INTO in_transaction_id;

    RETURN in_transaction_id;

END;
$function$ LANGUAGE plpgsql;

-- mass account 
CREATE OR REPLACE PROCEDURE insert_bank_transactions(IN in_bank_transactions jsonb[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT insert_bank_transaction(val) from unnest(in_bank_transactions) as val
    ) INTO not_success_result;

    IF not_success_result THEN
        ROLLBACK;
        RAISE 'malformed';
    END IF;

END $$;

CREATE OR REPLACE FUNCTION update_bank_transaction(in_bank_transaction jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_transaction_id TEXT;
    in_transaction_date DATE;
    in_transaction_amount INTEGER;
    in_transaction_payee TEXT;
    in_transaction_notes TEXT;
BEGIN

    in_transaction_id = NULL;
    in_transaction_notes = NULL;
    in_transaction_date = NULL;
    in_transaction_amount = NULL;
    in_transaction_payee = NULL;

-- DEFAULT EVERYTHING
    IF in_bank_transaction->>'transaction_id' IS NULL THEN
        RETURN -1;
    END IF;


-- SET VALUES
    SELECT 
        bt.transaction_id,
        bt.transaction_date,
        bt.transaction_amount,
        bt.transaction_payee,
        bt.transaction_notes
    INTO 
        in_transaction_id
        in_transaction_date
        in_transaction_amount
        in_transaction_payee
        in_transaction_notes
    FROM bank_transactions bt
    WHERE bt.transaction_id = in_bank_transaction->>'transaction_id'

    IF in_transaction_id IS NULL THEN
        RETURN -1;
    END IF;

-- OPTIONAL
    IF in_bank_transaction->>'transaction_notes' IS NOT NULL THEN
        in_transaction_notes = in_bank_transaction->>'transaction_notes';
    END IF;

    IF in_bank_transaction->>'transaction_date' IS NULL THEN
        in_transaction_date = in_bank_transaction->>'transaction_date';
    END IF;

    IF in_bank_transaction->>'transaction_amount' IS NULL THEN
        in_transaction_amount = in_bank_transaction->>'transaction_amount';
    END IF;

    IF in_bank_transaction->>'transaction_payee' IS NULL THEN
        in_transaction_payee = in_bank_transaction->>'transaction_payee';
    END IF;
        
-- UPDATE VALUES
    UPDATE bank_transactions
    SET 
        transaction_date = in_transaction_date;
        transaction_amount = in_transaction_amount;
        transaction_payee = in_transaction_payee;
        transaction_notes = in_transaction_notes;
    WHERE
        transaction_id = in_transaction_id;

    RETURN in_transaction_id;

END;
$function$ LANGUAGE plpgsql;

-- mass update 
CREATE OR REPLACE PROCEDURE update_bank_transactions(IN in_bank_transactions jsonb[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT update_bank_transaction(val) from unnest(in_bank_transactions) as val
    ) INTO not_success_result;

    IF not_success_result THEN
        ROLLBACK;
        RAISE 'malformed';
    END IF;

END $$;


-- GET ALL ACCT_TRANS
SELECT *
FROM bank_transactions t
LEFT JOIN trans_tags_joining_table ttjt ON t.trans_id = ttjt.trans_id
WHERE ttjt.tag_id = 2;



-- CALL insert_account_transactions(ARRAY['{"trans_value": "one_more_one"}'::jsonb, 
--        '{"trans_value": "two_more"}'::jsonb]);

/*
    SELECT * FROM insert_transaction('{"trans_value": "new_trans"}'::jsonb);

    CALL insert_account_transactions(
        ARRAY['{"trans_value": "one_more_one"}'::jsonb, '{"trans_value": "two_more"}'::jsonb]
    );


    -- GET ALL ACCT_TRANS
    SELECT *
    FROM account_transactions t
    LEFT JOIN trans_tags_joining_table ttjt ON t.trans_id = ttjt.trans_id
    WHERE ttjt.tag_id = 2;


    GET
        - 1 
        - ALL (PAGINATED)
            - DATE
            - PAYEE
            - CATEGORY
            - AMOUNT
            - ACCOUNT
            - TAG

    INSERT
        - 1
        - MANY (array in body, not a problem)

    UPDATE
        - 1
        - MANY (array in body, not a problem)

    DELETE
        - 1
        - MANY (array)
*/
