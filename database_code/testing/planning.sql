-- insert single account
CREATE OR REPLACE FUNCTION insert_plan(in_value jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_plan_id TEXT;
    in_category_id TEXT;
BEGIN

    in_plan_id = NULL;


    IF in_value->>'planning_id' IS NULL THEN
        RETURN -1;
    END IF;


    IF in_value->>'category_id' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_value->>'planned_amount' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_value->>'begin_planning_date' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_value->>'end_planning_date' IS NULL THEN
        RETURN -1;
    END IF;


-- NO INTERSECTIONS
    SELECT cp.planning_id
    INTO in_plan_id
    FROM category_planning cp
    WHERE cp.category_id = in_value->>'category_id'
    AND (
/*
        CP BEGIN BETWEEN in_value->>'begin_planning_date' AND in_value->>'end_planning_date' OR
        CP END BETWEEN in_value->>'begin_planning_date' AND in_value->>'end_planning_date'
*/
    )

    IF in_plan_id IS NOT NULL THEN
        RETURN -1;
    END IF;

    SELECT cp.planning_id
    INTO in_plan_id
    FROM category_planning cp
    WHERE cp.category_id = in_value->>'category_id'
    AND cp.begin_planning_date = in_value->>'category_id'
    AND cp.end_planning_date = in_value->>'end_planning_date';

    IF in_plan_id IS NOT NULL THEN
        RETURN in_plan_id;
    END IF;

    INSERT INTO category_planning (
        planning_id, 
        category_id,
        planned_amount,
        begin_planning_date,
        end_planning_date
    )
    VALUES (
        in_value->>'planning_id', 
        in_value->>'category_id',
        in_value->>'planned_amount', 
        in_value->>'begin_planning_date',
        in_value->>'end_planning_date'
    )
    RETURNING planning_id INTO in_plan_id;

    RETURN in_plan_id;

END;
$function$ LANGUAGE plpgsql;


-- insert many accounts
CREATE OR REPLACE PROCEDURE insert_plans(IN in_plans jsonb[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT insert_plan(val) from unnest(in_plans) as val
    ) INTO not_success_result;

    IF not_success_result THEN
        ROLLBACK;
        RAISE 'malformed';
    END IF;

END $$;