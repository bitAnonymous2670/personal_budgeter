import os, sys, json, random, uuid
import psycopg2
from psycopg2.extras import Json
import collections




"""
	TODO 
		- make file directory an input for passing args
		- REALLY FIX THE ERROR LOGGING!! MAKE IT ROBUST
"""
CONN_STR = "dbname={} user={} host={} port={}"


def get_file_contents(file_name):
	if not os.path.exists(file_name): exit()

	with open(file_name, "r") as f:
		FILE_CONTENTS = f.read().split(chr(30))

	table_headers = FILE_CONTENTS[0].split(chr(29))
	table_contents = [x.split(chr(29)) for x in FILE_CONTENTS[1:] if x != ""]

	table_contents = [
		{table_headers[i]: x[i] for i in range(len(table_headers))} 
		for x in table_contents
	]

	return {
		"table_headers": table_headers,
		"table_contents": table_contents
	}

def run_insert_test(CONFIG, options):

## CHECK KEYS IN CONFIG
	if "upgrade_db" not in CONFIG: exit()

	sub_keys = [
		"database",
		"user",
		"host",
		"port"
	]

	for sub_key in sub_keys:
		if sub_key not in CONFIG["upgrade_db"]: exit()

## CHECK KEYS IN OPTIONS
	option_keys = [
		"file_name"
	]

	for option_key in option_keys:
		if option_key not in options: exit()

	file_contents = get_file_contents(file_name=options["file_name"])

## CREATE SAVE POINT 
	SAVE_POINT = str(uuid.uuid4()).replace("-","")[:8]
	DB_NAME = CONFIG["upgrade_db"]["database"]

	conn_str = CONN_STR.format(
		DB_NAME,
		CONFIG["upgrade_db"]["user"],
		CONFIG["upgrade_db"]["host"],
		CONFIG["upgrade_db"]["port"]
	)

	conn = psycopg2.connect(conn_str)
	curs = conn.cursor()
	curs.execute("SET search_path TO budget;")
	curs.execute("SAVEPOINT {};".format(SAVE_POINT))


	for x in file_contents["table_contents"]:
		curs.execute("SELECT COUNT(*) FROM {}.budget.bank_transactions".format(DB_NAME))
		total = curs.fetchall()[0][0]
		curs.execute(sql_str, (json.dumps(x),))
		ret = curs.fetchall()
		curs.execute("SELECT COUNT(*) FROM {}.budget.bank_transactions".format(DB_NAME))
		total_two = curs.fetchall()[0][0]
		assert(total_two == total + 1)
		curs.execute("ROLLBACK TO SAVEPOINT {};".format(SAVE_POINT))

	return None


DB_NAME = "budgeting_upgrade_alt"
USER = "postgres"
HOST = "192.168.1.151"
PORT = "5432"
CONN_STR = "dbname={} user={} host={} port={}"

conn_str = CONN_STR.format(DB_NAME, USER, HOST, PORT)

FILE_NAME = os.sep + os.sep.join([
	"stream_backup",
	"budget_db_backup",
	"2021-08-16",
	"bank_transactions_2021_08_16_01.backup"
])

FILE_CONTENTS = None

with open(FILE_NAME, "r") as f:
	FILE_CONTENTS = f.read().split(chr(30))

table_headers = FILE_CONTENTS[0].split(chr(29))
table_contents = [x.split(chr(29)) for x in FILE_CONTENTS[1:] if x != ""]

table_contents = [
	{table_headers[i]: x[i] for i in range(len(table_headers))} 
	for x in table_contents
]

sql_str = "SELECT * FROM budgeting_upgrade_alt.budget.insert_bank_transaction(%s);"

conn = psycopg2.connect(conn_str)
curs = conn.cursor()
curs.execute("SET search_path TO budget;")
curs.execute("SAVEPOINT before_everything;")


for x in table_contents:
	curs.execute("SELECT COUNT(*) FROM budgeting_upgrade_alt.budget.bank_transactions")
	total = curs.fetchall()[0][0]
	# print(total)
	curs.execute(sql_str, (json.dumps(x),))
	ret = curs.fetchall()
	curs.execute("SELECT COUNT(*) FROM budgeting_upgrade_alt.budget.bank_transactions")
	total_two = curs.fetchall()[0][0]
	assert(total_two == total + 1)
	curs.execute("ROLLBACK TO SAVEPOINT before_everything;")


curs.execute("SELECT COUNT(*) FROM budgeting_upgrade_alt.budget.bank_transactions")
total = curs.fetchall()[0][0]
# all of them
sql_str = "SELECT * FROM budgeting_upgrade_alt.budget.insert_bank_transactions(%s::jsonb[]);"

table_contents_two = table_contents
table_contents = [Json(x) for x in table_contents]
curs.execute(sql_str, (table_contents,))
ret = curs.fetchall()[0][0]
dups = [item for item, count in collections.Counter(ret).items() if count > 1]
print("\n".join([str(x) for x in table_contents_two if x["transaction_id"] not in ret]))
print("\n".join([str(x) for x in table_contents_two if x["transaction_id"] in dups]))
assert(len(ret) == len(table_contents))
curs.execute("SELECT COUNT(*) FROM budgeting_upgrade_alt.budget.bank_transactions")
total_two = curs.fetchall()[0][0]
print(total_two,total,len(table_contents))
assert(total_two == total + len(table_contents))
curs.execute("ROLLBACK TO SAVEPOINT before_everything;")

