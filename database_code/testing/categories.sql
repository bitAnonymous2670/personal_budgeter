-- insert single account
CREATE OR REPLACE FUNCTION insert_category(in_value jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_category_id TEXT;
BEGIN

    in_category_id = NULL;

    IF in_value->>'category_value' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_value->>'category_id' IS NULL THEN
        RETURN -1;
    END IF;

    SELECT cvc.category_id
    INTO in_category_id
    FROM category_view_contents cvc
    WHERE cvc.category_value = in_value->>'category_value';

    IF in_category_id IS NOT NULL THEN
        RETURN in_category_id;
    END IF;

    INSERT INTO category_view_contents (category_id, category_value)
    VALUES (in_value->>'category_id', in_value->>'category_value')
    RETURNING category_id INTO in_category_id;

    RETURN in_category_id;

END;
$function$ LANGUAGE plpgsql;


-- insert many accounts
CREATE OR REPLACE PROCEDURE insert_categories(IN in_categories jsonb[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT insert_category(val) from unnest(in_categories) as val
    ) INTO not_success_result;

    IF not_success_result THEN
        ROLLBACK;
        RAISE 'malformed';
    END IF;

END $$;