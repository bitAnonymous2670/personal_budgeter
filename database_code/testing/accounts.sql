-- insert single account
CREATE OR REPLACE FUNCTION insert_account(in_value jsonb)
RETURNS TEXT AS $function$
DECLARE
    in_account_id TEXT;
BEGIN

    in_account_id = NULL;

    IF in_value->>'account_name' IS NULL THEN
        RETURN -1;
    END IF;

    IF in_value->>'account_id' IS NULL THEN
        RETURN -1;
    END IF;

    SELECT ad.account_id
    INTO in_account_id
    FROM account_details ad
    WHERE ad.account_name = in_value->>'account_name';

    IF in_account_id IS NOT NULL THEN
        RETURN in_account_id;
    END IF;

    INSERT INTO account_details (account_id, account_name)
    VALUES (in_value->>'account_id', in_value->>'account_name')
    RETURNING account_id INTO in_account_id;

    RETURN in_account_id;

END;
$function$ LANGUAGE plpgsql;


-- insert many accounts
CREATE OR REPLACE PROCEDURE insert_accounts(IN in_accounts jsonb[])
LANGUAGE plpgsql AS $$
DECLARE
    not_success_result BOOLEAN;
BEGIN

    SELECT -1 = ANY(
        SELECT insert_account(val) from unnest(in_accounts) as val
    ) INTO not_success_result;

    IF not_success_result THEN
        ROLLBACK;
        RAISE 'malformed';
    END IF;

END $$;