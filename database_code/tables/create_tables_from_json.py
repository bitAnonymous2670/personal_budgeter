import os, json
import utils
import tables.table_sql_templates as table_sql_templates


class invalid_table_object_format(Exception):
	pass

class invalid_column_reference(Exception):
	pass


BASE = utils.get_base()
CONFIG = None
TESTING_TABLES_FILE = os.sep.join([BASE, "json_files", "added_list_of_tables.json"])

if not os.path.exists(os.sep.join([BASE, "config.json"])):
	exit("config file does not exist")


with open(os.sep.join([BASE, "config.json"])) as f:
	CONFIG = json.loads(f.read())


LIST_OF_TABLES 		= None

with open(os.sep.join([BASE, "json_files/list_of_tables.json"])) as f:
	LIST_OF_TABLES = json.loads(f.read())


if "build_testing" in CONFIG and CONFIG["build_testing"] and os.path.exists(TESTING_TABLES_FILE):
	with open(TESTING_TABLES_FILE, "r") as f:
		LIST_OF_TABLES.extend(json.loads(f.read()))



def order_table_list():
	global LIST_OF_TABLES


	table_requirements = {table["name"]: [] for table in LIST_OF_TABLES}

	for table_info in LIST_OF_TABLES:
		if "foreign_keys" in table_info:
			table_requirements[table_info["name"]] = [
				fk["table_name"] 
				for fk in table_info["foreign_keys"] 
				if fk["table_name"] != table_info["name"]
			]
		else:
			table_requirements[table_info["name"]] = []

	written, unwritten, max_loops = [], [table["name"] for table in LIST_OF_TABLES], 1000
	while len(unwritten) != 0 and max_loops != 0:
		current = unwritten.pop(0)
		max_loops -= 1

		if len([dep for dep in table_requirements[current] if not dep in written]) != 0:
			unwritten.extend([current])
			continue

		written.append(current)


	LIST_OF_TABLES = sorted(LIST_OF_TABLES, key=lambda x: written.index(x["name"]))


def format_default(col):
	if not "default" in col:
		return ""

	return " DEFAULT " + col["default"]

def format_constraints(col):
	if not "constraints" in col or col["constraints"] is None or len(col["constraints"]) == 0:
		return ""

	return " " + " ".join(
		sorted(col["constraints"], key=lambda x:table_sql_templates.CONSTRAINTS_ORDER.index(x))
	)



def format_table_comment(table_info):
	return table_sql_templates.CREATE_COMMENT_ON_TABLE.format(
		**{
			"schema": table_info["schema"],
			"name": table_info["name"],
			"comment": table_info["comment"]
		}
	)


def combined_column_comments(comment_obj):
	return table_sql_templates.CREATE_COMMENT_ON_COLUMN.format(
		**{
			"schema": comment_obj["schema"],
			"name": comment_obj["name"],
			"col": comment_obj["col"],
			"comment": comment_obj["comment"]
		}
	)


def combined_foreign_keys(table_info, foreign_keys):
	return ",\n".join(
		[
			table_sql_templates.FOREIGN_KEY_CONSTRAINT.format(
				**{
					"schema": table_info["schema"],
					"column": foreign_key["column"],
					"table_name": foreign_key["table_name"],
					"table_col": foreign_key["column_name"]
				}
			) for foreign_key in foreign_keys
		]
	)


def combined_sql_columns(columns):
	return ",\n".join(
		[
			table_sql_templates.FORMAT_COLUMN.format(
				**{
					"col_name": col["name"],
					"col_dtype": col["dtype"],
					"constraints": format_constraints(col),
					"default": format_default(col)
				}
			) for col in columns
		]
	)


def combined_sql_constraints(combined_constraints):
	return ",\n".join(
		[
			table_sql_templates.COMBINED_CONSTRAINT_LINE.format(
				**{
					"constraint": combined_constraint["constraint"],
					"cols": ", ".join(
						[
							col_name for col_name in combined_constraint["cols"]
						]
					)
				}
			) for combined_constraint in combined_constraints
		]
	)


def full_sql_table_format(table_info, col_def):
	return table_sql_templates.CREATE_TABLE.format(
		**{
			"schema": table_info["schema"],
			"name": table_info["name"],
			"cols": col_def
		}
	)




def json_to_sql(table_info):
	if not "name" in table_info:
		raise invalid_table_object_format("Key \"name\" does not exist is variable \"table_info\"")

	if not "cols" in table_info:
		raise invalid_table_object_format("Key \"cols\" does not exist is variable \"table_info\"")

	for col in table_info["cols"]:

		if not "name" in col:
			raise invalid_table_object_format("Key \"name\" does not exist is variable \"col\"")

		if not "dtype" in col:
			raise invalid_table_object_format("Key \"type\" does not exist is variable \"col\"")

	if not "schema" in table_info:
		raise "Key \"schema\" does not exist is variable \"table_info\""



# COLUMN DEFINITIONS
	col_defs = combined_sql_columns(table_info["cols"])

# COMBINED COUMN CONSTRAINTS
	if "combined_constraints" in table_info:

		for combined_constraint in table_info["combined_constraints"]:


# CHECKING FOR VALID COMBINED CONSTRAINT FORMAT
			if not "constraint" in combined_constraint:
				msg = "Key \"constraint\" does not exist is variable \"combined_constraint\""
				raise invalid_table_object_format(msg)

			if not "cols" in combined_constraint:
				msg = "Key \"cols\" does not exist is variable \"combined_constraint\""
				raise invalid_table_object_format(msg)


# CHECKING THAT ALL THE COLUMNS EXIST
			for col_name in combined_constraint["cols"]:
				if [col["name"] for col in table_info["cols"]].index(col_name) == -1:
					msg = "Column \"{}\" does not exist in table \"\"".format(
						col_name, table_info["name"]
					)
					raise invalid_column_reference(msg)



		combined_constrainsts = combined_sql_constraints(table_info["combined_constraints"])

		col_defs = ",\n".join([col_defs, combined_constrainsts])


# COMBINE FOREIGN KEY CONSTRAINTS
	if "foreign_keys" in table_info:

		for foreign_key in table_info["foreign_keys"]:

# CHECKING FOR VALID FOREIGN KEY OBJECT FORMAT
			if not "column" in foreign_key:
				msg = "Key \"column\" does not exist is variable \"foreign_key\""
				raise invalid_table_object_format(msg)

			if not "table_name" in foreign_key:
				msg = "Key \"table_name\" does not exist is variable \"foreign_key\""
				raise invalid_table_object_format(msg)

			if not "column_name" in foreign_key:
				msg = "Key \"column_name\" does not exist is variable \"foreign_key\""
				raise invalid_table_object_format(msg)


		foreign_keys = combined_foreign_keys(table_info, table_info["foreign_keys"])

		col_defs = ",\n".join([col_defs, foreign_keys])


# COMBINING TABLE AND COLUMN COMMENTS
	col_comments, comments = [], []
	if "comment" in table_info:
		comments.append(format_table_comment(table_info))

	for col in table_info["cols"]:
		if "comment" in col:
			col_comments.append(
				combined_column_comments(
					{
						"schema": 	table_info["schema"],
						"name": 	table_info["name"],
						"col": 		col["name"],
						"comment": 	col["comment"]
					}
				)
			)

	comments.extend(col_comments)
	comments = "\n".join(comments)


# FULL TABLE SQL
	return "\n".join([full_sql_table_format(table_info, col_defs), comments])


def table_comment_json_to_sql(table_info):
	return table_sql_templates.CREATE_COMMENT_ON_TABLE.format(
		**{
			"schema": table_info["schema"],
			"name": table_info["name"],
			"comment": table_info["comment"],
		}
	)


def column_comment_json_to_sql(table_info, col_info):
	return table_sql_templates.CREATE_COMMENT_ON_COLUMN.format(
		**{
			"schema": table_info["schema"],
			"name": table_info["name"],
			"col": col_info["name"],
			"comment": col_info["comment"],
		}
	)


def owner_json_to_sql(table_info):
	return table_sql_templates.OWNER_ON_TABLE.format(
		**{
			"schema": table_info["schema"],
			"name": table_info["name"],
			"role_name": table_info["owner"]
		}
	)



def create_table_sql():
	global LIST_OF_TABLES

	order_table_list()

#  CREATE
	create_sql = [json_to_sql(table_info) for table_info in LIST_OF_TABLES]

# COMMENT
#	TABLE
	for table_info in LIST_OF_TABLES:
		if "comment" in table_info:
			create_sql.append(table_comment_json_to_sql(table_info))
# 	COLUMN
		for col_info in table_info["cols"]:
			if "comment" in col_info:
				create_sql.append(column_comment_json_to_sql(table_info, col_info))

# OWNER
	for table_info in LIST_OF_TABLES:
		if "owner" in table_info:
			create_sql.append(owner_json_to_sql(table_info))

	return create_sql
		

if __name__ == "__main__": 
	create_table_sql()