"""
	TODO
		- add comments to tables
		- add comments to columns
		- add owner to tables
		- add foreign_keys
		- handle the case when tables have types that are custom

"""


"""
FOREIGN KEY () REFERENCES {} ({})

"""



CREATE_TABLE = """
	
	CREATE TABLE IF NOT EXISTS {schema}.{name} (
		{cols}
	);

"""

CREATE_COMMENT_ON_TABLE = """
	
	COMMENT ON TABLE {schema}.{name} IS '{comment}';

"""

CREATE_COMMENT_ON_COLUMN = """

	COMMENT ON COLUMN {schema}.{name}.{col} IS '{comment}';

"""


FORMAT_COLUMN = "{col_name}		{col_dtype}{constraints}{default}"

COMBINED_CONSTRAINT_LINE = "{constraint}({cols})"

FOREIGN_KEY_CONSTRAINT = "FOREIGN KEY ({column}) REFERENCES {schema}.{table_name} ({table_col}) ON DELETE CASCADE"

OWNER_ON_TABLE = """

	ALTER TABLE {schema}.{name} OWNER TO {role_name};
"""


CONSTRAINTS_ORDER = [
	"PRIMARY KEY",
	"FOREIGN KEY",
	"UNIQUE",
	"NOT NULL",
	"NULL"
]