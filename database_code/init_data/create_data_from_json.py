import json, os
import utils
import init_data.data_sql_templates as data_sql_templates

BASE = utils.get_base()
LIST_OF_DATA = None


with open(os.sep.join([BASE, "json_files/initial_data_inserts.json"])) as f:
	LIST_OF_DATA = json.loads(f.read())


class invalid_data_object_format(Exception):
	pass


def convert_array(val):
	if isinstance(val, str):
		return "'{}'".format(val)
	elif isinstance(val, list):
		return "{" + "'{}'".format("','".join(val)) + "}"
	else:
		exit("ERR!!! VAL IS NOT STRING NOR ARRAY!!!")

def combine_cols(col_values):
	return ",".join([convert_array(col_values[key]) for key in col_values])



def json_to_sql(data_info):
	if not "function" in data_info:
		msg = "Key \"function\" does not exist is variable \"data_info\""
		raise invalid_data_object_format(msg)


	if not "schema" in data_info:
		msg = "Key \"schema\" does not exist is variable \"data_info\""
		raise invalid_data_object_format(msg)


	if not "rows" in data_info:
		msg = "Key \"rows\" does not exist is variable \"data_info\""
		raise invalid_data_object_format(msg)


	for row in data_info["rows"]:
		for key in row:
			if not isinstance(row[key], str):
				msg = "Invalid format."
				invalid_data_object_format(msg)


	return [
		data_sql_templates.RUN_FUNCTION_SQL.format(
			**{
				"schema": data_info["schema"],
				"function": data_info["function"],
				"cols": combine_cols(col_values)
			}
		) for col_values in data_info["rows"]
	]


def create_data_sql():
	global LIST_OF_DATA

	return sum([json_to_sql(data_info) for data_info in LIST_OF_DATA], [])


if __name__ == "__main__": 
	create_data_sql()