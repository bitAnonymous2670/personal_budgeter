import os, json
import utils
import views.views_sql_template as views_sql_template


BASE = utils.get_base()
CONFIG = None
LIST_OF_VIEWS = []
VIEWS_FILE = os.sep.join([BASE, "json_files", "list_of_views.json"])
TESTING_VIEWS_FILE = os.sep.join([BASE, "json_files", "added_list_of_views.json"])


if os.path.exists(VIEWS_FILE):
	with open(VIEWS_FILE) as f:
		# print(f.read())
		LIST_OF_VIEWS = json.loads(f.read())


with open(os.sep.join([BASE, "config.json"])) as f:
	CONFIG = json.loads(f.read())


if "build_testing" in CONFIG and CONFIG["build_testing"] and os.path.exists(TESTING_VIEWS_FILE):
	with open(TESTING_VIEWS_FILE, "r") as f:
		LIST_OF_VIEWS.extend(json.loads(f.read()))




def json_to_sql(view_info):

# CHECK BASIC TYPES
	if not "name" in view_info:
		raise invalid_type_object_format("Key \"name\" does not exist is variable \"view_info\"")

	if not "sql" in view_info:
		raise invalid_type_object_format("Key \"sql\" does not exist is variable \"view_info\"")

	if not "schema" in view_info:
		raise invalid_type_object_format("Key \"schema\" does not exist is variable \"view_info\"")


	return views_sql_template.CREATE_VIEW.format(
		**{
			"schema": view_info["schema"],
			"name": view_info["name"],
			"sql": view_info["sql"]
		}
	)





def create_views_sql():
	global LIST_OF_VIEWS

# CREATION
	create_sql = [json_to_sql(view_info) for view_info in LIST_OF_VIEWS]


	return create_sql


if __name__ == "__main__":
	create_types_sql()