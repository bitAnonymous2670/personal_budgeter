import os, sys, json, arrow

import connection_functions

CONFIG = None
BASE = os.path.dirname(os.path.abspath(__file__))

if not os.path.exists(os.sep.join([BASE, "config.json"])):
	exit("config file does not exist")


with open(os.sep.join([BASE, "config.json"]), "r") as f:
	CONFIG = json.loads(f.read())
	CONFIG["database"]["database"] = CONFIG["upgrade_db"]["database"]
	connection_functions.set_config(CONFIG)

get_all_tbls = """ SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname = '{}';"""
get_all_tbl_cols = """

	SELECT column_name FROM information_schema.columns
	WHERE table_schema = '{}' AND table_name   = '{}';

"""


if __name__ == "__main__":

	tables = []
	conn = connection_functions.get_postgres_conn()
	curs = conn.cursor()

	curs.execute(get_all_tbls.format(CONFIG["upgrade_db"]["schema"]))

	for row in curs.fetchall():
		tbl = row[0]

		curs_2 = conn.cursor()

		curs_2.execute(get_all_tbl_cols.format(CONFIG["upgrade_db"]["schema"], tbl))

		table_columns = [x[0] for x in curs_2.fetchall()]
		tables.append({
			"schema": CONFIG["upgrade_db"]["schema"],
			"table": tbl,
			"table_columns": table_columns
		})

		curs_2.close()
	curs.close()

	
	backup_folder = arrow.utcnow().format("YYYY-MM-DD")
	backup_folder = os.sep.join([CONFIG["backup"], backup_folder])

	if not os.path.exists(backup_folder):
		os.mkdir(backup_folder, mode=0o777)

	for table in tables:
		file_name = "{}_{}".format(table["table"], arrow.utcnow().format("YYYY_MM_DD_HH"))
		print(file_name)
		with open(os.sep.join([backup_folder, "{}.backup".format(file_name)]), "w+") as f:
			curs = conn.cursor()

			formatted_cols = ",".join(table["table_columns"])
			f.write("{}".format(chr(29).join(formatted_cols.split(","))) + chr(30))
			curs.execute("SELECT {} FROM {}.{};".format(
				formatted_cols,
				table["schema"],
				table["table"]
			))

			f.write(chr(30).join([chr(29).join([str(y) for y in list(x)]) for x in curs.fetchall()]))
			curs.close()
