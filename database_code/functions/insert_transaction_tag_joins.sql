CREATE OR REPLACE FUNCTION insert_transaction_tag_joins(in_transaction_tag_joins jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_transaction_tag_ids TEXT[];
BEGIN

	out_transaction_tag_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT insert_transaction_tag_join(val) 
			FROM unnest(in_transaction_tag_joins) AS val
		) INTO out_transaction_tag_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_transaction_tag_ids;

END; 
$function$ LANGUAGE plpgsql;