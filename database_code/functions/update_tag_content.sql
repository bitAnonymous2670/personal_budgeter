CREATE OR REPLACE FUNCTION update_tag_content(in_tag_content jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_tag_content_id TEXT;
	in_tag_content_value TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_tag_content_id = NULL;
	in_tag_content_value = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for tag_contents.';

-- DEFAULT EVERYTHING
	IF in_tag_content->>'tag_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key tag_id.',
			hint = err_hint;
	END IF;


-- SET VALUES
	SELECT 
		tc.tag_id,
		tc.tag_value
	INTO 
		in_tag_content_id,
		in_tag_content_value
	FROM tag_contents tc
	WHERE tc.tag_id = in_tag_content->>'tag_id';

	IF in_tag_content_id IS NULL THEN
		RETURN -1;
	END IF;

-- OPTIONAL
	IF in_tag_content->>'tag_value' IS NOT NULL THEN
		in_tag_content_value = in_tag_content->>'tag_value';
	END IF;
		
-- UPDATE VALUES
	UPDATE tag_contents
	SET 
		tag_value = in_tag_content_value
	WHERE
		tag_id = in_tag_content_id;

	RETURN in_tag_content_id;

END;
$function$ LANGUAGE plpgsql;