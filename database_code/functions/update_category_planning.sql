CREATE OR REPLACE FUNCTION update_category_planning(in_category_planning jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_planning_id TEXT;
	in_category_id TEXT;
	in_planned_amount INTEGER;
	in_begin_planning_date DATE;
	in_end_planning_date DATE;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_planning_id = NULL;
	in_category_id = NULL;
	in_planned_amount = NULL;
	in_begin_planning_date = NULL;
	in_end_planning_date = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for bank transactions.';

-- DEFAULT EVERYTHING
	IF in_category_planning->>'planning_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key planning_id.',
			hint = err_hint;
	END IF;


-- SET VALUES
	SELECT 
		cp.planning_id,
		cp.category_id,
		cp.planned_amount,
		cp.begin_planning_date,
		cp.end_planning_date
	INTO 
		in_planning_id,
		in_category_id,
		in_planned_amount,
		in_begin_planning_date,
		in_end_planning_date
	FROM category_planning cp
	WHERE cp.planning_id = in_category_planning->>'planning_id';

	IF in_planning_id IS NULL THEN
		RETURN -1;
	END IF;

-- OPTIONAL

	IF in_category_planning->>'category_id' IS NULL THEN
		in_category_id = in_category_planning->>'category_id';
	END IF;

	IF in_category_planning->>'planned_amount' IS NULL THEN
		in_planned_amount = CAST(in_category_planning->>'planned_amount' AS INTEGER);
	END IF;

	IF in_category_planning->>'begin_planning_date' IS NULL THEN
		in_begin_planning_date = TO_DATE(
			in_category_planning->>'begin_planning_date', 'YYYY-MM-DD'
		);
	END IF;

	IF in_category_planning->>'end_planning_date' IS NOT NULL THEN
		in_end_planning_date = TO_DATE(
			in_category_planning->>'end_planning_date', 'YYYY-MM-DD'
		);
	END IF;
		
-- UPDATE VALUES
	UPDATE category_planning
	SET 
		category_id = in_category_id,
		planned_amount = in_planned_amount,
		begin_planning_date = in_begin_planning_date,
		end_planning_date = in_end_planning_date
	WHERE
		planning_id = in_planning_id;

	RETURN in_planning_id;

END;
$function$ LANGUAGE plpgsql;