CREATE OR REPLACE FUNCTION update_tag_contents(in_tag_contents jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_tag_content_ids TEXT[];
BEGIN

	out_tag_content_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT update_tag_content(val) 
			FROM unnest(in_tag_contents) AS val
		) INTO out_tag_content_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_tag_content_ids;
	
END; 
$function$ LANGUAGE plpgsql;