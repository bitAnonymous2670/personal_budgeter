CREATE OR REPLACE FUNCTION delete_transaction_account_joins(in_transaction_account_ids jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_transaction_account_ids TEXT[];
BEGIN

	out_transaction_account_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT delete_transaction_account_join(val) 
			FROM unnest(in_transaction_account_ids) AS val
		) INTO out_transaction_account_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_transaction_account_ids;
	
END; 
$function$ LANGUAGE plpgsql;