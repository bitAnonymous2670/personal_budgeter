CREATE OR REPLACE FUNCTION delete_category_planning(in_category_planning_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_category_planning_id TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_category_planning_id = NULL;

-- MAKE SURE IT EXISTS
	SELECT cp.planning_id
	INTO in_category_planning_id
	FROM category_planning cp
	WHERE cp.planning_id = in_category_planning_id;

	IF in_category_planning_id IS NULL THEN
		RETURN in_category_planning_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM category_planning
	WHERE planning_id = in_category_planning_id;

-- RETURN CONFIRMATION
	RETURN in_category_planning_id;

END;
$function$ LANGUAGE plpgsql;