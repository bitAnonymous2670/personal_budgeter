CREATE OR REPLACE FUNCTION insert_transaction_category_joins(in_transaction_category_joins jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_transaction_category_ids TEXT[];
BEGIN

	out_transaction_category_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT insert_transaction_category_join(val) 
			FROM unnest(in_transaction_category_joins) AS val
		) INTO out_transaction_category_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_transaction_category_ids;

END; 
$function$ LANGUAGE plpgsql;