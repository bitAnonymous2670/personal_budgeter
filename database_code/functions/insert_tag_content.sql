CREATE OR REPLACE FUNCTION insert_tag_content(in_tag_content jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_tag_content_id TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_tag_content_id = NULL;
	

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for tag_contents.';

-- REQUIRED
	IF in_tag_content->>'tag_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key tag_id.',
			hint = err_hint;
	END IF;

	IF in_tag_content->>'tag_value' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key tag_value.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	


-- CHECK IF TRANSACTION EXISTS
	SELECT tc.tag_id
	INTO in_tag_content_id
	FROM tag_contents tc
	WHERE tc.tag_value = in_tag_content->>'tag_value';

	IF in_tag_content_id IS NOT NULL THEN
		RETURN in_tag_content_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO tag_contents (
		tag_id,
		tag_value
	)
	VALUES (
		in_tag_content->>'tag_id',
		in_tag_content->>'tag_value'
	)
	RETURNING tag_id INTO in_tag_content_id;
	

	RETURN in_tag_content_id;

END;
$function$ LANGUAGE plpgsql;