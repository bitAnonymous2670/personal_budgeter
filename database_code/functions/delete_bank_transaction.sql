CREATE OR REPLACE FUNCTION delete_bank_transaction(in_bank_transaction_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_bank_transaction_id_confirm TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_bank_transaction_id_confirm = NULL;

-- MAKE SURE IT EXISTS
	SELECT bt.transaction_id
	INTO in_bank_transaction_id_confirm
	FROM bank_transactions bt
	WHERE bt.transaction_id = in_bank_transaction_id;

	IF in_bank_transaction_id_confirm IS NULL THEN
		RETURN in_bank_transaction_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM bank_transactions
	WHERE transaction_id = in_bank_transaction_id_confirm;

-- RETURN CONFIRMATION
	RETURN in_bank_transaction_id_confirm;

END;
$function$ LANGUAGE plpgsql;