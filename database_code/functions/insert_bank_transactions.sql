CREATE OR REPLACE FUNCTION insert_bank_transactions(in_bank_transactions jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_bank_transaction_ids TEXT[];
BEGIN

	out_bank_transaction_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT insert_bank_transaction(val) FROM unnest(in_bank_transactions) AS val
		) INTO out_bank_transaction_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_bank_transaction_ids;

END; 
$function$ LANGUAGE plpgsql;