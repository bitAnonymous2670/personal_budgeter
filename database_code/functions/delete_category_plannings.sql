CREATE OR REPLACE FUNCTION delete_category_plannings(in_category_plannings jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_category_planning_ids TEXT[];
BEGIN

	out_category_planning_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT delete_category_planning(val) FROM unnest(in_category_plannings) AS val
		) INTO out_category_planning_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_category_planning_ids;
	
END; 
$function$ LANGUAGE plpgsql;