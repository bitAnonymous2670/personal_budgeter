CREATE OR REPLACE FUNCTION update_category_view_content(in_category_view_content jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_category_view_content_id TEXT;
	in_category_view_content_value TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_category_view_content_id = NULL;
	in_category_view_content_value = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for category view contents.';

-- DEFAULT EVERYTHING
	IF in_category_view_content->>'category_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key category_id.',
			hint = err_hint;
	END IF;


-- SET VALUES
	SELECT 
		cvc.category_id,
		cvc.category_value
	INTO 
		in_category_view_content_id,
		in_category_view_content_value
	FROM category_view_contents cvc
	WHERE cvc.category_id = in_category_view_content->>'category_id';

	IF in_category_view_content_id IS NULL THEN
		RETURN -1;
	END IF;

-- OPTIONAL
	IF in_category_view_content->>'category_value' IS NOT NULL THEN
		in_category_view_content_value = in_category_view_content->>'category_value';
	END IF;
		
-- UPDATE VALUES
	UPDATE category_view_contents
	SET 
		category_value = in_category_view_content_value
	WHERE
		category_id = in_category_view_content_id;

	RETURN in_category_view_content_id;

END;
$function$ LANGUAGE plpgsql;