CREATE OR REPLACE FUNCTION update_account_detail(in_account_detail jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_account_detail_id TEXT;
	in_account_detail_name TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_account_detail_id = NULL;
	in_account_detail_name = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for account details.';

-- DEFAULT EVERYTHING
	IF in_account_detail->>'account_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key account_id.',
			hint = err_hint;
	END IF;


-- SET VALUES
	SELECT 
		ad.account_id,
		ad.account_name
	INTO 
		in_account_detail_id,
		in_account_detail_name
	FROM account_details ad
	WHERE ad.account_id = in_account_detail->>'account_id';

	IF in_account_detail_id IS NULL THEN
		RETURN -1;
	END IF;

-- OPTIONAL
	IF in_account_detail->>'account_name' IS NOT NULL THEN
		in_account_detail_name = in_account_detail->>'account_name';
	END IF;
		
-- UPDATE VALUES
	UPDATE account_details
	SET 
		account_name = in_account_detail_name
	WHERE
		account_id = in_account_detail_id;

	RETURN in_account_detail_id;

END;
$function$ LANGUAGE plpgsql;