CREATE OR REPLACE FUNCTION delete_transaction_account_join(in_transaction_account_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_transaction_account_id_confirm TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_transaction_account_id_confirm = NULL;

-- MAKE SURE IT EXISTS
	SELECT taj.joining_id
	INTO in_transaction_account_id_confirm
	FROM transaction_account_joining_table taj
	WHERE taj.joining_id = in_transaction_account_id;

	IF in_transaction_account_id_confirm IS NULL THEN
		RETURN in_transaction_account_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM transaction_account_joining_table
	WHERE joining_id = in_transaction_account_id_confirm;

-- RETURN CONFIRMATION
	RETURN in_transaction_account_id_confirm;

END;
$function$ LANGUAGE plpgsql;