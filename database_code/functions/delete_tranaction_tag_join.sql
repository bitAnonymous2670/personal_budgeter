CREATE OR REPLACE FUNCTION delete_transaction_tag_join(in_transaction_tag_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_transaction_tag_id_confirm TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_transaction_tag_id_confirm = NULL;

-- MAKE SURE IT EXISTS
	SELECT ttj.joining_id
	INTO in_transaction_tag_id_confirm
	FROM transaction_tag_joining_table ttj
	WHERE ttj.joining_id = in_transaction_tag_id;

	IF in_transaction_tag_id_confirm IS NULL THEN
		RETURN in_transaction_tag_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM transaction_tag_joining_table
	WHERE joining_id = in_transaction_tag_id_confirm;

-- RETURN CONFIRMATION
	RETURN in_transaction_tag_id_confirm;

END;
$function$ LANGUAGE plpgsql;