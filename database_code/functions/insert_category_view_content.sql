CREATE OR REPLACE FUNCTION insert_category_view_content(in_category_view_content jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_category_view_content_id TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_category_view_content_id = NULL;
	

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for category view contents.';

-- REQUIRED
	IF in_category_view_content->>'category_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key category_id.',
			hint = err_hint;
	END IF;

	IF in_category_view_content->>'category_value' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key category_value.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	


-- CHECK IF TRANSACTION EXISTS
	SELECT cvc.category_id
	INTO in_category_view_content_id
	FROM category_view_contents cvc
	WHERE cvc.category_value = in_category_view_content->>'category_value';

	IF in_category_view_content_id IS NOT NULL THEN
		RETURN in_category_view_content_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO category_view_contents (
		category_id,
		category_value
	)
	VALUES (
		in_category_view_content->>'category_id',
		in_category_view_content->>'category_value'
	)
	RETURNING category_id INTO in_category_view_content_id;
	

	RETURN in_category_view_content_id;

END;
$function$ LANGUAGE plpgsql;