CREATE OR REPLACE FUNCTION update_bank_transaction(in_bank_transaction jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_transaction_id TEXT;
	in_transaction_date DATE;
	in_transaction_amount INTEGER;
	in_transaction_payee TEXT;
	in_transaction_notes TEXT;
	in_transaction_line INTEGER;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_transaction_id = NULL;
	in_transaction_notes = NULL;
	in_transaction_date = NULL;
	in_transaction_amount = NULL;
	in_transaction_payee = NULL;
	in_transaction_line = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for bank transactions.';

-- DEFAULT EVERYTHING
	IF in_bank_transaction->>'transaction_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_id.',
			hint = err_hint;
	END IF;


-- SET VALUES
	SELECT 
		bt.transaction_id,
		bt.transaction_date,
		bt.transaction_amount,
		bt.transaction_payee,
		bt.transaction_notes,
		bt.transaction_line
	INTO 
		in_transaction_id,
		in_transaction_date,
		in_transaction_amount,
		in_transaction_payee,
		in_transaction_notes,
		in_transaction_line
	FROM bank_transactions bt
	WHERE bt.transaction_id = in_bank_transaction->>'transaction_id';

	IF in_transaction_id IS NULL THEN
		RETURN -1;
	END IF;

-- OPTIONAL
	IF in_bank_transaction->>'transaction_notes' IS NOT NULL THEN
		in_transaction_notes = in_bank_transaction->>'transaction_notes';
	END IF;

	IF in_bank_transaction->>'transaction_date' IS NULL THEN
		in_transaction_date = in_bank_transaction->>'transaction_date';
	END IF;

	IF in_bank_transaction->>'transaction_amount' IS NULL THEN
		in_transaction_amount = in_bank_transaction->>'transaction_amount';
	END IF;

	IF in_bank_transaction->>'transaction_payee' IS NULL THEN
		in_transaction_payee = in_bank_transaction->>'transaction_payee';
	END IF;

	IF in_bank_transaction->>'transaction_line' IS NULL THEN
		in_transaction_line = in_bank_transaction->>'transaction_line';
	END IF;
		
-- UPDATE VALUES
	UPDATE bank_transactions
	SET 
		transaction_date = in_transaction_date,
		transaction_amount = in_transaction_amount,
		transaction_payee = in_transaction_payee,
		transaction_notes = in_transaction_notes,
		transaction_line = in_transaction_line
	WHERE
		transaction_id = in_transaction_id;

	RETURN in_transaction_id;

END;
$function$ LANGUAGE plpgsql;