CREATE OR REPLACE FUNCTION delete_tag_content(in_tag_content_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_tag_content_id_confirm TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_tag_content_id_confirm = NULL;

-- MAKE SURE IT EXISTS
	SELECT tc.category_id
	INTO in_tag_content_id_confirm
	FROM tag_contents tc
	WHERE tc.category_id = in_tag_content_id;

	IF in_tag_content_id_confirm IS NULL THEN
		RETURN in_tag_content_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM tag_contents
	WHERE category_id = in_tag_content_id_confirm;

-- RETURN CONFIRMATION
	RETURN in_tag_content_id_confirm;

END;
$function$ LANGUAGE plpgsql;