CREATE OR REPLACE FUNCTION delete_category_view_content(in_category_view_content_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_category_view_content_id_confirm TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_category_view_content_id_confirm = NULL;

-- MAKE SURE IT EXISTS
	SELECT cvc.category_id
	INTO in_category_view_content_id_confirm
	FROM category_view_contents cvc
	WHERE cvc.category_id = in_category_view_content_id;

	IF in_category_view_content_id_confirm IS NULL THEN
		RETURN in_category_view_content_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM category_view_contents
	WHERE category_id = in_category_view_content_id_confirm;

-- RETURN CONFIRMATION
	RETURN in_category_view_content_id_confirm;

END;
$function$ LANGUAGE plpgsql;