CREATE OR REPLACE FUNCTION delete_category_view_contents(in_category_view_contents jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_category_view_content_ids TEXT[];
BEGIN

	out_category_view_content_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT delete_category_view_content(val) 
			FROM unnest(in_category_view_contents) AS val
		) INTO out_category_view_content_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_category_view_content_ids;
	
END; 
$function$ LANGUAGE plpgsql;