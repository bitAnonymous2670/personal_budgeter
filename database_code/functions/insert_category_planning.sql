CREATE OR REPLACE FUNCTION insert_category_planning(in_category_planning jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_planning_id TEXT;
	in_planned_amount INTEGER;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_planning_id = NULL;
	in_planned_amount = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for bank transactions.';

-- REQUIRED
	IF in_category_planning->>'in_planning_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key in_planning_id.',
			hint = err_hint;
	END IF;


	IF in_category_planning->>'in_category_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key in_category_id.',
			hint = err_hint;
	END IF;

	
	IF in_category_planning->>'planned_amount' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key planned_amount.',
			hint = err_hint;
	END IF;

	in_planned_amount = CAST(in_category_planning->>'planned_amount' AS INTEGER)

	
	IF in_category_planning->>'begin_planning_date' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key begin_planning_date.',
			hint = err_hint;
	END IF;


	IF in_category_planning->>'end_planning_date' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key end_planning_date.',
			hint = err_hint;
	END IF;

	

-- OPTIONAL


-- CHECK FOR PROBLEMS
	SELECT cp.in_planning_id
	INTO in_planning_id
	FROM category_planning cp
	WHERE (
		cp.begin_planning_date BETWEEN 
		TO_DATE(in_category_planning->>'begin_planning_date', 'YYYY-MM-DD') AND 
		TO_DATE(in_category_planning->>'end_planning_date', 'YYYY-MM-DD')
		OR
		cp.end_planning_date BETWEEN 
		TO_DATE(in_category_planning->>'begin_planning_date', 'YYYY-MM-DD') AND 
		TO_DATE(in_category_planning->>'end_planning_date', 'YYYY-MM-DD')
		OR 
		(
			TO_DATE(
				in_category_planning->>'end_planning_date', 'YYYY-MM-DD'
			) <= cp.end_planning_date
			AND
			cp.begin_planning_date <= TO_DATE(
				in_category_planning->>'begin_planning_date', 'YYYY-MM-DD'
			)
		)
	) AND cp.category_id =  in_category_planning->>'in_category_id';

	IF in_planning_id IS NOT NULL THEN
		-- WARN OF ERROR
	END IF;

-- CHECK IF TRANSACTION EXISTS
	

-- INSERT TRANSACTION
	INSERT INTO bank_transactions (
		planning_id,
		category_id,
		planned_amount,
		begin_planning_date,
		end_planning_date
	)
	VALUES (
		in_category_planning->>'in_planning_id',
		in_category_planning->>'in_category_id',
		in_planned_amount,
		TO_DATE(in_category_planning->>'begin_planning_date', 'YYYY-MM-DD'),
		TO_DATE(in_category_planning->>'end_planning_date', 'YYYY-MM-DD')	
	)
	RETURNING planning_id INTO in_planning_id;

	RETURN in_planning_id;

END;
$function$ LANGUAGE plpgsql;