CREATE OR REPLACE FUNCTION insert_bank_transaction(in_bank_transaction jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_transaction_id TEXT;
	in_transaction_notes TEXT;
	in_transaction_amount INTEGER;
	in_transaction_line INTEGER;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

	in_transaction_id = NULL;
	in_transaction_notes = NULL;
	in_transaction_amount = NULL;
	in_transaction_line = NULL;

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for bank transactions.';

-- REQUIRED
	IF in_bank_transaction->>'transaction_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_id.',
			hint = err_hint;
	END IF;

	IF in_bank_transaction->>'transaction_date' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_date.',
			hint = err_hint;
	END IF;

	IF in_bank_transaction->>'transaction_amount' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_amount.',
			hint = err_hint;
	END IF;

	in_transaction_amount = CAST(in_bank_transaction->>'transaction_amount' AS INTEGER);

	IF in_bank_transaction->>'transaction_payee' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_payee.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	IF in_bank_transaction->>'transaction_notes' IS NOT NULL THEN
		in_transaction_notes = in_bank_transaction->>'transaction_notes';
	END IF;

	IF in_bank_transaction->>'transaction_line' IS NOT NULL THEN
		in_transaction_line = in_bank_transaction->>'transaction_line';
	END IF;


-- CHECK IF TRANSACTION EXISTS
	SELECT bt.transaction_id
	INTO in_transaction_id
	FROM bank_transactions bt
	WHERE bt.transaction_date = TO_DATE(in_bank_transaction->>'transaction_date', 'YYYY-MM-DD')
	AND bt.transaction_amount = in_transaction_amount
	AND bt.transaction_payee = in_bank_transaction->>'transaction_payee'
	AND bt.transaction_line = in_bank_transaction->>'transaction_line';

	IF in_transaction_id IS NOT NULL THEN
		RETURN in_transaction_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO bank_transactions (
		transaction_id,
		transaction_date,
		transaction_amount,
		transaction_payee,
		transaction_notes,
		transaction_line
	)
	VALUES (
		in_bank_transaction->>'transaction_id',
		TO_DATE(in_bank_transaction->>'transaction_date', 'YYYY-MM-DD'),
		in_transaction_amount,
		in_bank_transaction->>'transaction_payee',
		in_transaction_notes,
		in_transaction_line
	)
	RETURNING transaction_id INTO in_transaction_id;

	RETURN in_transaction_id;

END;
$function$ LANGUAGE plpgsql;