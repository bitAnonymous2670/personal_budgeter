CREATE OR REPLACE FUNCTION delete_account_detail(in_account_detail_id TEXT) 
RETURNS TEXT AS $function$
DECLARE 
	in_account_detail_id_confirm TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_account_detail_id_confirm = NULL;

-- MAKE SURE IT EXISTS
	SELECT ad.account_id
	INTO in_account_detail_id_confirm
	FROM account_details ad
	WHERE ad.account_id = in_account_detail_id;

	IF in_account_detail_id_confirm IS NULL THEN
		RETURN in_account_detail_id;
	END IF;

-- DELETE IT IF IT EXISTS
	DELETE FROM account_details
	WHERE account_id = in_account_detail_id_confirm;

-- RETURN CONFIRMATION
	RETURN in_account_detail_id_confirm;

END;
$function$ LANGUAGE plpgsql;