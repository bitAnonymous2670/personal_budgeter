CREATE OR REPLACE FUNCTION insert_transaction_category_join(in_transaction_category_join jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_transaction_category_id TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_transaction_category_id = NULL;
	

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for transaction category join.';

-- REQUIRED
	IF in_transaction_category_join->>'joining_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key joining_id.',
			hint = err_hint;
	END IF;

	IF in_transaction_category_join->>'transaction_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_id.',
			hint = err_hint;
	END IF;

	IF in_transaction_category_join->>'category_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key category_id.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	


-- CHECK IF TRANSACTION EXISTS
	SELECT tcj.joining_id
	INTO in_transaction_category_id
	FROM transaction_category_joining_table tcj
	WHERE tcj.transaction_id = in_transaction_category_join->>'transaction_id'
	AND tcj.category_id = in_transaction_category_join->>'category_id';

	IF in_transaction_category_id IS NOT NULL THEN
		RETURN in_transaction_category_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO transaction_category_joining_table (
		joining_id,
		transaction_id,
		category_id
	)
	VALUES (
		in_transaction_category_join->>'joining_id',
		in_transaction_category_join->>'transaction_id',
		in_transaction_category_join->>'category_id'
	)
	RETURNING joining_id INTO in_transaction_category_id;
	

	RETURN in_transaction_category_id;

END;
$function$ LANGUAGE plpgsql;