CREATE OR REPLACE FUNCTION insert_account_detail(in_account_detail jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_account_id TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_account_id = NULL;
	

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for account details.';

-- REQUIRED
	IF in_account_detail->>'account_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key account_id.',
			hint = err_hint;
	END IF;

	IF in_account_detail->>'account_name' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key account_name.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	


-- CHECK IF TRANSACTION EXISTS
	SELECT ad.account_id
	INTO in_account_id
	FROM account_details ad
	WHERE ad.account_name = in_account_detail->>'account_name';

	IF in_account_id IS NOT NULL THEN
		RETURN in_account_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO account_details (
		account_id,
		account_name
	)
	VALUES (
		in_account_detail->>'account_id',
		in_account_detail->>'account_name'
	)
	RETURNING account_id INTO in_account_id;
	

	RETURN in_account_id;

END;
$function$ LANGUAGE plpgsql;