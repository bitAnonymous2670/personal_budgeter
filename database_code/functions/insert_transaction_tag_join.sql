CREATE OR REPLACE FUNCTION insert_transaction_tag_join(in_transaction_tag_join jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_transaction_tag_id TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_transaction_tag_id = NULL;
	

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for transaction tag join.';

-- REQUIRED
	IF in_transaction_tag_join->>'joining_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key joining_id.',
			hint = err_hint;
	END IF;

	IF in_transaction_tag_join->>'transaction_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_id.',
			hint = err_hint;
	END IF;

	IF in_transaction_tag_join->>'tag_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key tag_id.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	


-- CHECK IF TRANSACTION EXISTS
	SELECT ttj.joining_id
	INTO in_transaction_tag_id
	FROM transaction_tag_joining_table ttj
	WHERE ttj.transaction_id = in_transaction_tag_join->>'transaction_id'
	AND ttj.tag_id = in_transaction_tag_join->>'tag_id';

	IF in_transaction_tag_id IS NOT NULL THEN
		RETURN in_transaction_tag_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO transaction_tag_joining_table (
		joining_id,
		transaction_id,
		tag_id
	)
	VALUES (
		in_transaction_tag_join->>'joining_id',
		in_transaction_tag_join->>'transaction_id',
		in_transaction_tag_join->>'tag_id'
	)
	RETURNING joining_id INTO in_transaction_tag_id;
	

	RETURN in_transaction_tag_id;

END;
$function$ LANGUAGE plpgsql;