CREATE OR REPLACE FUNCTION insert_transaction_account_join(in_transaction_account_join jsonb)
RETURNS TEXT AS $function$
DECLARE
	in_transaction_account_id TEXT;

	malformed_json_err  TEXT;
	err_hint TEXT;
BEGIN

-- SET EVERYTHING TO NULL
	in_transaction_account_id = NULL;
	

	malformed_json_err = 'MALFORMED_JSON';
	err_hint = 'Check JSON requirements for transaction account join.';

-- REQUIRED
	IF in_transaction_account_join->>'joining_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key joining_id.',
			hint = err_hint;
	END IF;

	IF in_transaction_account_join->>'transaction_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key transaction_id.',
			hint = err_hint;
	END IF;

	IF in_transaction_account_join->>'account_id' IS NULL THEN
		RAISE EXCEPTION USING
			errcode = malformed_json_err,
			message = 'Object did not include key account_id.',
			hint = err_hint;
	END IF;

-- OPTIONAL
	


-- CHECK IF TRANSACTION EXISTS
	SELECT taj.joining_id
	INTO in_transaction_account_id
	FROM transaction_account_joining_table taj
	WHERE taj.transaction_id = in_transaction_account_join->>'transaction_id'
	AND taj.account_id = in_transaction_account_join->>'account_id';

	IF in_transaction_account_id IS NOT NULL THEN
		RETURN in_transaction_account_id;
	END IF;

-- INSERT TRANSACTION
	INSERT INTO transaction_account_joining_table (
		joining_id,
		transaction_id,
		account_id
	)
	VALUES (
		in_transaction_account_join->>'joining_id',
		in_transaction_account_join->>'transaction_id',
		in_transaction_account_join->>'account_id'
	)
	RETURNING joining_id INTO in_transaction_account_id;
	

	RETURN in_transaction_account_id;

END;
$function$ LANGUAGE plpgsql;