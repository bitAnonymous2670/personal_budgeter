CREATE OR REPLACE FUNCTION update_account_details(in_account_details jsonb[])
RETURNS TEXT[] AS $function$
DECLARE
	out_account_detail_ids TEXT[];
BEGIN

	out_account_detail_ids = NULL;
	BEGIN 
		SELECT ARRAY(
			SELECT update_account_detail(val) FROM unnest(in_account_details) AS val
		) INTO out_account_detail_ids;
	EXCEPTION
		WHEN OTHERS THEN NULL;
	END;

	RETURN out_account_detail_ids;
	
END; 
$function$ LANGUAGE plpgsql;