import json, os
import utils

CONFIG_FILE = None
CONFIG_FILE_NAME = os.sep.join([utils.get_base(), "config.json"])

if not os.path.exists(CONFIG_FILE_NAME):
	exit()


with open(CONFIG_FILE_NAME, "r") as f:
	CONFIG_FILE = json.loads(f.read())


print(CONFIG_FILE)