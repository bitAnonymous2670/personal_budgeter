const { Pool, Client } 	= require('pg');
const config 			= require("../config.json");

module.exports = {
	create_pool: function() {
		const pool = new Pool(config.database);

		pool.on('connect', (client) => {
			client.query("SET search_path TO " + config.database.schema + ";");
		});

		return pool;
	}
};
