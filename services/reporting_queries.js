const sprintf 	= require("sprintf");

/*
const { Pool, Client } = require("pg");
const config 			= require("../config.json");
const pool = new Pool(config.database);

pool.on('connect', (client) => {
	client.query("SET search_path TO " + config.database.schema + ";");
});
*/
const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();

module.exports = {
	get_category_aggregate_pivot: function(callback) {
		pool.connect((err, client, done) => {

			if (err) {
				done();
				return callback(err);
			}
  
			let sql_query = `
				select  TO_CHAR(bt.transaction_date, 'YYYY')  as bank_year,
						TO_CHAR(bt.transaction_date, 'MM')  as bank_month,
						cvc.category_value as category_value,
						SUM(bt.transaction_amount) as total_spent
				from bank_transactions bt
				left join transaction_category_joining_table tcjt 
					on bt.transaction_id = tcjt.transaction_id
				LEFT JOIN category_view_contents cvc 
					ON cvc.category_id = tcjt.category_id
				WHERE tcjt.category_id IS NOT NULL
				group by 
					cvc.category_value, 
					TO_CHAR(bt.transaction_date, 'YYYY'),
					TO_CHAR(bt.transaction_date, 'MM');
			`;

			client.query(sql_query, (err, result) => {
				if (err) {
					done();
					return callback(err);
				}

				let return_result = result.rows;

				done();
				return callback(null, return_result);
			});
		})
	},
	get_available_columns: function(callback) {
		pool.connect((err, client, done) => {
			if (err) {
				done();
				return callback(err);
			}

			sql_query = `

			`;

		})
	}
}