const sprintf 	= require("sprintf");

const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();

function get_transaction_cols() {
	return [
		"transaction_id",
		"transaction_date",
		"transaction_payee",
		"transaction_amount",
		"transaction_notes",
		"transaction_category",
		"transaction_account",
		"category_value"
	]
}

function get_category_summary_columns() {
	return [
		"category_value",
		"total_transaction_amount",
		"negative_transaction_amount",
		"positive_transaction_amount",
		"negative_transaction_count",
		"positive_transaction_count"
	]
}

module.exports = {
	get_transaction_cols: get_transaction_cols,
	get_category_summary_columns: get_category_summary_columns,
	get_transactions: function(from_date, to_date, callback) {
		pool.connect((err, client, done) => {
			if (err) {
				done();
				return callback(err);
			}

			let sql_query = `
				SELECT
					t.transaction_id AS transaction_id,
					t.transaction_date AS transaction_date,
					t.transaction_amount AS transaction_amount,
					t.transaction_payee AS transaction_payee,
					t.transaction_notes AS transaction_notes,
					tcj.category_id AS transaction_category,
					taj.account_id AS transaction_account,
					cvc.category_value AS category_value
				FROM
					bank_transactions t
				LEFT JOIN
					transaction_category_joining_table tcj
				ON
					tcj.transaction_id = t.transaction_id
				LEFT JOIN
					transaction_account_joining_table taj
				ON
					taj.transaction_id = t.transaction_id
				LEFT JOIN
					category_view_contents cvc
				ON
					tcj.category_id = cvc.category_id
				WHERE
					t.transaction_date BETWEEN '${from_date}' AND '${to_date}'
				ORDER BY 
					t.transaction_date;
			`;

			client.query(sql_query, (err, result) => {

				if (err) {
					done();
					return callback(err);
				}

				let return_result = result.rows;

				for (let i = 0; i < return_result.length; i++) {
					return_result[i].transaction_amount = return_result[i].transaction_amount / 100;
				}

				done();
				return callback(null, return_result);

			});
		});
	},
	get_transaction_category_summary: function(from_date, to_date, callback) {
		pool.connect((err, client, done) => {
			if (err) {
				done();
				return callback(err);
			}

			let sql_query = `
				SELECT
					category_value,
					SUM(positive_transaction_amount) AS positive_transaction_amount,
					SUM(negative_transaction_amount) AS negative_transaction_amount,
					SUM(positive_transaction_count)::INTEGER AS positive_transaction_count,
					SUM(negative_transaction_count)::INTEGER AS negative_transaction_count,
					SUM(total_transaction_amount) AS total_transaction_amount
				FROM(
					SELECT
						cvc.category_value AS category_value,
						CASE
							WHEN 0 <= t.transaction_amount THEN t.transaction_amount::FLOAT / 100
							ELSE 0
						END AS positive_transaction_amount,
						CASE
							WHEN t.transaction_amount < 0 THEN t.transaction_amount::FLOAT / 100
							ELSE 0
						END AS negative_transaction_amount,
						CASE
							WHEN 0 <= t.transaction_amount THEN 1
							ELSE 0
						END AS positive_transaction_count,
						CASE
							WHEN t.transaction_amount < 0 THEN 1
							ELSE 0
						END AS negative_transaction_count,
						t.transaction_amount::FLOAT / 100 AS total_transaction_amount
					FROM
						bank_transactions t
					LEFT JOIN
						transaction_category_joining_table tcj
					ON
						tcj.transaction_id = t.transaction_id
					LEFT JOIN
						transaction_account_joining_table taj
					ON
						taj.transaction_id = t.transaction_id
					LEFT JOIN
						category_view_contents cvc
					ON
						tcj.category_id = cvc.category_id
					WHERE
						'${from_date}' <= t.transaction_date AND t.transaction_date <= '${to_date}'
				) AS nothing
				GROUP BY category_value;
			`;

			client.query(sql_query, (err, result) => {

				if (err) {
					done();
					return callback(err);
				}

				let return_result = result.rows;

				for (let i = 0; i < return_result.length; i++) {
					return_result[i].transaction_amount = return_result[i].transaction_amount / 100;
				}

				done();
				return callback(null, return_result);

			});
		});
	}
}