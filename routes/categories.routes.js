const express = require('express');
const router  = express.Router();
const short   = require('short-uuid');
const sprintf = require("sprintf");
const format = require('pg-format');
const moment = require("moment");


const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();

router.get("/", (req, res) => {
	let call_id = short.generate();

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "SELECT * FROM category_view_contents;";

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});


router.put("/", (req, res) => {
	let call_id = short.generate();

	if (!req.body.category_details) {
		let err_msg = "Did not send \"category_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let category_details = req.body.category_details;

	if (!Array.isArray(category_details)) {
		let err_msg = "Variable \"category_details\" is not an array.";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let err_msg = "Variable \"category_details\" at position %s doesnt contain %s.";
	let insert_array = new Array(category_details.length);

	for (let i = 0; i < category_details.length; i++) {
		if (!category_details[i].hasOwnProperty("category_value")) {
			let property = "\"category_details.category_value\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		insert_array[i] = [
			short.generate(),
			category_details[i].category_value
		]
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = format(
			`
				INSERT INTO 
					category_view_contents
				(
					category_id,
					category_value
				)
				VALUES
				%L;
			`,
			insert_array
		);

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			sql_query_two = `
				DELETE FROM
					category_view_contents
				WHERE
					category_id IN (
						SELECT 
							category_id
						FROM (
							SELECT
								cvc.category_id,
								cvc.insert_date,
								MIN(cvc.insert_date) OVER (
									PARTITION BY (
										cvc.category_value
									)
								) AS min_date
							FROM
								category_view_contents
						) tmp 
						WHERE 
							tmp.min_date != tmp.insert_date
					);
			`;

			client.query(sql_query_two, (err, result_2) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_2.rows});

			});
		});
	});
});


router.post("/:category_id", (req, res) => {
	let call_id = short.generate();
	let i;

	if (!req.params.category_id) {
		let err_msg = "Did not sent \"category_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.body.category_details) {
		let err_msg = "Did not send \"category_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let category_details = req.body.category_details;
	let cols = [ "category_value" ];
	var original_categories = new Array(cols.length + 1);

	for (i = 0; i < cols.length + 1; i++) { original_categories[i] = null; }

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let comb_cols = cols.join(", ");
		let sql_query = "SELECT %s FROM category_view_contents WHERE category_id = $1;";

		client.query(sprintf(sql_query, comb_cols), [req.params.category_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			if (result.rows.length == 0) {
				done();
				let err_msg = "category with id = \"%s\" does not exist.";
				return res.status(400).json({
					"err": sprintf(err_msg, req.params.category_id), "call_id": call_id
				});
			}

			for (i = 0; i < cols.length; i++) {
				if (category_details.hasOwnProperty(cols[i])) {
					original_categories[i] = category_details[cols[i]];
				} else if (result.rows[0].hasOwnProperty(cols[i])) {
					original_categories[i] = result.rows[0][cols[i]];
				}
			}

			original_categories[cols.length] = req.params.category_id;

			let sql_query_two = `
				UPDATE
					category_view_contents
				SET 
					category_value = $1
				WHERE 
					category_id = $2;
			`;

			client.query(sql_query_two, original_categories, (err, result_two) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_two.rows});

			});
		});
	});
});


router.delete("/:category_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.category_id) {
		return res.status(400).json({"err": "Did not sent \"category_id\"."});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "DELETE FROM category_view_contents WHERE category_id = $1;";
		client.query(sql_query, [req.params.category_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

// ################################################################################################

router.delete("/:category_id/transaction/", (req, res) => {
	let call_id = short.generate();

	if (!req.params.category_id) {
		let err_msg = "Did not sent \"category_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}
	
	let category_id = req.params.category_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_category_joining_table 
			WHERE 
				category_id = $1
		`;

		client.query(sql_query, [category_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});


module.exports = router;