const express = require('express');
const router  = express.Router();
const short   = require('short-uuid');
const sprintf = require("sprintf");
const format = require('pg-format');
const moment = require("moment");


const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();

router.get("/", (req, res) => {
	let call_id = short.generate();

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "SELECT * FROM account_details ORDER BY account_insert;";

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

router.put("/", (req, res) => {

	let call_id = short.generate();

	if (!req.body.account_details) {
		let err_msg = "Did not send \"account_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let account_details = req.body.account_details;

	if (!Array.isArray(account_details)) {
		let err_msg = "Variable \"account_details\" is not an array.";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let err_msg = "Variable \"account_details\" at position %s doesnt contain %s.";
	let insert_array = new Array(account_details.length);

	for (let i = 0; i < account_details.length; i++) {
		if (!account_details[i].hasOwnProperty("account_name")) {
			let property = "\"account_details.account_name\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		insert_array[i] = [
			short.generate(),
			account_details[i].account_name
		]
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = format(
			`
				INSERT INTO 
					account_details
				(
					account_id,
					account_name
				)
				VALUES
				%L;
			`,
			insert_array
		);

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			sql_query_two = `
				DELETE FROM
				    account_details
				WHERE
				    account_id IN (
				        SELECT
				            tmp.account_id
				        FROM (
				            SELECT
				                ad.account_id,
				                ad.account_insert,
				                MIN(ad.account_insert) OVER (
				                    PARTITION BY (
				                        ad.account_name
				                    )
				                ) AS min_date
				            FROM
				                account_details ad
				        ) tmp
				        WHERE
				            tmp.min_date != tmp.account_insert
				    );
			`;

			client.query(sql_query_two, (err, result_2) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_2.rows});

			});
		});
	});

});

router.post("/:account_id", (req, res) => {
	let call_id = short.generate();
	let i;

	if (!req.params.account_id) {
		let err_msg = "Did not sent \"account_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.body.account_details) {
		let err_msg = "Did not send \"account_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let account_details = req.body.account_details;
	let cols = [ "account_name" ];
	var original_categories = new Array(cols.length + 1);

	for (i = 0; i < cols.length + 1; i++) { original_categories[i] = null; }

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let comb_cols = cols.join(", ");
		let sql_query = "SELECT %s FROM account_details WHERE account_id = $1;";

		client.query(sprintf(sql_query, comb_cols), [req.params.account_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			if (result.rows.length == 0) {
				done();
				let err_msg = "category with id = \"%s\" does not exist.";
				return res.status(400).json({
					"err": sprintf(err_msg, req.params.category_id), "call_id": call_id
				});
			}

			for (i = 0; i < cols.length; i++) {
				if (category_details.hasOwnProperty(cols[i])) {
					original_categories[i] = category_details[cols[i]];
				} else if (result.rows[0].hasOwnProperty(cols[i])) {
					original_categories[i] = result.rows[0][cols[i]];
				}
			}

			original_categories[cols.length] = req.params.category_id;

			let sql_query_two = `
				UPDATE
					account_details
				SET 
					account_name = $1
				WHERE 
					account_id = $2;
			`;

			client.query(sql_query_two, original_categories, (err, result_two) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_two.rows});

			});
		});
	});
});

router.delete("/:account_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.account_id) {
		return res.status(400).json({"err": "Did not sent \"account_id\"."});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "DELETE FROM account_details WHERE account_id = $1;";
		client.query(sql_query, [req.params.account_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

// ################################################################################################

router.delete("/:account_id/transaction", (req, res) => {
	let call_id = short.generate();

	if (!req.params.account_id) {
		let err_msg = "Did not sent \"account_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}
	
	let account_id = req.params.account_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_account_joining_table 
			WHERE 
				account_id = $1
		`;

		client.query(sql_query, [account_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

module.exports = router;