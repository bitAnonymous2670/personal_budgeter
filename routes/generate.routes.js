const express 				= require("express");
const router  				= express.Router();
const short   				= require("short-uuid");
const ExcelJS 				= require('exceljs');
const moment				= require("moment");
const transaction_queries 	= require("../services/transaction_queries.js");
const request 				= require("request")

/*
const { Pool, Client } = require('pg');
const config 			= require("../config.json");
const pool = new Pool(config.database);

pool.on('connect', (client) => {
	client.query("SET search_path TO " + config.database.schema + ";");
});
*/

const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();

router.get("/", (req, res) => {

	let call_id = short.generate();
	let write_value = null;

	if (!req.query.from) {
		let err_msg = "Did not sent \"from date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.query.to) {
		let err_msg = "Did not sent \"to date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let query = "from=" + req.query.from + "&to=" + req.query.to;
	let url =  "http://192.168.1.151:3001/generate?" + query;

	req.pipe(request({uri: url})).pipe(res);

});

module.exports = router;