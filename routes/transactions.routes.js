const express 				= require("express");
const router  				= express.Router();
const short   				= require("short-uuid");
const sprintf 				= require("sprintf");
const format 				= require("pg-format");
const moment				= require("moment");
const transaction_queries 	= require("../services/transaction_queries.js");
const config                = require("../config.json");
const fs					= require("fs");

const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();

router.get("/", (req, res) => {
	let call_id = short.generate();

	if (!req.query.from) {
		let err_msg = "Did not sent \"from date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.query.to) {
		let err_msg = "Did not sent \"to date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}


	let from_date = moment(req.query.from, "MMDDYYYY").format("MM-DD-YYYY");
	let to_date = moment(req.query.to, "MMDDYYYY").format("MM-DD-YYYY");

	transaction_queries.get_transactions(from_date, to_date, function(err, result){
		if (err) {
			return res.status(400).json({"err": err, "call_id": call_id});
		}

		return res.status(200).json({"result": result});
	});
});

router.get("/summary/gross", (req, res) => {
	let call_id = short.generate();

	if (!req.query.from) {
		let err_msg = "Did not sent \"from date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.query.to) {
		let err_msg = "Did not sent \"to date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}


	let from_date = moment(req.query.from, "MMDDYYYY").format("MM-DD-YYYY");
	let to_date = moment(req.query.to, "MMDDYYYY").format("MM-DD-YYYY");

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			SELECT
				SUM(t.transaction_amount) AS transaction_amount
			FROM
				bank_transactions t
			WHERE
				t.transaction_date BETWEEN '${from_date}' AND '${to_date}';
		`;

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			let return_result = result.rows[0];
			return_result.transaction_amount = return_result.transaction_amount / 100;

			done();
			res.status(200).json({"result": return_result});

		});
	});
});

router.get("/:transation_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "SELECT * FROM bank_transactions WHERE transaction_id = $1;";
		client.query(sql_query, [req.params.transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			let return_result = result.rows;

			for (let i = 0; i < return_result.length; i++) {
				return_result[i].transaction_amount = return_result[i].transaction_amount / 100;
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

router.post("/:transation_id", (req, res) => {
	let call_id = short.generate();
	let i;

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.body.transaction_details) {
		let err_msg = "Did not send \"transaction_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transaction_details = req.body.transaction_details[0];

	let cols = [
		"transaction_date",
		"transaction_amount",
		"transaction_payee",
		"transaction_notes"
	];

	var original_transaction = new Array(cols.length + 1);
	let type_err_msg = "Variable \"%s\" cannot be converted to a float.";

	for (i = 0; i < cols.length + 1; i++) {
		original_transaction[i] = null;
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let comb_cols = cols.join(", ");
		let sql_query = "SELECT %s FROM bank_transactions WHERE transaction_id = $1;";

		client.query(sprintf(sql_query, comb_cols), [req.params.transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			if (result.rows.length == 0) {
				done();
				let err_msg = "transaction with id = \"%s\" does not exist.";
				return res.status(400).json({
					"err": sprintf(err_msg, req.params.transation_id), "call_id": call_id
				});
			}

			for (i = 0; i < cols.length; i++) {
				if (transaction_details.hasOwnProperty(cols[i])) {

					if (cols[i] == "transaction_amount") {

						try {
							original_transaction[i] = (parseFloat(
								transaction_details[cols[i]]
							) * 100).toFixed(0);
						} catch (err) {
							done();
							let property = "\"transaction_details.transaction_amount\"";
							return res.status(400).json({
								"err": sprintf(type_err_msg, property), "call_id": call_id
							});
						}

						original_transaction[i] = transaction_details[cols[i]];
					} else {
						original_transaction[i] = transaction_details[cols[i]];
					}

				} else {
					original_transaction[i] = result.rows[0][cols[i]];
				}
			}

			original_transaction[cols.length] = req.params.transation_id;

			let sql_query_two = `
				UPDATE 
					bank_transactions
				SET 
					transaction_date = $1,
					transaction_amount = $2,
					transaction_payee = $3,
					transaction_notes = $4
				WHERE 
					transaction_id = $5;
			`;

			client.query(sql_query_two, original_transaction, (err, result_two) => {
				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_two.rows});
			});

		});
	});

});

router.put("/", (req, res) => {
	let call_id = short.generate();

	if (!req.body.transaction_details) {
		let err_msg = "Did not send \"transaction_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transaction_details = req.body.transaction_details;

	if (!Array.isArray(transaction_details)) {
		let err_msg = "Variable \"transaction_details\" is not an array.";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let err_msg = "Variable \"transaction_details\" at position %s doesnt contain %s.";
	let type_err_msg = "Variable \"%s\" at position %s cannot be converted to a float.";
	let insert_array = new Array(transaction_details.length);

	for (let i = 0; i < transaction_details.length; i++) {
		if (!transaction_details[i].hasOwnProperty("transaction_date")) {
			let property = "\"transaction_details.transaction_date\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		if (!transaction_details[i].hasOwnProperty("transaction_amount")) {
			let property = "\"transaction_details.transaction_amount\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		try {
			transaction_details[i].transaction_amount = (parseFloat(
				transaction_details[i].transaction_amount
			) * 100).toFixed(0);
		} catch (err) {
			let property = "\"transaction_details.transaction_amount\"";
			return res.status(400).json({
				"err": sprintf(type_err_msg, property, i), "call_id": call_id
			});
		}

		if (!transaction_details[i].hasOwnProperty("transaction_payee")) {
			let property = "\"transaction_details.transaction_payee\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		if (!transaction_details[i].hasOwnProperty("transaction_notes")) {
			let property = "\"transaction_details.transaction_notes\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		insert_array[i] = [
			short.generate(),
			transaction_details[i].transaction_date,
			transaction_details[i].transaction_amount,
			transaction_details[i].transaction_payee,
			transaction_details[i].transaction_notes
		]
	}

	let fn = config.write_raw_transaction_put_location + "/";
	fn += moment().format("MMDDYYYY_HHmmss_x") + ".data";
	fs.writeFile(fn, JSON.stringify(req.body.transaction_details), (err) => {  });

	pool.connect((err, client, done) => {
		if (err) {
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = format(
			`
				INSERT INTO 
					bank_transactions
				(
					transaction_id,
					transaction_date,
					transaction_amount,
					transaction_payee,
					transaction_notes
				)
				VALUES
				%L;
			`,
			insert_array
		);

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			sql_query_two = `
			DELETE FROM
				bank_transactions
			WHERE
				transaction_id IN (
					SELECT
						tmp.transaction_id
					FROM (
						SELECT
							bt.transaction_id,
							bt.insert_date,
							MIN(bt.insert_date) OVER (
								PARTITION BY (
									bt.transaction_date,
									bt.transaction_amount,
									bt.transaction_payee
								)
							) AS min_date
						FROM
							bank_transactions bt
					) tmp
					WHERE 
						tmp.min_date != tmp.insert_date
				);
			`

			client.query(sql_query_two, (err, result_2) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_2.rows});
			});
		});
	});

});

router.delete("/:transation_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		return res.status(400).json({"err": "Did not sent \"transation_id\"."});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "DELETE FROM bank_transactions WHERE transaction_id = $1;";
		client.query(sql_query, [req.params.transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});


// ################################################################################################
router.put("/:transation_id/category/:category_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.params.category_id) {
		let err_msg = "Did not sent \"category_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transation_id = req.params.transation_id;
	let category_id = req.params.category_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_category_joining_table 
			WHERE 
				transaction_id = $1;
		`;

		client.query(sql_query, [transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			sql_query_two = format(`
					INSERT INTO
						transaction_category_joining_table
					(
						joining_id,
						transaction_id,
						category_id
					)
					VALUES 
					%L
				`,
				[[short.generate(), transation_id, category_id]]
			);

			client.query(sql_query_two, (err, result_2) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_2.rows});
			});
		});
	});
});

router.put("/:transation_id/tags/:tag_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.params.tag_id) {
		let err_msg = "Did not sent \"category_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transation_id = req.params.transation_id;
	let tag_id = req.params.tag_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
				INSERT INTO
					transaction_tag_joining_table
				(
					joining_id,
					transaction_id,
					tag_id
				)
				VALUES 
				($1, $2, $3)
				RETURNING joining_id;
			`;

		client.query(sql_query, [short.generate(), transation_id, tag_id], (err, result) => {

			if (err) {
				console.log(err)
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
				res.status(200).json({"result": result.rows});
			
		});
	});
});

router.delete("/:transation_id/category/", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transation_id = req.params.transation_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_category_joining_table 
			WHERE 
				transaction_id = $1
		`;

		client.query(sql_query, [transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});


// ################################################################################################

router.put("/:transation_id/account/:account_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.params.account_id) {
		let err_msg = "Did not sent \"account_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transation_id = req.params.transation_id;
	let account_id = req.params.account_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_account_joining_table 
			WHERE 
				transaction_id = $1;
		`;

		client.query(sql_query, [transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			sql_query_two = format(`
					INSERT INTO
						transaction_account_joining_table
					(
						joining_id,
						transaction_id,
						account_id
					)
					VALUES 
					%L	
				`,
				[[short.generate(), transation_id, account_id]]
			);

			client.query(sql_query_two, (err, result_2) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_2.rows});
			});
		});
	});
});

router.delete("/:transation_id/account/", (req, res) => {
	let call_id = short.generate();

	if (!req.params.transation_id) {
		let err_msg = "Did not sent \"transation_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let transation_id = req.params.transation_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_account_joining_table 
			WHERE 
				transaction_id = $1
		`;

		client.query(sql_query, [transation_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

// ################################################################################################


module.exports = router;