const path = require('path');
const sprintf = require("sprintf");
const short   = require('short-uuid');

const { spawn } = require('child_process');

const express 				= require("express");
const router  				= express.Router();

const { Pool, Client } = require('pg');

const reporting_queries 	= require("../services/reporting_queries.js");
const config 				= require("../config.json");



router.get("/pivot", (req, res) => {
	let call_id = short.generate();

	reporting_queries.get_category_aggregate_pivot(function(err, result) {
		if (err) {
			return res.status(400).json({"err": err, "call_id": call_id});
		}

		return res.status(200).json({"result": result});
	});
});

router.get("/pivot/columns", (req, res) => {
	
});

/*

	/usr/bin/python3 
	/build/py_budget_api/db_query_script.py 
		-filter year=2021 
		-filter month=January 
	| 
	/usr/bin/python3 
	/build/py_budget_api/access_summary.py 
		-dc transaction_amount,category_value,transaction_year,
			transaction_month,transaction_day 
		-pc transaction_year,transaction_month,transaction_day 
		-sc transaction_amount 
		-fn sum 
		-dl 29 
		-frmt data

*/

let DEFAULT_DB_COLUMNS = [
	"transaction_amount",
	"category_value",
	"transaction_year",
	"transaction_month",
	"transaction_day"
];

router.get("/new-pivot", (req, res) => {

	if (!("pivot_columns" in req.query)) {
		return res.status(400).json({"err": "Bad Query."});
	}

	if (!("summary_columns" in req.query)) {
		return res.status(400).json({"err": "Bad Query."});
	}

	let python_path = path.sep + ["usr", "bin", "python3"].join(path.sep);
	let db_query_script_params = [
		path.sep + [
			"build", 
			"py_budget_api", 
			"db_query_script.py"
		].join(path.sep),
		"-c", path.sep + [
			"build", 
			"py_budget_api", 
			"config.json"
		].join(path.sep),
	];

	if ("year" in req.query) {
		db_query_script_params.push("-filter");
		db_query_script_params.push("year=" + req.query.year);
	}

	if ("month" in req.query) {
		db_query_script_params.push("-filter");
		db_query_script_params.push("month=" + req.query.month);
	}

	if ("day" in req.query) {
		db_query_script_params.push("-filter");
		db_query_script_params.push("day=" + req.query.day);
	}

	if ("category" in req.query) {
		db_query_script_params.push("-filter");
		db_query_script_params.push("category=" + req.query.category);
	}

	let pivot_params = [
		path.sep + [
			"build", 
			"py_budget_api", 
			"access_summary.py"
		].join(path.sep),
		"-frmt", "data",
		"-dc", DEFAULT_DB_COLUMNS.join(","),
		"-fn", "sum",
		"-dl", "29",
		"-pc", req.query.pivot_columns,
		"-sc", req.query.summary_columns
	];

	const db_access = spawn(python_path, db_query_script_params);
	const pivot = spawn(python_path, pivot_params);

	db_access.stdout.pipe(pivot.stdin);
	pivot.stdout.pipe(res);
});



module.exports = router;