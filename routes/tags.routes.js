

const express = require('express');
const router  = express.Router();
const short   = require('short-uuid');
const sprintf = require("sprintf");
const format = require('pg-format');
const moment = require("moment");


const db_pool = require("../services/create_db_pool.js");
const pool = db_pool.create_pool();


router.get("/", (req, res) => {
	let call_id = short.generate();

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "SELECT * FROM tag_contents;";

		client.query(sql_query, (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

router.put("/", (req, res) => {
	let call_id = short.generate();

	if (!req.body.tag_details) {
		let err_msg = "Did not send \"tag_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let tag_details = req.body.tag_details;

	if (!Array.isArray(tag_details)) {
		let err_msg = "Variable \"tag_details\" is not an array.";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let err_msg = "Variable \"tag_details\" at position %s doesnt contain %s.";
	let insert_array = new Array(tag_details.length);

	for (let i = 0; i < tag_details.length; i++) {
		if (!tag_details[i].hasOwnProperty("tag_value")) {
			let property = "\"tag_details.tag_value\"";
			return res.status(400).json({"err": sprintf(err_msg, i, property), "call_id": call_id});
		}

		insert_array[i] = [
			short.generate(),
			tag_details[i].tag_value
		]
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = format(
			`
				INSERT INTO 
					tag_contents
				(
					tag_id,
					tag_value
				)
				VALUES
				%L;
			`,
			insert_array
		);

		// console.log(sql_query)

		client.query(sql_query, (err, result) => {

			if (err) {
				// console.log(err)
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			sql_query_two = `
				DELETE FROM
					tag_contents
				WHERE
					tag_id IN (
						SELECT 
							tag_id
						FROM (
							SELECT
								tc.tag_id,
								tc.insert_date,
								MIN(tc.insert_date) OVER (
									PARTITION BY (
										tc.tag_value
									)
								) AS min_date
							FROM
								tag_contents tc
						) tmp 
						WHERE 
							tmp.min_date != tmp.insert_date
					);
			`;

			client.query(sql_query_two, (err, result_2) => {

				if (err) {
					// console.log(err)
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_2.rows});

			});
		});
	});
});

router.post("/:tag_id", (req, res) => {
	let call_id = short.generate();
	let i;

	if (!req.params.tag_id) {
		let err_msg = "Did not sent \"tag_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.body.tag_details) {
		let err_msg = "Did not send \"tag_details\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let tag_details = req.body.tag_details;
	let cols = [ "tag_value" ];
	var original_tags = new Array(cols.length + 1);

	for (i = 0; i < cols.length + 1; i++) { original_tags[i] = null; }

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let comb_cols = cols.join(", ");
		let sql_query = "SELECT %s FROM tag_contents WHERE tag_id = $1;";

		client.query(sprintf(sql_query, comb_cols), [req.params.tag_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			if (result.rows.length == 0) {
				done();
				let err_msg = "tag with id = \"%s\" does not exist.";
				return res.status(400).json({
					"err": sprintf(err_msg, req.params.tag_id), "call_id": call_id
				});
			}

			for (i = 0; i < cols.length; i++) {
				if (tag_details.hasOwnProperty(cols[i])) {
					original_tags[i] = tag_details[cols[i]];
				} else if (result.rows[0].hasOwnProperty(cols[i])) {
					original_tags[i] = result.rows[0][cols[i]];
				}
			}

			original_tags[cols.length] = req.params.tag_id;

			let sql_query_two = `
				UPDATE
					tag_contents
				SET 
					tag_value = $1
				WHERE 
					tag_id = $2;
			`;

			client.query(sql_query_two, original_tags, (err, result_two) => {

				if (err) {
					done();
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				done();
				res.status(200).json({"result": result_two.rows});

			});
		});
	});
});

router.delete("/:tag_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.tag_id) {
		return res.status(400).json({"err": "Did not sent \"tag_id\"."});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "DELETE FROM tag_contents WHERE tag_id = $1;";
		client.query(sql_query, [req.params.tag_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});

router.delete("/:tag_id/transaction/", (req, res) => {
	let call_id = short.generate();

	if (!req.params.tag_id) {
		let err_msg = "Did not sent \"tag_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}
	
	let tag_id = req.params.tag_id;

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = `
			DELETE FROM 
				transaction_tag_joining_table 
			WHERE 
				tag_id = $1
		`;

		client.query(sql_query, [tag_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			done();
			res.status(200).json({"result": result.rows});

		});
	});
});


module.exports = router;