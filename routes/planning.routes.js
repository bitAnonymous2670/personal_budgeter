const express 				= require("express");
const router  				= express.Router();
const short   				= require("short-uuid");
const sprintf 				= require("sprintf");
const moment				= require("moment");
const format 				= require('pg-format');

const db_pool 				= require("../services/create_db_pool.js");
const pool 					= db_pool.create_pool();

function get_category_planning_cols() {
	return [
		"category_id",
		"planned_amount",
		"begin_planning_date",
		"end_planning_date"
	];
}

router.get("/:planning_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.planning_id) {
		return res.status(400).json({"err": "Did not sent \"planning_id\"."});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_columns = [
			"cvc.category_value AS category_value",
			"cp.planned_amount AS planned_amount",
			"cp.begin_planning_date AS begin_planning_date",
			"cp.end_planning_date AS end_planning_date"
		]

		let sql_query = "SELECT " + sql_columns.join(",") + " FROM category_planning cp ";
		sql_query += "LEFT JOIN category_view_contents cvc ";
		sql_query += "ON cvc.category_id = cp.category_id ";
		sql_query += "WHERE cp.planning_id = $1;";

		client.query(sql_query, [req.params.planning_id], (err, result) => {
			done();

			if (err) {
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			res.status(200).json({"result": result.rows});

		});
	});
});

router.get("/", (req, res) => {
	let call_id = short.generate();

	if (!req.query.from) {
		let err_msg = "Did not sent \"from date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.query.to) {
		let err_msg = "Did not sent \"to date\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}


	let from_date = moment(req.query.from, "MMDDYYYY").format("MM-DD-YYYY");
	let to_date = moment(req.query.to, "MMDDYYYY").format("MM-DD-YYYY");

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let between_stmnt = `BETWEEN '${from_date}' AND '${to_date}' `;

// Summing transaction amount
		let sub_query	 = "SELECT SUM(bt.transaction_amount) FROM bank_transactions bt ";
		sub_query 		+= "left join transaction_category_joining_table tcjt ";
		sub_query 		+= "on bt.transaction_id = tcjt.transaction_id ";
		sub_query 		+= "left join category_planning cps ";
		sub_query 		+= "on cps.category_id = tcjt.category_id ";
		sub_query 		+= "WHERE bt.transaction_date BETWEEN "
		sub_query 		+= "cps.begin_planning_date AND cps.end_planning_date ";
		sub_query 		+= "AND cps.category_id = cvc.category_id ";
		sub_query 		+= "AND tcjt.category_id = cp.category_id ";
		sub_query		+= "AND cps.planning_id = cp.planning_id";


// Retruning the table values for the datatable
		let return_cols = [
			"cvc.category_value",
			"cp.planning_id",
			"cp.planned_amount",
			"cp.begin_planning_date",
			"cp.end_planning_date",
			"(" + sub_query + ") AS spent"
		]

		let sql_query 	=  "SELECT " + return_cols.join(", ");
		sql_query 		+= " FROM category_planning cp ";
		sql_query 		+= "left join category_view_contents cvc ";
		sql_query 		+= "on cvc.category_id = cp.category_id ";
		sql_query 		+= "WHERE begin_planning_date " + between_stmnt;
		sql_query 		+= "OR end_planning_date " + between_stmnt;
		sql_query 		+= "ORDER BY planning_insert;";

		// console.log(sql_query)

		client.query(sql_query, (err, result) => {

			done();

			if (err) {
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			res.status(200).json({"result": result.rows});

		});
	});
});


router.put("/", (req, res) => {

	let call_id = short.generate();

	if (!req.body.category_planning) {
		let err_msg = "Did not send \"category_planning\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let category_planning = req.body.category_planning;

	if (!Array.isArray(category_planning)) {
		let err_msg = "Variable \"category_planning\" is not an array.";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let err_msg = "Variable \"category_planning\" at position %s doesnt contain %s.";
	let insert_array = new Array(category_planning.length);

	let category_planning_values = get_category_planning_cols();

// Check everything and gather everything
	for (let i = 0; i < category_planning.length; i++) {
		for (let j = 0; j < category_planning_values.length; j++) {
			if (!category_planning[i].hasOwnProperty(category_planning_values[j])) {
				let property = sprintf("category_planning.%s", category_planning_values[j]);
				return res.status(400).json({
					"err": sprintf(err_msg, i, property), 
					"call_id": call_id
				});
			}	
		}

		insert_array[i] = [
			short.generate()
		]

		for (j = 0; j < category_planning_values.length; j++) {
			insert_array[i].push(category_planning[i][category_planning_values[j]]);
		}
	}

// Connect to run the query
	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query 	= "INSERT INTO category_planning ( ";
		sql_query 		+= (["planning_id"].concat(category_planning_values)).join(",");
		sql_query 		+= ") VALUES %L;";

		sql_query = format(sql_query, insert_array);

		// console.log(sql_query)

		client.query(sql_query, (err, result) => {

			done();

			if (err) {
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			res.status(200).json({"result": result.rows});
		});
	});
});


router.post("/:planning_id", (req, res) => {
	let call_id = short.generate();
	let i;

	if (!req.params.planning_id) {
		let err_msg = "Did not sent \"planning_id\".";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	if (!req.body.category_planning) {
		let err_msg = "Did not send \"category_planning\"";
		return res.status(400).json({"err": err_msg, "call_id": call_id});
	}

	let category_planning = req.body.category_planning;
	let cols = get_category_planning_cols();
	var original_planning = new Array(cols.length + 1);

	for (i = 0; i < cols.length + 1; i++) {
		original_planning[i] = null;
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let comb_cols = cols.join(", ");
		let sql_query = "SELECT %s FROM category_planning WHERE planning_id = $1;";

		client.query(sprintf(sql_query, comb_cols), [req.params.planning_id], (err, result) => {

			if (err) {
				done();
				return res.status(400).json({
					"err": err, 
					"call_id": call_id
				})
			}

			if (result.rows.length == 0) {
				done();
				let err_msg = "planning with id = \"%s\" does not exist.";
				return res.status(400).json({
					"err": sprintf(err_msg, req.params.planning_id), 
					"call_id": call_id
				});
			}

			for (i = 0; i < cols.length; i++) {
				if (category_planning.hasOwnProperty(cols[i])) {
					original_planning[i] = category_planning[cols[i]];
				} else if (result.rows[0].hasOwnProperty(cols[i])) {
					original_planning[i] = result.rows[0][cols[i]];
				}
			}

			original_planning[cols.length] = req.params.planning_id;

			let sql_array_two = [];
			let sql_query_two = "UPDATE category_planning SET ";

			for (i = 0; i < cols.length; i++) {
				sql_array_two.push(sprintf("%s = $%s", cols[i], i + 1));
			}

			sql_query_two += sql_array_two.join(", ");
			sql_query_two += sprintf(" WHERE planning_id = $%s;", cols.length + 1);


/*
			let sql_query_two = `
				UPDATE
					category_view_contents
				SET 
					category_value = $1
				WHERE 
					planning_id = $2;
			`;
*/

			client.query(sql_query_two, original_planning, (err, result_two) => {
				done();

				if (err) {
					return res.status(400).json({"err": err, "call_id": call_id})
				}

				res.status(200).json({"result": result_two.rows});

			});
		});
	});
});


router.delete("/:planning_id", (req, res) => {
	let call_id = short.generate();

	if (!req.params.planning_id) {
		return res.status(400).json({"err": "Did not sent \"planning_id\"."});
	}

	pool.connect((err, client, done) => {
		if (err) {
			done();
			return res.status(400).json({"err": err, "call_id": call_id})
		}

		let sql_query = "DELETE FROM category_planning WHERE planning_id = $1;";
		client.query(sql_query, [req.params.planning_id], (err, result) => {
			done();

			if (err) {
				return res.status(400).json({"err": err, "call_id": call_id})
			}

			res.status(200).json({"result": result.rows});

		});
	});
});

module.exports = router;